MIDDLEWARE_CLASSES = (
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'htmlmin.middleware.HtmlMinifyMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',
    'django_mobile.middleware.MobileDetectionMiddleware',
    'django_mobile.middleware.SetFlavourMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'easy_timezones.middleware.EasyTimezoneMiddleware',
)
STAGE = 'beta'

ADMINS = (
    ('Frittie Team', 'frittie.beta@gmail.com'),
)


WEBSITE_HOMEPAGE = "http://beta.frittie.com/"

DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'

AWS_ACCESS_KEY_ID = 'AKIAIEO3YXO4RD2WXSHA'
AWS_SECRET_ACCESS_KEY = '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'
AWS_STORAGE_BUCKET_NAME = 'frittie-beta'
AWS_PRELOAD_METADATA = True 

AWS_EXPIRE = 60 * 60 * 24 * 7
AWS_HEADERS = {
	'Expires': 'Sun, 19 Jul 2020 18:06:32 GMT',
    'Cache-Control': 'max-age=%d, s-maxage=%d, must-revalidate' % (AWS_EXPIRE,
        AWS_EXPIRE)
}

AWS_QUERYSTRING_EXPIRE = 63115200

DEBUG = False
TEMPLATE_DEBUG = DEBUG

########## DATABASE CONFIGURATION
DATABASES = postgresify()
########## END DATABASE CONFIGURATION


########## CACHE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = memcacheify()
########## END CACHE CONFIGURATION




# See: http://docs.celeryproject.org/en/latest/configuration.html#broker-transport
BROKER_TRANSPORT = 'amqplib'

# Set this number to the amount of allowed concurrent connections on your AMQP
# provider, divided by the amount of active workers you have.
#
# For example, if you have the 'Little Lemur' CloudAMQP plan (their free tier),
# they allow 3 concurrent connections. So if you run a single worker, you'd
# want this number to be 3. If you had 3 workers running, you'd lower this
# number to 1, since 3 workers each maintaining one open connection = 3
# connections total.
#
# See: http://docs.celeryproject.org/en/latest/configuration.html#broker-pool-limit
BROKER_POOL_LIMIT = 3

# See: http://docs.celeryproject.org/en/latest/configuration.html#broker-connection-max-retries
BROKER_CONNECTION_MAX_RETRIES = 0

# See: http://docs.celeryproject.org/en/latest/configuration.html#broker-url
BROKER_URL = environ.get('RABBITMQ_URL') or environ.get('CLOUDAMQP_URL')

# See: http://docs.celeryproject.org/en/latest/configuration.html#celery-result-backend
CELERY_RESULT_BACKEND = 'amqp'

INSTALLED_APPS += (
    'storages',
)

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': os.environ['SEARCHBOX_URL'],
        'INDEX_NAME': 'documents',
    },
}

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

STATICFILES_STORAGE = DEFAULT_FILE_STORAGE

STATIC_URL = 'https://%s.s3.amazonaws.com/' % AWS_STORAGE_BUCKET_NAME

#COMPRESS_STORAGE = DEFAULT_FILE_STORAGE

# MIDDLEWARE_CLASSES += (
#     'frittie.apps.main.middleware.RestrictedAccessMiddleware',
#     'frittie.apps.main.middleware.HandleRedirectionMiddleware',
# )

FACEBOOK_APP_ID = "1443355205907066"
FACEBOOK_APP_SECRET = '877b85a82cb5721044b9eec5e296f51f'

GEOS_LIBRARY_PATH = environ.get('GEOS_LIBRARY_PATH')
GDAL_LIBRARY_PATH = environ.get('GDAL_LIBRARY_PATH')

DATABASES = {
   'default': {
       'ENGINE': 'django.db.backends.postgresql_psycopg2', 
       'NAME':  "frittie_beta",                    
       'USER': 'frittie_db_admin',                    
       'PASSWORD': environ.get('DB_PASSWORD',"frittie4success"),                  
       'HOST': 'frittie.c7cof98jckr2.ap-southeast-1.rds.amazonaws.com',                      
       'PORT': '5432',                     
   }
}
