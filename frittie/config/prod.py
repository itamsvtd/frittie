from os import environ

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'django.middleware.cache.UpdateCacheMiddleware',
    'htmlmin.middleware.HtmlMinifyMiddleware',
    'django_mobile.middleware.MobileDetectionMiddleware',
    'django_mobile.middleware.SetFlavourMiddleware',
    "django_mobile.cache.middleware.CacheFlavourMiddleware",
    'django.middleware.cache.FetchFromCacheMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'easy_timezones.middleware.EasyTimezoneMiddleware',
    "profiler.middleware.ProfileMiddleware",
)

HTML_MINIFY = True

STAGE = 'prod'

ADMINS = (
    ('Frittie Team', 'frittie.prod@gmail.com'),
)

WEBSITE_HOMEPAGE = "http://www.frittie.com/"

DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'

AWS_ACCESS_KEY_ID = 'AKIAIEO3YXO4RD2WXSHA'
AWS_SECRET_ACCESS_KEY = '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'
AWS_STORAGE_BUCKET_NAME = 'frittie'
AWS_PRELOAD_METADATA = True 
AWS_IS_GZIPPED = True
GZIP_CONTENT_TYPES = (
    'text/css',
    'text/html',
    'application/javascript',
    'application/x-javascript',
    'application/json',
    'application/xml'
)
AWS_EXPIRE = 60 * 60 * 24 * 7
AWS_HEADERS = {
	'Expires': 'Sun, 19 Jul 2020 18:06:32 GMT',
    'Cache-Control': 'max-age=%d, s-maxage=%d, must-revalidate' % (AWS_EXPIRE,
        AWS_EXPIRE)
}

AWS_QUERYSTRING_EXPIRE = 63115200

DEBUG = True
TEMPLATE_DEBUG = DEBUG

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

# See: http://docs.celeryproject.org/en/latest/configuration.html#broker-transport
BROKER_TRANSPORT = 'amqplib'

# Set this number to the amount of allowed concurrent connections on your AMQP
# provider, divided by the amount of active workers you have.
#
# For example, if you have the 'Little Lemur' CloudAMQP plan (their free tier),
# they allow 3 concurrent connections. So if you run a single worker, you'd
# want this number to be 3. If you had 3 workers running, you'd lower this
# number to 1, since 3 workers each maintaining one open connection = 3
# connections total.
#
# See: http://docs.celeryproject.org/en/latest/configuration.html#broker-pool-limit
BROKER_POOL_LIMIT = 3

# See: http://docs.celeryproject.org/en/latest/configuration.html#broker-connection-max-retries
BROKER_CONNECTION_MAX_RETRIES = 0

# See: http://docs.celeryproject.org/en/latest/configuration.html#broker-url
BROKER_URL = environ.get('RABBITMQ_URL') or environ.get('CLOUDAMQP_URL')

# See: http://docs.celeryproject.org/en/latest/configuration.html#celery-result-backend
CELERY_RESULT_BACKEND = 'amqp'

INSTALLED_APPS += (
    'storages',
    "profiler",
)

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': environ.get('SEARCHBOX_URL','http://128.199.159.117:9200'),
        'INDEX_NAME': 'documents',
        'TIMEOUT': 60 * 5,
    },
}

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

STATICFILES_STORAGE = DEFAULT_FILE_STORAGE

#STATIC_URL = 'https://%s.s3.amazonaws.com/' % AWS_STORAGE_BUCKET_NAME
STATIC_URL = "https://d1aiy1qufjcdnj.cloudfront.net/"

# COMPRESS_URL = STATIC_URL
# COMPRESS_ROOT = STATIC_ROOT
# COMPRESS_STORAGE = STATICFILES_STORAGE
# COMPRESS_OFFLINE = True

#MIDDLEWARE_CLASSES += (
#    'frittie.apps.main.middleware.HandleRedirectionMiddleware',
#)

FACEBOOK_APP_ID = "265377323561995"
FACEBOOK_APP_SECRET = '7fc45806dff04c60b2532331ee964e67'

GEOS_LIBRARY_PATH = environ.get('GEOS_LIBRARY_PATH')
GDAL_LIBRARY_PATH = environ.get('GDAL_LIBRARY_PATH')

DATABASES = {
   'default': {
       'ENGINE': 'django.db.backends.postgresql_psycopg2',
       'NAME':  "frittie_prod",
       'USER':  'frittie_db_admin',
       'PASSWORD': 'frittie4success',
       'HOST': 'frittie.c7cof98jckr2.ap-southeast-1.rds.amazonaws.com',
       'PORT': '5432',
   }
}



