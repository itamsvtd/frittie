import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "frittie.settings")

import frittie.startup as startup
startup.run()

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
