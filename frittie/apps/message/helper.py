from django.http import HttpResponse, HttpResponseRedirect
from django.db.models import Q

from frittie.apps.main.models import Message, Conversation
from frittie.apps.app_helper import get_user_login_object

def get_new_messages_count(request):
	user_login = get_user_login_object(request)
	return Message.objects.filter(user_receive=user_login,status="1").count()
