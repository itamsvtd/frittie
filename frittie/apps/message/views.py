# Views for the main app
from django.template import Context, loader
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.db.models import Q
from django.contrib.auth.decorators import login_required

from frittie.apps.main.models import Message, Conversation
from frittie.apps.app_helper import get_user_login_object

import datetime

@login_required
def main_page(request):
	data = {}
	user_login = get_user_login_object(request)
	data['is_no_message'] = True
	conversations_count = Conversation.objects.filter(Q(user1=user_login) | Q(user2=user_login)).count()
	if conversations_count > 0:
		data['is_no_message'] = False
		conversations = Conversation.objects.filter(Q(user1=user_login) | Q(user2=user_login)).order_by("-latest_message__date")[:20]
		data['conversations'] = conversations
	return render_to_response('apps/message/page/main_page.html',data,context_instance=RequestContext(request))


@login_required
def show_conversation(request,conversation_unique_id):
	user_login = get_user_login_object(request)
	conversation = get_object_or_404(Conversation,unique_id=conversation_unique_id)
	user_chat = conversation.user1
	if conversation.user2.username != user_login.username:
		user_chat = conversation.user2
	new_messages = conversation.messages.filter(status="1")
	for message in new_messages:
		message.status = "0"
		message.save()
	messages = conversation.messages.order_by("date")[:100]
	return render_to_response("apps/message/page/conversation_page.html",{
			"conversation": conversation,
			"user_chat": user_chat,
			"messages": messages
		},context_instance=RequestContext(request))