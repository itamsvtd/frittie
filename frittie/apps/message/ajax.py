from dajax.core import Dajax
from dajaxice.decorators import dajaxice_register

from django.utils import simplejson
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.db.models import Q

from frittie.apps.app_helper import get_user_login_object, generate_html_snippet, generate_unique_id
from frittie.apps.app_helper import generate_message
from frittie.apps.main.models import Conversation, Message, EmailTracking
from frittie.apps.member.helper import get_user_common_info

from frittie.apps.mailer.tasks import send_email

from frittie.apps.app_settings import MESSAGE_PAGE
from frittie.apps.main.constants import MESSAGE_ITEM, CONVERSATION_ITEM
from frittie.settings import WEBSITE_HOMEPAGE

import json, logging

logger = logging.getLogger(__name__)

@dajaxice_register
def send_message(request,conversation_unique_id,message_content):
	results = {}
	try:
		user_login = get_user_login_object(request)
		conversation = Conversation.objects.get(unique_id=conversation_unique_id)
		user_chat = conversation.user1
		if conversation.user2.username != user_login.username:
			user_chat = conversation.user2
		new_message = Message.objects.create(user_send=user_login,
				user_receive=user_chat,unique_id=generate_unique_id("message"),
				content=message_content,status='1')
		conversation.messages.add(new_message)
		conversation.latest_message = new_message
		new_message.save()
		conversation.save()
		results['success'] = True
		results['snippet_return'] = generate_html_snippet(request,MESSAGE_PAGE[MESSAGE_ITEM],{"message": new_message})
		#results['success_message'] = generate_message("send_message","success",{})
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("send_message","error",{})	
	return json.dumps({'results': results})


@dajaxice_register
def compose_new_message(request,user_chat_username,message_content):
	results = {}
	try:
		user_login = get_user_login_object(request)
		user_chat = User.objects.get(username=user_chat_username)
		message = Message.objects.create(user_send=user_login,
			user_receive=user_chat,unique_id=generate_unique_id("message"),
			content=message_content,status="1")
		conversation = None
		conversations = Conversation.objects.filter(Q(user1=user_login,user2=user_chat) | Q(user1=user_chat,user2=user_login))
		if len(conversations) == 0:
			results['is_conversation_exist'] = False
			conversation = Conversation.objects.create(user1=user_login,user2=user_chat,unique_id=generate_unique_id("conversation"))
		else:
			results['is_conversation_exist'] = True
			conversation = conversations[0]
		conversation.messages.add(message)
		conversation.latest_message = message
		conversation.save()
		context = {
			"user_send": get_user_common_info(user_login),
			"message_content": message.content,
			"url": WEBSITE_HOMEPAGE + "messages/" + str(conversation.unique_id)
		}
		email_tracking = EmailTracking.objects.create(to_emails=user_chat.email,email_template="send_message",context_data=str(context))
		send_email.delay(email_tracking.id)

		results['success'] = True
		#results['email_tracking_id'] = email_tracking.id
		results['conversation_unique_id'] = conversation.unique_id
		results['snippet_return'] = generate_html_snippet(request,MESSAGE_PAGE[CONVERSATION_ITEM],{"conversation": conversation})
		#results['success_message'] = generate_message("send_message","success",{})
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("send_message","error",{})
	return json.dumps({'results': results})

