from django.db.models import Q
from cities_light.models import City
from frittie.apps.search.helper import build_city_autocomplete_data

import logging

logger = logging.getLogger(__name__)

city_autocomplete_data_normal = []
city_autocomplete_data_explore = []

def main():
	try:
		city_autocomplete_data_normal = build_city_autocomplete_data(City.objects.filter(Q(country__code2="US")|Q(country__code2="VN")),[],False)
		city_autocomplete_data_explore = build_city_autocomplete_data(City.objects.filter(Q(country__code2="US")|Q(country__code2="VN")),[],True)
	except Exception as e:
		logger.exception(e)

if __name__ == "__main__":
	main()

