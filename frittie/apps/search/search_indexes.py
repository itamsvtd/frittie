from haystack import indexes

from django.contrib.auth.models import User
from django.db.models import Q

from frittie.apps.main.models import Location, UserProfile, Activity
from frittie.apps.main.constants import PUBLIC

from cities_light.models import City

class LocationIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    author = indexes.CharField(model_attr='create_by')
    content = indexes.CharField(model_attr="name")
    #content_auto = indexes.EdgeNgramField(model_attr='name')
    #coordinate = indexes.LocationField(model_attr='get_coordinate')

    def get_model(self):
        return Location

  	def index_queryset(self, using=None):
		return self.get_model().objects.all()	

   
class ActivityIndex(indexes.SearchIndex, indexes.Indexable):
	text = indexes.CharField(document=True, use_template=True)
	author = indexes.CharField(model_attr='user_create')
	pub_date = indexes.DateTimeField(model_attr='start_time')
	#location = indexes.LocationField(model_attr='get_location_coordinate')
	content = indexes.CharField(model_attr="name")
	#content_auto = indexes.EdgeNgramField(model_attr='name')
	
	def get_model(self):
		return Activity

	def index_queryset(self, using=None):
		return self.get_model().objects.filter(activity_type=PUBLIC)


class CityIndex(indexes.SearchIndex, indexes.Indexable):
	text = indexes.CharField(document=True,use_template=True)
	content = indexes.CharField(model_attr="search_names")
	#content_auto = indexes.EdgeNgramField(model_attr="display_name")

	def get_model(self):
		return City

	def index_queryset(self,using=None):
		return self.get_model().objects.filter(Q(country__code2="US") | Q(country__code2="VN"))

class UserProfileIndex(indexes.SearchIndex, indexes.Indexable):
	text = indexes.CharField(document=True,use_template=True)
	content = indexes.CharField(model_attr="get_user_fullname")
	#content_auto = indexes.EdgeNgramField(model_attr='get_user_fullname')

	def get_model(self):
		return UserProfile

	def index_queryset(self, using=None):
		return self.get_model().objects.all()

