from random import choice, randint
from string import lowercase, uppercase

def generate_string(string_length, is_uppercase):
	"Generate string"
	if is_uppercase:
		return "".join(choice(uppercase) for i in xrange(int(string_length)))
	return "".join(choice(lowercase) for i in xrange(int(string_length)))

def rand_int(a,b):
	return randint(a,b)

