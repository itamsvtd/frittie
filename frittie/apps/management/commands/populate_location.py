from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from frittie.apps.app_helper import generate_unique_id
from frittie.apps.management import commands_helper
from frittie.apps.main.constants import LOCATION_CATEGORY
from frittie.apps.main.models import Location, LocationBusinessHour, LocationDetailInfo

class Command(BaseCommand):
    args = '<foo bar ...>'
    help = 'our help string comes here'

    def _create_locations(self, num_locations):
    	""
    	print "Creating %s locations..." %(num_locations)
    	num_locations = int(num_locations)
    	for i in xrange(num_locations):
    		location_name = "Generated_" + commands_helper.generate_string(5, True)
    		description = commands_helper.generate_string(100, False)
    		category = LOCATION_CATEGORY[commands_helper.rand_int(0,len(LOCATION_CATEGORY)-1)][0]
    		address1 = commands_helper.generate_string(20, False)
    		city = commands_helper.generate_string(10, True)
    		country = commands_helper.generate_string(10, True)
    		lat = commands_helper.rand_int(-90,90)
    		lng = commands_helper.rand_int(-179,180)
    		created_by = User.objects.get(username="admin")
    		location = Location(unique_id=generate_unique_id("location"),name=location_name,description=description,
					category=category,address1=address1,address2="",city=city,country=country,
					lat=lat,lng=lng,website_link="http://abc.com",create_by=created_by,phone="",google_place_id="")
    		location.save()
    	print "Finish creating locations..."

    def handle(self, *args, **options):
        self._create_locations(args[0])