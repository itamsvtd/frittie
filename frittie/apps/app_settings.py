NUM_PHOTO_PER_LOAD = 25

NUM_ITEM_PER_PAGE = 20

PEOPLE_SEARCH_TEMPLATE = "apps/search/page/people_search_page.html"
LOCATION_SEARCH_TEMPLATE = "apps/search/page/location_search_page.html"
ACTIVITY_SEARCH_TEMPLATE = "apps/search/page/activity_search_page.html"


LOCATION_PAGE = {
	"list_activity": "list_activity_location",
	"comment_item": "comment_item_location",
}

ACTIVITY_PAGE = {
	"comment_item": "comment_item_activity",
	#"preview_calculate_ticket_item": "preview_calculate_ticket",
}

WATCH_LIST_PAGE = {
	"watch_list_item": "watch_list_item_activity",
}

NOTIFICATION_PAGE = {
	"list_notification": "list_notification"
}

MESSAGE_PAGE = {
	"message_item": "message_item",
	"conversation_item": "conversation_item"
}

MEMBER_PAGE = {
	"list_activity": "list_activity_member",
	"list_location": "list_location_member",
	"list_user": "list_user_member",
	"list_media": "list_media_member",
	"list_media_thumbnail": "list_media_thumbnail_member",
	"list_ticket": "list_ticket_member"
}


MESSAGE_SNIPPET_TEMPLATE = 	{	
	# Auth app
	"signup_success": "texts/message/apps/auth/signup_success.html",
	"confirm_email_success": "texts/message/apps/auth/confirm_email_success.html",
	"confirm_email_asking": "texts/message/apps/auth/confirm_email_asking.html",
	"resend_confirm_email_success": "texts/message/apps/auth/resend_confirm_email_success.html",
	"resend_confirm_email_error": "texts/message/apps/auth/resend_confirm_email_error.html",
	"change_password_success": "texts/message/apps/auth/change_password_success.html",
	"social_login_error": "texts/message/apps/auth/social_login_error.html",
	"reset_password_success": "texts/message/apps/auth/reset_password_success.html",

	# Location page
	"delete_location_error": "texts/message/apps/location/delete_location_error.html",
	"delete_location_success": "texts/message/apps/location/delete_location_success.html",
	"create_location_error": "texts/message/apps/location/create_location_error.html",
	"create_location_no_picture_error": "texts/message/apps/location/create_location_no_picture_error.html",
	"create_location_success": "texts/message/apps/location/create_location_success.html",
	"edit_location_error": "texts/message/apps/location/edit_location_error.html",
	"edit_location_success": "texts/message/apps/location/edit_location_success.html",

	# Member page
	"edit_profile_fail" : "texts/message/apps/member/edit_profile_fail.html",
	"edit_profile_success": "texts/message/apps/member/edit_profile_success.html",

	# Activity page
	'create_activity_success': "texts/message/apps/activity/create_activity_success.html",
	"create_activity_error": "texts/message/apps/activity/create_activity_error.html",
	"request_join_activity_success": "texts/message/apps/activity/request_join_activity_success.html",
	"request_join_activity_already": "texts/message/apps/activity/request_join_activity_already.html",
	"request_accepted_already": "texts/message/apps/activity/request_accepted_already.html",
	"cancel_request_join_activity_success": "texts/message/apps/activity/cancel_request_join_activity_success.html",
	"activity_limit_fail": "texts/message/apps/activity/activity_limit_fail.html",
	"accept_invite_activity_success": "texts/message/apps/activity/accept_invite_activity_success.html",
	"decline_invite_activity_success": "texts/message/apps/activity/decline_invite_activity_success.html",
	"update_logo_success": "texts/message/apps/activity/update_logo_success.html",
	"update_logo_error": "texts/message/apps/activity/update_logo_error.html",
	"activity_confirmation_success": "texts/message/apps/activity/activity_confirmation_success.html",
	"confirmation_not_match": "texts/message/apps/activity/confirmation_not_match.html",
	"ticket_confirmed_already": "texts/message/apps/activity/ticket_confirmed_already.html",
	"send_invitation_success": "texts/message/apps/activity/send_invitation_success.html",
	"add_ticket_success": "texts/message/apps/activity/add_ticket_success.html",
	"add_ticket_error": "texts/message/apps/activity/add_ticket_error.html",
	"update_activity_success": "texts/message/apps/activity/update_activity_success.html",
	"update_activity_success_need_publish": "texts/message/apps/activity/update_activity_success_need_publish.html",
	"update_activity_error": "texts/message/apps/activity/update_activity_error.html",
	"delete_activity_error": "texts/message/apps/activity/delete_activity_error.html",
	'publish_activity_success': 'texts/message/apps/activity/publish_activity_success.html',
	"publish_activity_error": 'texts/message/apps/activity/publish_activity_error.html',
	"reserve_ticket_success": "texts/message/apps/activity/reserve_ticket_success.html",
	"reserve_ticket_error": "texts/message/apps/activity/reserve_ticket_error.html",

	# Message page
	#"send_message_success": "texts/message/apps/message/send_message_success.html",
	"send_message_error": "texts/message/apps/message/send_message_error.html",

	# Photo
	#"delete_photo_success": "texts/message/apps/photo/delete_photo_success.html",
	#"add_photo_success": "texts/message/apps/photo/add_photo_success.html",
	"add_photo_file_size_error": "texts/message/apps/photo/add_photo_file_size_error.html",
	"add_photo_error": "texts/message/apps/photo/add_photo_error.html",

	# Common
	"ajax_request_fail": "texts/message/common/ajax_request_fail.html",
	"report_success": "texts/message/common/report_success.html",
}

HTML_SNIPPET_TEMPLATE = {
	# Activity snippet
	"comment_item_activity": "apps/activity/snippet/comment_item.html",
	"attending_activity_button": "apps/activity/snippet/attending_btn.html",
	"request_join_activity_button": "apps/activity/snippet/request_join_btn.html",
	"user_going_item": "apps/activity/snippet/user_going_item.html",

	# Watchlist snippet
	"watch_list_item_activity": "apps/watchlist/snippet/watch_list_item.html",

	# Location snippet 
	"comment_item_location": "apps/location/snippet/comment_item.html",
	"business_hours_location": "apps/location/snippet/business_hours.html",
	"detail_info_location": "apps/location/snippet/detail_info.html",
	"list_activity_location": "apps/location/snippet/list_activity.html",
	
	# Notification snippet
	"list_notification": "apps/notification/snippet/list_notification.html",
	
	# Message snippet
	"message_item": "apps/message/snippet/message_item.html",
	"conversation_item": "apps/message/snippet/conversation_item.html",

	# Member page snippet
	"list_activity_member": "apps/member/snippet/list_activity.html",
	"list_location_member": "apps/member/snippet/list_location.html",
	"list_user_member": "apps/member/snippet/list_user.html",
	"list_media_member": "apps/member/snippet/list_media.html",
	"list_media_thumbnail_member": "apps/member/snippet/list_media_thumbnail.html",
	"list_ticket_member": "apps/member/snippet/list_ticket.html",

	# Photo
	"list_photo_thumbnail": "apps/photo/snippet/list_photo_thumbnail.html",

	# Ticket
	"ticket_item": "apps/ticket/snippet/ticket_item.html",
	"preview_calculate_ticket": "apps/ticket/snippet/preview_calculate_ticket_item.html",

	# Home page
	"list_activity_homepage": "apps/main/snippet/list_activity.html",
	"list_location_homepage": "apps/main/snippet/list_location.html",

	# Common
	"preview_activity_modal": "common/modal/preview_activity.html",
	"list_activity_map": "common/map/activity_list_item.html",

}

EMAIL_SUBJECT_SNIPPET_TEMPLATE = {

	# Message
	"send_message": "texts/email/apps/message/send_message_subject.txt",
	
	# Activity
	"host_approve_send_confirmation" : "texts/email/apps/activity/host_approve_send_confirmation_subject.txt",
	"accept_invite_send_confirmation": "texts/email/apps/activity/accept_invite_send_confirmation_subject.txt",
	"request_join_activity": "texts/email/apps/activity/request_join_activity_subject.txt",
	"cancel_attending_activity": "texts/email/apps/activity/cancel_attending_activity_subject.txt",
	"remind_followers":"texts/email/apps/activity/activity_remind_followers_subject.txt",
	"accept_invite_notify_host": "texts/email/apps/activity/accept_invite_notify_host_subject.txt",
	"decline_invite_notify_host": "texts/email/apps/activity/decline_invite_notify_host_subject.txt",
	"invite_activity": "texts/email/apps/activity/invite_activity_subject.txt",

	"remind_upcoming_activities":"texts/email/apps/activity/remind_upcoming_activities_subject.txt",

	"reserve_ticket_confirmation": "texts/email/apps/activity/reserve_ticket_confirmation_subject.txt",
	"user_reserve_ticket_notice": "texts/email/apps/activity/user_reserve_ticket_notice_subject.txt",
}

EMAIL_CONTENT_HTML_SNIPPET_TEMPLATE = {

	# Message
	"send_message": "texts/email/apps/message/send_message_content.html",
	
	# Activity
	"host_approve_send_confirmation" 	: "texts/email/apps/activity/host_approve_send_confirmation_content.html",
	"accept_invite_send_confirmation"	: "texts/email/apps/activity/accept_invite_send_confirmation_content.html",
	"request_join_activity"				: "texts/email/apps/activity/request_join_activity_content.html",
	"cancel_attending_activity"			: "texts/email/apps/activity/cancel_attending_activity_content.html",
	"remind_followers"					: "texts/email/apps/activity/activity_remind_followers_content.html",
	"accept_invite_notify_host"			: "texts/email/apps/activity/accept_invite_notify_host_content.html",
	"decline_invite_notify_host"		: "texts/email/apps/activity/decline_invite_notify_host_content.html",
	"invite_activity"					: "texts/email/apps/activity/invite_activity_content.html",

	"remind_upcoming_activities"		: "texts/email/apps/activity/remind_upcoming_activities_content.html",

	"reserve_ticket_confirmation"		: "texts/email/apps/activity/reserve_ticket_confirmation_content.html",
	"user_reserve_ticket_notice"		: "texts/email/apps/activity/user_reserve_ticket_notice_content.html",
}

EMAIL_CONTENT_TEXT_SNIPPET_TEMPLATE = {
	# Message
	"send_message": "texts/email/apps/message/send_message_content.txt",
	
	# Activity
	"host_approve_send_confirmation" : "texts/email/apps/activity/host_approve_send_confirmation_content.txt",
	"accept_invite_send_confirmation": "texts/email/apps/activity/accept_invite_send_confirmation_content.txt",
	"request_join_activity": "texts/email/apps/activity/request_join_activity_content.txt",
	"cancel_attending_activity": "texts/email/apps/activity/cancel_attending_activity_content.txt",
	"remind_followers": "texts/email/apps/activity/activity_remind_followers_content.txt",
	"accept_invite_notify_host": "texts/email/apps/activity/accept_invite_notify_host_content.txt",
	"decline_invite_notify_host": "texts/email/apps/activity/decline_invite_notify_host_content.txt",
	"invite_activity": "texts/email/apps/activity/invite_activity_content.txt",

	"remind_upcoming_activities" : "texts/email/apps/activity/remind_upcoming_activities_content.txt",

	"reserve_ticket_confirmation": "texts/email/apps/activity/reserve_ticket_confirmation_content.txt",
	"user_reserve_ticket_notice": "texts/email/apps/activity/user_reserve_ticket_notice_content.txt",
}

NOTIFICATION_SNIPPET_TEMPLATE = {
	"location_have_user_comment": "texts/notification/location_have_user_comment.html",

	"activity_have_user_comment": "texts/notification/activity_have_user_comment.html",
	"activity_have_user_request_join": "texts/notification/activity_have_user_request_join.html",
	"activity_have_user_cancel_attending": "texts/notification/activity_have_user_cancel_attending.html",
	"activity_have_user_accept_invite": "texts/notification/activity_have_user_accept_invite.html",
	"activity_have_host_accept_request": "texts/notification/activity_have_host_accept_request.html",
	"activity_have_host_invite": "texts/notification/activity_have_host_invite.html"
}

FEED_SNIPPET_TEMPLATE = {
	"comment_location": "texts/feed/comment_location.html",
	"comment_activity": "texts/feed/comment_activity.html",
	"follow_location": "texts/feed/follow_location.html",
	"follow_user": "texts/feed/follow_user.html",
	"join_activity": "texts/feed/join_activity.html",
	"create_activity": "texts/feed/create_activity.html",
	"update_activity": "texts/feed/update_activity.html",
}

SEARCH_TYPE = { "activity":None,"people":None,"location":None }

KEYWORDS_URL = [
	'admin','signup','login','password',"accounts"
	'logout','confirm_email','search','settings',
	'buzz','messages',"about",'api','asset','photo',
	'feeds','friends'
]

MODEL_KEY_LIST = [
	"LO", "AC", "IV", "PT", "VD", "CM", "CN", "RP", "NF", "MG", "FD", "DI", "UN"
]

MODEL_KEY_MAP = {
	"location": "LO",
	"activity": "AC",
	"invitation": "IV",
	"photo": "PT",
	"video": "VD",
	"comment": "CM",
	"conversation": "CN",
	"report": "RP",
	"notification": "NF",
	"message": "MG",
	"feed": "FD",
	"default_image": "DI",
}

UNSPECIFIED_MODEL_KEY = "UN"


DEFAULT_IMAGE_PATH_MAPPING = {

	# User Image
	"default_male_icon": "default/img/user/male_icon.png",
	"default_female_icon": "default/img/user/female_icon.png",
	"default_cover_picture": 'default/img/user/cover_picture.png',

	# Location Image
	"location_category_1": "default/img/location/location_category_1.png",
	"location_category_2": "default/img/location/location_category_2.png",
	"location_category_3": "default/img/location/location_category_3.png",
	"location_category_4": "default/img/location/location_category_4.png",
	"location_category_5": "default/img/location/location_category_5.png",
	"location_category_6": "default/img/location/location_category_6.png",
	"location_category_7": "default/img/location/location_category_7.png",
	"location_category_8": "default/img/location/location_category_8.png",
	"location_category_9": "default/img/location/location_category_9.png",
	"location_category_10": "default/img/location/location_category_10.png",

	# Activity Image
	"activity_category_1": "default/img/activity/activity_category_1.png",
	"activity_category_2": "default/img/activity/activity_category_2.png",
	"activity_category_3": "default/img/activity/activity_category_3.png",
	"activity_category_4": "default/img/activity/activity_category_4.png",
	"activity_category_5": "default/img/activity/activity_category_5.png",
	"activity_category_6": "default/img/activity/activity_category_6.png",
	"activity_category_7": "default/img/activity/activity_category_7.png",
	"activity_category_8": "default/img/activity/activity_category_8.png",
	"activity_category_9": "default/img/activity/activity_category_9.png",
}


DEFAULT_IMAGE_UNIQUE_ID = {

	"default_male_icon": "DIBHgn2pXkaWLAYgpRsQGTo3088",
	"default_female_icon": "DIJE4S63KnuKozEq4BsC2PH4019",
	"default_cover_picture": 'DInXsK7dXR9BGXpmKR9Mhd6D124',

	"location_category_1": "DIHikKPPoKpEZp97cw2jGJyJ071",
	"location_category_2":"DI9jGzmWcrSCKsHYiwL6BkPj090",
	"location_category_3":"DIvKN9RoXXBGdQoz2GVr9T65096",
	"location_category_4":"DIQvAbExkEXhzkLNQBFCoHhk039",
	"location_category_5":"DIeGxnoUHWusmNtwMFFFJRkQ022",
	"location_category_6":"DI734uiws4UapmTHY5xakBQQ012",
	"location_category_7":"DIsRKr7Hq5C2KLbNzTPePnih041",
	"location_category_8":"DIT9JBiMPcKjUW4jh7ge2ZoF006",
	"location_category_9":"DIthxaS5tfUpkSq7wvyTz3bY037",
	"location_category_10":"DITvFQ7v9Wd64eMD4fNhXDeC041",

	"activity_category_1": "DIch6XW3c2g6RkMoLZfcVFUH033",
	"activity_category_2": "DIumuoYHXabx9hDsuyf9hkRX775",
	"activity_category_3": "DIwDysEgkHHKSr9szSsCZyzH193",
	"activity_category_4": "DIkLLsyFREFqDCMLTa4VdjEc839",
	"activity_category_5": "DIMToqk83S7R52TRe6akY3ph231",
	"activity_category_6": "DI3e5h4U5Br2fcUvTubWe2Dx041",
	"activity_category_7": "DIyzgrJAeukQEZLaHsfSrGTf746",
	"activity_category_8": "DIhvMScsr5zUrzXR4cvvs4tj234",
	"activity_category_9": "DIBaeZAYSWnzpT8DeTEF8EZ7457",
}






