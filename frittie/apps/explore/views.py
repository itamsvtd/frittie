###############################
#  Author: Dang Nguyen,Duc Vu #
#  Date: 5/24/2012            #
#  Last Modified: 10/04/2013  #
###############################

from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.utils import simplejson
from django.core import serializers

from frittie.apps.activity.helper import get_nearby_activity

APP_NAME = "explore"

def main_page(request):
	context_instance = RequestContext(request)
	data = { "is_explore": True, "app_name": APP_NAME }
	if "lat" in request.GET and "lng" in request.GET and "city" in request.GET:
		data['activities'] = get_nearby_activity(request,request.GET['lat'],request.GET['lng'])
		context_instance['current_lat'] = request.GET['lat']
		context_instance['current_lng'] = request.GET['lng']
		context_instance['current_city'] = request.GET['city']
	else:
		data['activities'] = get_nearby_activity(request)
	return render_to_response("apps/explore/page/main_page.html",data,context_instance=context_instance)

