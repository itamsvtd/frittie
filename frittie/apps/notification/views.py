from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.contrib.auth.models import User
from django.core import serializers

from frittie.apps.main.models import Notification
from frittie.apps.app_helper import convert_queryset_to_list, get_user_login_object

import copy
import datetime

@login_required()
def main_page(request):
	data = {}
	data['is_no_notification'] = True
	user_login = get_user_login_object(request)
	new_notifications = Notification.objects.filter(notify_to=user_login,notify_from__profile__privacy_status="1",status="1").order_by("-date")
	old_notifications = convert_queryset_to_list(Notification.objects.filter(notify_to=user_login,notify_from__profile__privacy_status="1",status="0").order_by("-date"))
	data['old_notifications'] = old_notifications
	data['new_notifications'] = new_notifications
	if len(new_notifications) > 0 or len(old_notifications) > 0:
		data['is_no_notification'] = False
	for notification in new_notifications:
		notification.status = "0"
		notification.save()
	return render_to_response("apps/notification/page/main_page.html",data,context_instance=RequestContext(request))

