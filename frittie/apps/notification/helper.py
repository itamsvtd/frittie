from django.template.loader import render_to_string

from frittie.apps.main.models import Notification
from frittie.apps.main.constants import OBJECT_VERB

from frittie.apps.app_settings import NOTIFICATION_SNIPPET_TEMPLATE
from frittie.apps.app_helper import get_user_login_object, generate_unique_id

import uuid

'''
	@param: subj - The subject of the notification. 
				   A dictionary which have required 
				   main subject
	
	@param:	action - The action of the notification.
					 A dictionary which have required main 
					 action and optional extra data 
	
	@param: obj - The object of the notification.
				  A dictionary which have required
				  main object, object type, display 
				  name and related object

	@param force - force send notification regardless
				   main subject privacy. Used in request
				   join activity	
'''
def generate_notification_helper(subj,action,obj,force=False,extra_context=None,template=None):
	data = {
		"subject": subj['main'],
		"action": action,
		"object": obj['main'],
	}

	if extra_context != None:
		data["extra_context"] = extra_context

	if force == False:
		# If main subject is private user, dont generate notification
		if subj['main'].get_profile().privacy_status == "0": return 

	if len(obj['related']) > 0:
		if template == None:
			template = obj['display'] + "_" + OBJECT_VERB + "_" + subj['display'] + "_" + action
		notification_content = render_to_string(NOTIFICATION_SNIPPET_TEMPLATE[template],data)
		notification = Notification.objects.create(unique_id=generate_unique_id("notification"),
					content=notification_content,notification_type=obj['object_type'],notify_from=subj['main'])
		if subj['main'] in obj['related']:
			obj['related'].remove(subj['main'])
		notification.notify_to.add(*obj['related'])
		notification.save()


def get_new_notifications_count(request):
	user_login = get_user_login_object(request)
	return Notification.objects.filter(notify_to=user_login,status="1").count()
