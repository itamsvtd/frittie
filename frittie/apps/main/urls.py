from django.conf.urls import *
from frittie import settings

urlpatterns = patterns('frittie.apps.main.views',
    url(r"^$", "main_page"),
)
