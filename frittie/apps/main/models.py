from django.db import models
from django.db.models import Count
from django.contrib.auth.models import User
from django.contrib import admin
from django.db.models.signals import post_save
from django.conf import settings
from django.utils import timezone
from django.contrib.gis.geos import Point
from django.template.loader import render_to_string
from django.db.models import Q
from django.core.mail import get_connection, EmailMultiAlternatives
from django.core.mail.backends.smtp import EmailBackend as DjangoSmtpEmailBackend
	
from utils import get_elapse_time_text, is_empty

from constants import PHOTO_TYPE, APPEARANCE_STATUS 
from constants import NOTIFICATION_TYPE, STATUS, POST_TYPE
from constants import COMMENT_TYPE, REPORT_TYPE, GENDER, USA_STATES
from constants import PRIVACY_STATUS, LOCATION_CATEGORY, ACTIVITY_TYPE
from constants import PRIVACY_OPTIONS, ACTIVITY_REQUEST_STATUS, LOCATION_CATEGORY_MAP
from constants import DEFAULT_COUNTRIES_FORMAT, FEED_OBJECT_TYPE
from constants import TICKET_STATUS, ACTIVITY_CATEGORY, WEEK_DAY
from constants import ACTIVITY_CATEGORY_MAP, VIDEO_TYPE ,ENTER_ACTIVITY
from constants import REQUEST_WAITING, REQUEST_ACCEPT, REQUEST_INVITED
from constants import TICKET_OPEN, UNLIMITED, STATUS_NEW, PUBLIC, CURRENCY
from constants import WEEK_DAY_NUM_MAP, DEFAULT_SERVER_EMAIL, DEFAULT_SITE_NAME

from frittie.apps.app_settings import EMAIL_SUBJECT_SNIPPET_TEMPLATE, MODEL_KEY_LIST
from frittie.apps.app_settings import EMAIL_CONTENT_HTML_SNIPPET_TEMPLATE, EMAIL_CONTENT_TEXT_SNIPPET_TEMPLATE
  
from frittie.apps.friend.provider.facebook_provider import FacebookFriendsProvider

from frittie.settings import MEDIA_URL, SITE_DOMAIN, SERVER_EMAIL
from frittie.settings import SITE_NAME, MANDRILL_API_KEY, MAX_MANDRILL_EMAIL_ALLOW
from frittie.settings import EMAIL_PORT, EMAIL_HOST_USER, EMAIL_HOST_PASSWORD, EMAIL_USE_TLS

from allauth.socialaccount.models import SocialAccount

from django.db.models import Avg, Max, Min

import datetime, random, uuid, logging, json, ast, mandrill


logger = logging.getLogger(__name__)

try:
	storage = settings.MULTI_IMAGES_FOLDER + '/'
except AttributeError:
	storage = 'upload_storage/'


class SocialFriendsManager(models.Manager):
    def assert_user_is_social_auth_user(self, user):
        if not isinstance(user, SocialAccount):
            raise TypeError("user must be UserSocialAuth instance, not %s" % user)

    def fetch_social_friends(self, social_auth_user):
        self.assert_user_is_social_auth_user(social_auth_user)
        friends_provider = FacebookFriendsProvider()
        friends = friends_provider.fetch_friends_data(social_auth_user)
        return friends

    def existing_social_friends(self, user_social_auth=None, friends=None):
        self.assert_user_is_social_auth_user(user_social_auth)
        if not friends:
            friends = self.fetch_social_friends(user_social_auth)
        # Convert comma sepearated string to the list
        if isinstance(friends, basestring):
            friends = eval(friends)
        return User.objects.filter(socialaccount__uid__in=friends).all()  


    def get_or_create_with_social_auth(self, social_auth):
        self.assert_user_is_social_auth_user(social_auth)
        try:
            social_friend_list = self.filter(user_social_auth=social_auth).get()
            friends = social_friend_list.friends
            if len(friends) == 0:
                social_friend_list.friends = self.fetch_social_friends(social_auth)
                social_friend_list.save()
        except:
            # if no record found, create a new one
            friends = self.fetch_social_friends(social_auth)
            social_friend_list = SocialFriendList()
            social_friend_list.friends = friends
            social_friend_list.user_social_auth = social_auth
            social_friend_list.save()
        return social_friend_list

    def get_or_create_with_social_auths(self, social_auths):
        social_friend_coll = []
        for sa in social_auths:
            social_friend = self.get_or_create_with_social_auth(sa)
            social_friend_coll.append(social_friend)
        return social_friend_coll


class SocialFriendList(models.Model):
    user_social_auth = models.OneToOneField(SocialAccount, related_name="social_account")
    friends = models.TextField(blank=True)
    objects = SocialFriendsManager()

    def __unicode__(self):
        return "%s on %s" % (self.user_social_auth.user.username, self.user_social_auth.provider)

    def existing_social_friends(self):
        return SocialFriendList.objects.existing_social_friends(self.user_social_auth, self.friends)


class EmailTracking(models.Model):
	to_emails = models.TextField()
	email_template = models.CharField(max_length=200)
	subject = models.CharField(max_length=350,blank=True)
	text_content = models.TextField(blank=True)
	context_data = models.TextField()
	status = models.CharField(max_length=50,default="pending")
	send_time = models.DateTimeField(blank=True,null=True)

	def is_reach_mandrill_limit(self):
		try:
			mandrill_client = mandrill.Mandrill(MANDRILL_API_KEY)
			result = mandrill_client.users.info()
			last_month_sent = result['stats']['last_30_days']['sent']
			if last_month_sent >= MAX_MANDRILL_EMAIL_ALLOW:
				return True
		except Exception as e:
			logger.exception(e)
		return False

	def generate_email(self):
		try:
			context = ast.literal_eval(self.context_data)
			from_email = DEFAULT_SITE_NAME[0].upper() + DEFAULT_SITE_NAME[1:].lower() + " <" + DEFAULT_SERVER_EMAIL + ">"
			subject = render_to_string(EMAIL_SUBJECT_SNIPPET_TEMPLATE[self.email_template],context)
			text_content = render_to_string(EMAIL_CONTENT_TEXT_SNIPPET_TEMPLATE[self.email_template],context)
			html_content = render_to_string(EMAIL_CONTENT_HTML_SNIPPET_TEMPLATE[self.email_template],context)
			email = EmailMultiAlternatives(subject=subject,body=text_content,from_email=from_email,to=self.to_emails.split(","))
			email.attach_alternative(html_content, "text/html")
			email.content_subtype = "html"
			self.subject = subject
			self.text_content = text_content
			self.save()
			return email
		except Exception as e:
			logger.exception(e)
			raise

	def send_email(self):
		try:
			email = self.generate_email()
			email.send()
			response = email.mandrill_response[0]
			self.status = response['status']	
			self.send_time = datetime.datetime.now()
			self.save()
		except Exception as e:
			logger.exception(e)
			self.status = "fail"
			self.save()
			raise

class Photo(models.Model):
	unique_id = models.CharField(max_length=200)
	caption = models.TextField(blank=True)
	image = models.ImageField(upload_to=storage+"/photo/%Y/%m/%d")
	upload_date = models.DateTimeField(auto_now_add=True)
	user_post = models.ForeignKey(User)
	photo_type = models.CharField(max_length=50,choices=PHOTO_TYPE,blank=True)
	object_unique_id = models.CharField(max_length=200,blank=True)
	privacy_status = models.CharField(max_length=1,choices=PRIVACY_OPTIONS,default=PUBLIC)

	def __unicode__(self):
		return self.image.name
	

class Video(models.Model):
	unique_id = models.CharField(max_length=100)
	filename = models.CharField(max_length=60, blank=True, null=True)
	video = models.FileField(upload_to=storage+"/video/%Y/%m/%d")
	key_data = models.CharField(max_length=90, unique=True, blank=True, null=True)
	upload_date = models.DateTimeField(auto_now_add=True)
	user_post = models.ForeignKey(User)
	video_type = models.CharField(max_length=20,choices=VIDEO_TYPE)
	privacy_status = models.CharField(max_length=1,choices=PRIVACY_OPTIONS,default=PUBLIC)

	def __unicode__(self):
		return self.video.name

class ConfirmationActivity(models.Model):
	user_confirm = models.ForeignKey(User,related_name="confirmation_user")
	confirmation_id = models.CharField(max_length=20)
	is_confirmed = models.CharField(max_length=10,choices=ENTER_ACTIVITY)
	activity = models.ForeignKey("Activity",max_length=100,blank=True,null=True)

class Ticket(models.Model):
	price = models.DecimalField(max_digits=10, decimal_places=2)
	fee = models.DecimalField(max_digits=10,decimal_places=2)
	ticket_name = models.CharField(max_length=100)
	ticket_status = models.CharField(max_length=1,choices=TICKET_STATUS,default=TICKET_OPEN)
	currency = models.CharField(max_length=3,choices=CURRENCY,default="USD")
	total_available_quantity = models.IntegerField()
	maximum_quantity_transaction = models.IntegerField()

	def string_consecutive_transaction(self):
		s = [0]
		for i in range(0,self.maximum_quantity_transaction):
			s.append(i+1)
		return s

class TicketTransaction(models.Model):
	user = models.ForeignKey(User,related_name="transaction_users")
	ticket = models.ForeignKey(Ticket,related_name="transaction_ticket") 
	activity = models.ForeignKey("Activity",related_name='transaction_activity')
	quantity = models.IntegerField()
	total_cost = models.DecimalField(max_digits=10,decimal_places=2)
	transaction_date = models.DateTimeField(auto_now_add=True)

	def get_confirmation_id(self):
		confirmations = ConfirmationActivity.objects.filter(user_confirm=self.user,activity=self.activity)
		if len(confirmations) == 0:
			return None
		else:
			return confirmations[0].confirmation_id


class Message(models.Model):
	unique_id = models.CharField(max_length=100)
	user_send = models.ForeignKey(User,related_name="user_send")
	user_receive = models.ForeignKey(User,related_name="user_receive")
	content = models.TextField()
	status = models.CharField(max_length=1,choices=STATUS,default=STATUS_NEW)
	date = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.content

	def get_elapse_time(self):
		return get_elapse_time_text(self.date)


class Conversation(models.Model):
	unique_id = models.CharField(max_length=100)
	user1 = models.ForeignKey(User,related_name="user1")
	user2 = models.ForeignKey(User,related_name="user2")
	messages = models.ManyToManyField(Message,related_name="messages",null=True)
	latest_message = models.ForeignKey(Message,related_name="latest_message",null=True)


class Comment(models.Model):
	unique_id = models.CharField(max_length=100)
	comment_type = models.CharField(max_length=20,choices=COMMENT_TYPE)
	user = models.ForeignKey(User)
	content = models.TextField()
	create_date = models.DateTimeField(auto_now_add=True)
	edit_date =  models.DateTimeField(blank=True,null=True)
	reports = models.ManyToManyField("Report",related_name="comment_reports",blank=True,null=True)

	def get_create_elapse_time(self):
		return get_elapse_time_text(self.create_date)
	
	def get_edit_elapse_time(self):
		if self.edit_date != None:
			return get_elapse_time_text(self.edit_date)
		return None 


class Report(models.Model):
	unique_id = models.CharField(max_length=100)
	report_type = models.CharField(max_length=20,choices=REPORT_TYPE)
	user_report = models.ForeignKey(User,related_name='user_report',blank=True)
	date = models.DateTimeField(auto_now_add=True)
	report_content = models.TextField()
	
	
class Feed(models.Model):
	unique_id = models.CharField(max_length=100)
	content = models.TextField() 
	create_by = models.ForeignKey(User,related_name="feed_create_by")
	user_receive = models.ManyToManyField(User,related_name="feed_user_receive",blank=True,null=True)
	object_type = models.CharField(max_length=30,choices=FEED_OBJECT_TYPE)
	object_unique_id = models.CharField(max_length=100)
	date = models.DateTimeField(auto_now_add=True)

	def get_elapse_time(self):
		return get_elapse_time_text(self.date)


class Notification(models.Model):
	unique_id = models.CharField(max_length=100)
	content = models.TextField()
	status = models.CharField(max_length=1,choices=STATUS,default=STATUS_NEW)
	notification_type = models.CharField(max_length=30,choices=NOTIFICATION_TYPE,null=True)
	notify_to = models.ManyToManyField(User, related_name='notify_to',blank=True,null=True)
	notify_from = models.ForeignKey(User, related_name='notify_from')
	date = models.DateTimeField(auto_now_add=True)
	
	def get_elapse_time(self):
		return get_elapse_time_text(self.date)

class Invitation(models.Model):
	unique_id = models.CharField(max_length=100)
	message = models.TextField(blank=True)
	activity = models.ForeignKey('Activity',related_name="activity_invitation")
	send_from = models.ForeignKey(User, related_name='send_from')
	send_to_email = models.CharField(max_length=300)
	list_photos = models.ManyToManyField('Photo',blank=True)
	list_videos = models.ManyToManyField('Video',blank=True)

	def __unicode__(self):
		return self.activity.name


class ActivityRequest(models.Model):
	activity = models.ForeignKey("Activity")
	user_request = models.ForeignKey(User, related_name='activity_request_user')
	date = models.DateTimeField(auto_now_add=True)
	request_status = models.CharField(max_length=10,choices=ACTIVITY_REQUEST_STATUS,default=REQUEST_WAITING)
	introduction = models.TextField(blank=True)
	is_invite_request = models.BooleanField(default=False)

	def __unicode__(self):
		return self.activity.name


class ActivityCancelAttending(models.Model):
	activity = models.ForeignKey("Activity",related_name="activity_cancel")
	user_cancel = models.ForeignKey(User, related_name='activity_user_camcel')
	date = models.DateTimeField(auto_now_add=True)
	reason = models.TextField()


class ActivityUserRelation(models.Model):
	activity = models.ForeignKey('Activity')
	user = models.ForeignKey(User)
	status = models.CharField(max_length=1,choices=APPEARANCE_STATUS)


class Tag(models.Model):
	user_tag = models.ForeignKey(User, related_name="user_tag", blank=True,null=True) 
	content = models.CharField(max_length=20)


class GeneralToken(models.Model):
	key = models.CharField(max_length=100)
	token = models.TextField()
	token_type = models.CharField(max_length=50)
	create_date = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.token

class Post(models.Model):
	unique_id = models.CharField(max_length=100)
	content = models.TextField()
	post_type = models.CharField(max_length=50,choices=POST_TYPE)
	object_unique_id = models.CharField(max_length=100,blank=True)
	date = models.DateTimeField(auto_now_add=True)


class LocationDetailInfo(models.Model):
	location = models.ForeignKey('Location',related_name='location_detail_info')
	info = models.CharField(max_length=50)
	value = models.CharField(max_length=50)


class LocationBusinessHour(models.Model):
	location = models.ForeignKey('Location',related_name='location_open_hour')
	weekday = models.CharField(max_length=1,choices=WEEK_DAY)
	hour_start = models.CharField(max_length=50)
	hour_end = models.CharField(max_length=50)

class Location(models.Model):
	unique_id = models.CharField(max_length=100)
	name = models.CharField(max_length=50)
	description = models.TextField()
	category = models.CharField(max_length=30,choices=LOCATION_CATEGORY)
	address1 = models.CharField(max_length=100)
	address2 = models.CharField(max_length=100,blank=True)
	lat = models.DecimalField(max_digits=19, decimal_places=10)
	lng = models.DecimalField(max_digits=19, decimal_places=10)
	city = models.CharField(max_length=50)
	state = models.CharField(max_length=2,choices=USA_STATES,blank=True)
	zip_code = models.CharField(max_length=15,blank=True)
	country = models.CharField(max_length=50,choices=DEFAULT_COUNTRIES_FORMAT)
	website_link = models.URLField(blank=True)
	phone = models.CharField(max_length=20,blank=True)
	main_picture = models.ForeignKey("Photo",related_name="main_picture",blank=True,null=True)
	create_by = models.ForeignKey(User, related_name="create_by")
	follow_by = models.ManyToManyField(User, related_name="follow_by",blank=True,null=True)
	comments = models.ManyToManyField(Comment,related_name="location_comment",blank=True,null=True)
	list_photos = models.ManyToManyField(Photo,related_name="location_photos",blank=True)
	reports = models.ManyToManyField(Report,related_name='location_reports',blank=True,null=True)
	google_place_id = models.CharField(max_length=300,blank=True)

	class Meta:
		ordering = ['name']

	def __unicode__(self):
		return unicode(self.name)

	def get_coordinate(self):
		return Point(self.lat,self.lng)

	def get_ordered_posts(self):
		return self.posts.all().order_by("-date")

	def is_user_create(self,check_user):
		if check_user.username == self.create_by.username:
			return True
		return False

	def is_user_follow(self,check_user):
		user_folow = self.follow_by.all()
		for user in user_folow:
			if user.username == check_user.username:
				return True
		return False

	def get_latest_comments(self):
		return self.comments.all().order_by("-create_date")

	def get_all_activities(self):
		try:
			activities = Activity.objects.filter(location=self.unique_id).order_by("-start_time")
			return activities
		except Exception as e:
			logger.exception(e)
			return None

	def get_hot_activities(self,date=timezone.now()):
		try:
			activities = Activity.objects.filter(location=self.unique_id,start_time__gte=date).annotate(hot=Count("activity_requests")).order_by("-hot")
			return activities
		except Exception as e:
			logger.exception(e)
			return None

	def get_upcoming_activities(self,date=timezone.now()):
		try:
			activities = Activity.objects.filter(location=self.unique_id,start_time__gte=date).order_by("-start_time")
			return activities
		except Exception as e:
			logger.exception(e)
			return None 


	def get_similar_places(self):
		try:
			similar_places = []
			if self.country.lower() == "us":
				similar_places = list(Location.objects.filter(category=self.category,city__icontains=self.city,state=self.state,country=self.country).exclude(pk=self.pk))
			else:
				similar_places = list(Location.objects.filter(category=self.category,city__icontains=self.city,country=self.country).exclude(pk=self.pk))
			random.shuffle(similar_places)
			return similar_places[:5]
		except Exception as e:
			logger.exception(e)
			return None

	def get_category(self):
		return LOCATION_CATEGORY_MAP[self.category]

	def get_address(self):
		output = self.address1
		if is_empty(self.address2) == False: 
			output += ", " + self.address2 + "," 
		output += " " + self.city + ", "
		if is_empty(self.state) == False and self.zip_code != None:
			output += self.state + " " + str(self.zip_code)
		else:
			output += self.country
		return output

	def get_business_hours(self):
		business_hours = LocationBusinessHour.objects.filter(location=self)
		weekday = {}
		for business_hour in business_hours:
			data = {
				"id": business_hour.id,
				"display":business_hour.hour_start + " - " + business_hour.hour_end
			}
			if int(business_hour.weekday) in weekday:
				weekday[int(business_hour.weekday)].append(data)
			else:
				weekday[int(business_hour.weekday)] = []
				weekday[int(business_hour.weekday)].append(data)
		return weekday

	def get_detail_info(self):
		return LocationDetailInfo.objects.filter(location=self)

class Activity(models.Model):
	unique_id = models.CharField(max_length=200)
	name = models.CharField(max_length=100)
	description = models.TextField()
	create_time = models.DateTimeField(auto_now_add=True)
	timezone = models.CharField(max_length=30)
	start_time = models.DateTimeField()
	end_time = models.DateTimeField(blank=True,null=True)
	logo = models.ForeignKey("Photo",related_name="logo", blank=True,null=True)
	user_create = models.ForeignKey(User, related_name="user_create")    
	users_watch = models.ManyToManyField(User,related_name="user_watch",blank=True,null=True)     
	activity_requests = models.ManyToManyField("ActivityRequest",related_name="activity_requests",blank=True,null=True)
	location = models.CharField(max_length=300)
	location_additional_info = models.TextField(blank=True)
	limit = models.IntegerField(default=UNLIMITED)                       # Limit how many people can join this
	privacy_option = models.CharField(max_length=1,choices=PRIVACY_OPTIONS,default=PUBLIC)    # public or private activity
	category = models.CharField(max_length=30,choices=ACTIVITY_CATEGORY)
	tickets = models.ManyToManyField(Ticket,related_name='ticket',blank=True)
	confirmations = models.ManyToManyField(ConfirmationActivity,related_name="activity_confirmation",blank=True,null=True)
	activity_type = models.CharField(max_length=10,choices=ACTIVITY_TYPE)
	list_photos = models.ManyToManyField(Photo,related_name="activity_photos",blank=True)
	comments = models.ManyToManyField(Comment,related_name="activity_comment",blank=True,null=True)
	tags = models.ManyToManyField(Tag,related_name="tags",blank=True,null=True)
	reports = models.ManyToManyField(Report,related_name='activity_reports',blank=True,null=True)
	posts = models.ManyToManyField(Post,related_name="activity_posts",blank=True,null=True)
	tokens = models.ManyToManyField(GeneralToken,related_name="activity_tokens",blank=True,null=True)
	is_publish = models.BooleanField(default=False)
	is_repeated = models.BooleanField(default=False)
	last_update = models.DateTimeField(blank=True,null=True)
	ticket_policy = models.TextField(blank=True)

	class Meta:
		ordering = ['name']

	def __unicode__(self):
		return unicode(self.name)

	def get_location_coordinate(self):
		return Point(self.location.lat,self.location.lng)

	def get_ordered_posts(self):
		return self.posts.all().order_by("-date")[:10]

	def get_activity_status(self):
		now = timezone.now()
		end_time = self.start_time + timezone.timedelta(hours=3)		# If no end time specified, set to 3 hrs after start time
		if self.end_time != None: end_time = self.end_time
		if self.start_time > now: return "upcoming"
		if end_time < now: return "past"
		if self.start_time < now and end_time > now: return "happening"

	def get_people_going_name(self):
		u = User.objects.filter(activity_request_user__activity=self,activity_request_user__request_status=REQUEST_ACCEPT)
		list_user_names = ''
		count = 0 
		for i in u :
			count = count + 1
			if count > 3 :
				break
			list_user_names = ''.join([i.username,list_user_names])
		return list_user_names


	def get_people_going(self):
		return User.objects.filter(activity_request_user__activity=self,activity_request_user__request_status=REQUEST_ACCEPT)

	def get_total_people_going(self):
		return User.objects.filter(activity_request_user__activity=self,activity_request_user__request_status=REQUEST_ACCEPT).count()

	def get_watchers(self):
		return self.users_watch.all()

	def get_activity_waiting_requests(self):
		return self.activity_requests.filter(request_status=REQUEST_WAITING)

	def get_activity_invite_requests(self):
		return self.activity_requests.filter(is_invite_request=True)

	def get_category(self):
		return ACTIVITY_CATEGORY_MAP[self.category]

	def get_recent_comments(self):
		return self.comments.all().order_by("-create_date")[:5]

	def get_latest_comments(self):
		return self.comments.all().order_by("-create_date")

	def is_watching(self,user):
		return bool(user.get_profile().watchlist.filter(id=self.pk).count())

	def is_joining(self,user):
		return bool(ActivityRequest.objects.filter(activity=self,user_request=user,request_status=REQUEST_ACCEPT).count())

	def is_waiting(self,user):
		return bool(ActivityRequest.objects.filter(activity=self,user_request=user,request_status=REQUEST_WAITING).count())

	def is_invited(self,user):
		return bool(ActivityRequest.objects.filter(activity=self,user_request=user,request_status=REQUEST_INVITED).count())

	def is_user_made_request(self,user):
		try:
			activity_requests_count = ActivityRequest.objects.filter(actvitiy=self,user_request=user).count()
			if activity_requests_count == 1:
				return True
		except Exception as e:
			logger.exception(e)
		return False
		
	def is_attendable(self):
		available_spot = self.get_available_spot()
		if available_spot == UNLIMITED: return True
		if available_spot > 0: return True
		return False

	def get_user_join_amount(self):
		try:
			return len(Activity.objects.get(pk=self.pk).user_join.all())
		except Exception as e:
			print e
			return None

	def get_user_invite_amount(self):
		try:
			return len(Activity.objects.get(pk=self.pk).user_invite.all())
		except Exception as e:
			print e
			return None

	def get_available_spot(self):
		if self.limit != UNLIMITED:
			return self.limit - int(self.activity_requests.filter( Q(request_status=REQUEST_ACCEPT) | Q(request_status=REQUEST_INVITED)).count())
		return UNLIMITED

	def get_shorten_description(self):
		text = self.description
		if len(text) > 120:
			new_text = text[0:120]
			last_blank = new_text.rfind(" ")
			return new_text[0:last_blank] + '...'
		return text

	def get_activity_min_pk_ticket(self):
		s = self.tickets.aggregate(Min('pk'))
		return s['pk__min']

	def get_activity_max_pk_ticket(self):
		s = self.tickets.aggregate(Max('pk'))
		return s['pk__max']
		
	def is_location_model(self):
		s = self.location
		result = True
		if len(s) != 27 or s[24:27].isdigit() == False or s[0:2].upper() not in MODEL_KEY_LIST: 
			result = False
		return result

	def has_location_additional(self):
		s = self.location_additional_info
		return True if len(s) > 0 else False 

	def get_location_object(self):
		if self.is_location_model():
			return Location.objects.get(unique_id=self.location)
		else:
			return None

	def get_total_tickets_sold(self):
		ticket_transactions = TicketTransaction.objects.filter(activity=self)
		total_tickets = 0
		total_cost = 0
		currency = "USD"
		for transaction in ticket_transactions:
			total_tickets += transaction.quantity
			total_cost += transaction.total_cost
			currency = transaction.ticket.currency
		if currency.lower() == "usd":
			return [total_tickets, str(format(total_cost, ',.2f')) + " " + currency]
		else:
			return [total_tickets, str(format(total_cost, ',')) + " " + currency]

	def get_ticket_buyers(self):
		ticket_transactions = TicketTransaction.objects.filter(activity=self)
		check_user = {}
		result = []
		for transaction in ticket_transactions:
			if transaction.user.username not in check_user:
				result.append(transaction.user)
				check_user[transaction.user.username] = None
		return result

	def get_ticket_transactions(self):
		return TicketTransaction.objects.filter(activity=self)

	def is_all_tickets_sold_out(self):
		for ticket in self.tickets.all():
			if ticket.total_available_quantity > 0:
				return False
		return True


class UserProfile(models.Model):
	user = models.ForeignKey(User, unique=True,related_name='profile')   
	basic_info = models.TextField(blank=True)
	gender = models.CharField(max_length=1,choices=GENDER,default="m")
	avatar = models.ForeignKey("Photo",blank=True,null=True,related_name="avatar")
	cover_picture = models.ForeignKey("Photo",blank=True,null=True,related_name='cover_picture')
	city = models.CharField(max_length=50,blank=True,null=True)
	state = models.CharField(max_length=2,choices=USA_STATES,blank=True,null=True)
	country = models.CharField(max_length=100,choices=DEFAULT_COUNTRIES_FORMAT,blank=True)
	timezone = models.CharField(max_length=30,null=True)
	privacy_status = models.CharField(max_length=1,choices=PRIVACY_STATUS,default=PUBLIC)
	date_of_birth = models.DateField(blank=True,null=True)
	watchlist = models.ManyToManyField("Activity",related_name="watchlist",blank=True,null=True)
	invitations = models.ManyToManyField("Invitation",related_name="invitation",blank=True,null=True)
	facebook_id = models.CharField(max_length=40,blank=True,null=True)
	followers = models.ManyToManyField(User,related_name="followers",blank=True,null=True)
	following = models.ManyToManyField(User,related_name="following",blank=True,null=True)
	authorized_account = models.CharField(max_length=100,blank=True,null=True)
	reports = models.ManyToManyField(Report,related_name='user_reports',blank=True,null=True)

	def __unicode__(self):
		return unicode(self.user)

	def is_facebook_account(self):
		if self.facebook_id == None: return False
		if len(self.facebook_id) != 0:
			return True
		return False

	def get_user_fullname(self):
		first_name = self.user.first_name
		last_name = self.user.last_name
		if first_name.strip() == "" and last_name.strip() == "":
			username = self.user.username
			return username[0].upper() + username[1:].lower()
		return first_name + " " + last_name

	def get_all_activity_count(self):
		create_activities_count = Activity.objects.filter(user_create=self.user).count()
		join_activities_count = ActivityRequest.objects.filter(user_request=self.user,request_status="1").count()
		return create_activities_count + join_activities_count

	def get_tickets(self):
		return TicketTransaction.objects.filter(user=self)

	def get_create_locations(self):
		return Location.objects.filter(create_by=self.user)

	def get_follow_locations(self):
		return Location.objects.filter(follow_by=self.user)

	def get_create_activities(self):
		return Activity.objects.filter(user_create=self.user)

	def get_all_upload_photos(self):
		return Photo.objects.filter(user_post=self.user,privacy_status=PUBLIC).exclude(unique_id__in=[self.avatar.unique_id,self.cover_picture.unique_id]).order_by("-upload_date")

	def get_all_upload_photos_count(self):
		return Photo.objects.filter(user_post=self.user,privacy_status=PUBLIC).exclude(unique_id__in=[self.avatar.unique_id,self.cover_picture.unique_id]).order_by("-upload_date").count()

	def get_upload_photos(self,from_index=None,to_index=None):
		if from_index == None or to_index == None:
			return Photo.objects.filter(user_post=self.user,privacy_status=PUBLIC).exclude(unique_id__in=[self.avatar.unique_id,self.cover_picture.unique_id]).order_by("-upload_date")
		else:
			return Photo.objects.filter(user_post=self.user,privacy_status=PUBLIC).exclude(unique_id__in=[self.avatar.unique_id,self.cover_picture.unique_id]).order_by("-upload_date")[from_index:to_index]

	def get_join_activities(self):
		result = []
		activity_requests = ActivityRequest.objects.filter(user_request=self.user,request_status="1")
		for request in activity_requests:
			result.append(request.activity)
		return result

	def is_following(self,username):
		try:
			user_follow = User.objects.get(username=username)
			for user in self.following.all():
				if user.username == user_follow.username:
					return True
			return False
		except Exception as e:
			logger.exception(e)
		return False

def create_user_profile(sender, instance, created, **kwargs):
	if created:
		UserProfile.objects.create(user=instance)
		
post_save.connect(create_user_profile, sender=User)

class ApiItemType(models.Model):
	"This represents API for a model: Actiity, User, Ticket..." 
	name=models.CharField(max_length=100,null=False)

	def __str__(self):
		return "%s" %(self.name)

class ApiItem(models.Model):
	"This represents  an action for each model: create, edit Activity"
	name=models.CharField(max_length=1000,null=False)
	description=models.TextField(blank=True)
	api_item_type=models.ForeignKey(ApiItemType)

	def __str__(self):
		return "%s - %s" %(self.api_item_type, self.
			name)
