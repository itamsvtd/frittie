from dajax.core import Dajax
from dajaxice.decorators import dajaxice_register

from django.core import serializers
from django.contrib.auth.models import User
from django.db import connection

from frittie.apps.app_helper import generate_html_snippet
from frittie.apps.location.helper import get_nearby_location
from frittie.apps.activity.helper import get_nearby_activity

import json, logging

logger = logging.getLogger(__name__)


@dajaxice_register
def load_more_activity(request,num_activities_load,lat,lng):
	results = {}
	try:
		activities = get_nearby_activity(request,lat,lng)[num_activities_load:num_activities_load + 16]
		results['snippet_return'] = generate_html_snippet(request,"list_activity_homepage",{"activities":activities})
		activities_count = len(activities)
		if activities_count >= 16:
			results['num_activities_load'] = num_activities_load + 16
		else:
			results['num_activities_load'] = num_activities_load + activities_count
		results['success'] = True
	except Exception as e:
		logger.exception(e)
		results['success'] = False
	return json.dumps({"results": results})

@dajaxice_register
def load_more_location(request,num_locations_load,city):
	results = {}
	try:
		locations = get_nearby_location(request,city)[num_locations_load:num_locations_load + 16]
		results['snippet_return'] = generate_html_snippet(request,"list_location_homepage",{"locations":locations})
		locations_count = len(locations)
		if locations_count >= 16:
			results['num_locations_load'] = num_locations_load + 16
		else:
			results['num_locations_load'] = num_locations_load + locations_count
		results['success'] = True
	except Exception as e:
		logger.exception(e)
		results['success'] = False
	return json.dumps({"results": results})