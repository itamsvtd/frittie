# Create your views here.from frittie.main.views import *
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError, Http404
from django.core.urlresolvers import reverse
from django.utils.html import strip_tags
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.views.decorators.csrf import csrf_exempt, requires_csrf_token
from django.contrib.auth.models import User
from django.utils import simplejson, timezone
from django.core import serializers
from django.contrib.auth.decorators import login_required
from django.db.models import Q

from frittie.apps.main.models import Activity, Comment, Photo, ActivityRequest, Location , ConfirmationActivity, Ticket
from frittie.apps.activity.forms import ActivityForm
from frittie.apps.app_helper import get_user_login_object, generate_unique_id, convert_ampm_to_24hr
from frittie.apps.app_helper import convert_24hr_to_ampm, make_two_numbers, get_current_time_zone
from frittie.apps.main.constants import PUBLIC, PRIVATE, REQUEST_INVITED, UNLIMITED, DEFAULT_TIMEZONE
from frittie.apps.main.constants import PUBLIC_LOCATION, PRIVATE_LOCATION, LOCATION_CATEGORY_MAP, REQUEST_ACCEPT, REQUEST_RESERVED


from helper import check_user_activity_creator, check_activity_seeable, get_nearby_activity, convert_unicode_string

from urlparse import urlparse, parse_qs

from icalendar import Calendar, Event, vCalAddress, vText

import datetime, logging
import pytz

logger = logging.getLogger(__name__)

APP_NAME = "activity"

def main_page(request,activity_unique_id):
	data = {'app_name':APP_NAME}

	absolute_url = request.build_absolute_uri()
	dispatcher =  urlparse(absolute_url).query
	token = None
	try:
		new_string = str(parse_qs(dispatcher)['token'])
		token =  new_string[2:len(new_string)-2]
	except Exception as e:
		pass

	activity = None
	activities = Activity.objects.filter(unique_id=activity_unique_id).select_related().prefetch_related()
	if len(activities) == 0:
		raise Http404
	else:
		activity = activities[0]

	data['activity'] = activity
	user_login = get_user_login_object(request)

	is_user_activity_creator = check_user_activity_creator(user_login,activity)

	activity_requests = list(ActivityRequest.objects.filter(activity=activity,user_request=user_login).select_related().prefetch_related())
	activity_request = None if len(activity_requests) == 0 else activity_requests[0]
	data['activity_request'] = activity_request
	
	if token != None:
		valid_tokens = activity.tokens.filter(token=token)
		if len(valid_tokens) == 1:
			if activity_request == None:
				activity_request = ActivityRequest.objects.create(activity=activity,user_request=user_login,request_status=REQUEST_INVITED,is_invite_request=True)
				activity.activity_requests.add(activity_request)
				activity.save()

	is_activity_seeable = check_activity_seeable(user_login, activity, activity_request)

	template = "apps/activity/page/normal/public_main_page.html"
	if is_activity_seeable == False: 
		template = "apps/activity/page/normal/authorize_activity_notice.html"    
	
	if is_user_activity_creator:
		template = "apps/activity/page/admin/main_page.html"
	else:
		suggest_activities = Activity.objects.filter(category=activity.category,activity_type=PUBLIC,start_time__gte=timezone.now(),is_publish=True).exclude(unique_id=activity.unique_id).order_by("start_time")[:6]
		if activity.is_location_model():
			data['suggest_activities'] = get_nearby_activity(request,activity.get_location_object().lat,activity.get_location_object().lng,None,suggest_activities)
		else:
			data['suggest_activities'] = get_nearby_activity(request,None,None,None,suggest_activities)
	
	following_people_going = []
	followers_people_going = []
	others_people_going = []

	if user_login == None:
		data['is_watching'] = False
	else:
		accept_activity_requests = ActivityRequest.objects.filter(Q(activity=activity),Q(request_status=REQUEST_ACCEPT) | Q(activity=activity,request_status=REQUEST_RESERVED))
		for accept_activity_request in accept_activity_requests:
			user = accept_activity_request.user_request
			if user in user_login.following.all():
				following_people_going.append(accept_activity_request)
			elif user in user_login.followers.all():
				followers_people_going.append(accept_activity_request)
			else:
				others_people_going.append(accept_activity_request)
		data['is_watching'] = True if activity in user_login.get_profile().watchlist.all() else False

	data['following_people_going'] = following_people_going
	data['followers_people_going'] = followers_people_going
	data['others_people_going'] = others_people_going
	data['is_user_login_requested'] = False if activity_request == None else True
	data['user_login_request_status'] = None if data['is_user_login_requested'] == False else activity_requests[0].request_status
	data['list_activity_photos'] = activity.list_photos.all().order_by("-upload_date")[:3] 
	
	return render_to_response(template, data,context_instance = RequestContext(request))


@login_required
def create_activity(request):
	if request.method == "POST":
		user_login = get_user_login_object(request)
		request.session['is_show_request_message'] = True
		try:
			name = request.POST['name']
			name = convert_unicode_string(name)
			description = request.POST['description']
			start_date = request.POST['start_date']
			start_time = request.POST['start_time'].replace(" ","")
			hour = convert_ampm_to_24hr(int(start_time[0:2]),start_time[5:7])
			minute = int(start_time[3:5])
			start_datetime = datetime.datetime(int(start_date[6:]),
										int(start_date[:2]),int(start_date[3:5]),
										hour,minute)
			
			category = request.POST['category']
			privacy_option = request.POST['privacy']
			timezone = get_current_time_zone(request)
			activity_type = request.POST['activity_type']

			limit = UNLIMITED
			if "is_unlimited" not in request.POST:
				limit = request.POST['limit']
			new_activity = Activity(name=name,description=description,category=category,limit=limit,activity_type=activity_type,
									start_time=start_datetime,user_create=user_login,privacy_option=privacy_option,timezone=timezone,
									is_publish=False,unique_id=generate_unique_id(APP_NAME))
	
			if 'location_unique_id' in request.POST and len(request.POST['location_unique_id']) != 0:
				new_activity.location = request.POST['location_unique_id']
			else:
				is_google_place = request.POST['is_google_place']
				google_place_id =  ""
				if is_google_place == "true":
					google_place_id = request.POST['google_place_id']
					exist_locations = Location.objects.filter(google_place_id=google_place_id)
					if len(exist_locations) > 0 :
						new_activity.location = exist_locations[0].unique_id
					else:
						address1 = request.POST['google_place_address'] 
						location_category = ""
						if activity_type == PRIVATE:
							location_category = PRIVATE_LOCATION
						elif activity_type == PUBLIC:
							location_category = PUBLIC_LOCATION
						location_name = "User's place at " + address1[:address1.rfind(",")]
						new_location = Location(unique_id=generate_unique_id('location'),
												name = location_name, 
												address1 = address1,
												category = location_category,
												lat  = request.POST['google_place_lat'],
												lng = request.POST['google_place_lng'],
												city =  request.POST['google_place_city'],
												state = request.POST['google_place_state'],
												zip_code = request.POST['google_place_zipcode'],
												country = request.POST['google_place_country'],
												google_place_id = google_place_id,
												create_by = user_login)
						new_location.save()
						new_activity.location = new_location.unique_id

			if len(request.POST['end_date']) == 10:
				end_date = request.POST['end_date']
				end_time = request.POST['end_time'].replace(" ", "")
				hour = convert_ampm_to_24hr(int(end_time[0:2]),end_time[5:7])
				minute = int(end_time[3:5])
				end_datetime = datetime.datetime(int(end_date[6:]),int(end_date[:2]),int(end_date[3:5]),hour,minute)
				new_activity.end_time = end_datetime
			
			new_activity.save()
			user_login.get_profile().watchlist.add(new_activity)
			return HttpResponseRedirect("/activity/" + new_activity.unique_id + "/edit?action=create_activity&result=success")
		except Exception as e:
			raise
			#return HttpResponseRedirect("/?action=create_activity&result=error")
	return HttpResponseRedirect("/")
   


@login_required
def edit_activity(request,activity_unique_id):
	activity = Activity.objects.get(unique_id=activity_unique_id)
	if request.method == "POST":
		user_login = get_user_login_object(request)
		request.session['is_show_request_message'] = True
		try:
			activity.name = request.POST['name']
			activity.description = request.POST['description']
			activity.activity_type = request.POST['activity_type']
			activity.location_additional_info = request.POST['location_additional_info']
			if activity.activity_type == "1":
				activity.ticket_policy = request.POST['ticket_policy']

			start_date = request.POST['start_date']
			start_time = request.POST['start_time'].replace(" ","")
			hour = convert_ampm_to_24hr(int(start_time[0:2]),start_time[5:7])
			minute = int(start_time[3:5])
			activity.start_time = datetime.datetime(int(start_date[6:]),
										int(start_date[:2]),int(start_date[3:5]),
										hour,minute)
			
			if 'location_unique_id' in request.POST and len(request.POST['location_unique_id']) != 0:
				activity.location = request.POST['location_unique_id']
			else:
				activity.location = request.POST['activity_place']

			activity.category = request.POST['category']
			activity.privacy_option = request.POST['privacy']
			
			if "is_unlimited" not in request.POST:
				activity.limit = request.POST['limit']
			else:
				activity.limit = UNLIMITED
			# time format: HH:MM AM/PM
			if len(request.POST['end_date']) == 10:
				end_date = request.POST['end_date']
				end_time = request.POST['end_time'].replace(" ", "")
				hour = convert_ampm_to_24hr(int(end_time[0:2]),end_time[5:7])
				minute = int(end_time[3:5])
				activity.end_time = datetime.datetime(int(end_date[6:]),int(end_date[:2]),int(end_date[3:5]),hour,minute)
			if "logo" in request.FILES:
				new_photo_caption = activity.name
				new_photo = Photo.objects.create(caption=new_photo_caption,user_post=user_login,image=request.FILES['logo'],unique_id=generate_unique_id("photo"),photo_type='activity',object_unique_id=activity.unique_id)
				activity.logo = new_photo
			else:
				get_key = "activity_category_"+str(activity.category)
				activity.logo = Photo.objects.get(caption=get_key,photo_type='default_image')	
			is_make_publish = False
			if "is_make_publish" in request.POST:
				if bool(int(request.POST['is_make_publish'])) == True and activity.is_publish == False:
					activity.is_publish = True
					is_make_publish = True

			activity.last_update = datetime.datetime.now()
			activity.save()
			if activity.is_publish:
				if is_make_publish:
					return HttpResponseRedirect("/activity/" + activity.unique_id + "?action=publish_activity&result=success")
				else:
					return HttpResponseRedirect("/activity/" + activity.unique_id + "?action=update_activity&result=success")
			else:
				return HttpResponseRedirect("/activity/" + activity.unique_id + "?action=update_activity&result=success_need_publish")
		except Exception as e:
			logger.exception(e)
			return HttpResponseRedirect("/activity/" + activity.unique_id + "/edit?action=update_activity&result=error")
	else:
		start_time = convert_24hr_to_ampm(activity.start_time)
		default_data = {
			"name": activity.name, 
			"description": activity.description,
			"start_date": str("%02d" % activity.start_time.month) + "/" + str("%02d" % activity.start_time.day) + "/" + str(activity.start_time.year),
			"start_time": start_time[:5] + " " + start_time[5:].upper(),
			"privacy": activity.activity_type,
			"category": activity.category,
			'activity_type': activity.activity_type
		}
		if activity.end_time != None:
			default_data['end_date'] = str("%02d" % activity.end_time.month) + "/" + str("%02d" % activity.end_time.day) + "/" + str(activity.end_time.year)
			default_data['end_time'] = convert_24hr_to_ampm(activity.end_time)
		if activity.logo != None:
			default_data['logo'] = activity.logo.image
		form = ActivityForm(default_data)
		
		return render_to_response("apps/activity/page/admin/edit_page.html",{
			"app_name": APP_NAME,
			"form": form,
			"activity": activity,
			"default_data": default_data
		},context_instance=RequestContext(request))


@login_required
@requires_csrf_token
def delete_activity(request):
	user_login = get_user_login_object(request)
	if request.method == "POST":
		try:
			activity_unique_id = request.POST['activity_unique_id']
			activity = Activity.objects.get(unique_id=activity_unique_id)
			activity.delete()
			return HttpResponseRedirect("/people/" + str(user_login.username) )
		except Exception as e:
			logger.exception(e)
			request.session['is_show_request_message'] = True
			return HttpResponseRedirect("/people/" + str(user_login.username) + "?action=delete_activity&result=error")
	return HttpResponseRedirect("/")

@login_required
def add_to_calendar(request,activity_unique_id):
	activity = get_object_or_404(Activity,unique_id=activity_unique_id)
	is_location_model = activity.is_location_model()
	location = activity.get_location_object()
	if "type" in request.GET:
		calendar_type = request.GET['type']
		if calendar_type.lower() == "google":
			st = activity.start_time
			et = activity.end_time
			start_time = make_two_numbers(str(st.year)) + make_two_numbers(str(st.month))+ make_two_numbers(str(st.day))+"T"+make_two_numbers(str(st.hour))+make_two_numbers(str(st.minute))+make_two_numbers(str(st.second)) 
			if et == None:
				et = st + datetime.timedelta(hours=3)
			end_time = make_two_numbers(str(et.year)) + make_two_numbers(str(et.month))+ make_two_numbers(str(et.day))+"T"+make_two_numbers(str(et.hour))+make_two_numbers(str(et.minute))+make_two_numbers(str(st.second))
			activity_name = activity.name
			activity_name = activity_name.replace(" ","+") 
			redirect_web_address = "https://www.google.com/calendar/render?action=TEMPLATE&text=" + activity_name + "!&dates=" + start_time + "/" + end_time + "&details=More+details,+visit:" + "http://frittie.com/activity" + activity_unique_id 
			if is_location_model:
				redirect_web_address += "&location=" + location.address1.replace(" ","+") + "+," + location.city + "+," + location.country
			else:
				redirect_web_address += "&location=" + activity.location
			redirect_web_address += "&trp=false&sprop=http://frittie.com&output=xml"
			return HttpResponseRedirect(redirect_web_address)
		elif calendar_type.lower() == "icalendar" or calendar_type.lower() == "outlook":
			cal = Calendar()
			event = Event()
			event.add('summary', activity.name)
			st = activity.start_time
			et = activity.end_time
			timezone = activity.timezone
			if timezone == None or len(timezone.replace(" ","")) == 0:
				timezone = get_current_time_zone(request)
				activity.timezone = timezone
				activity.save()
			event.add('dtstart', datetime.datetime(st.year,st.month,st.day,st.hour,st.second,tzinfo=pytz.timezone(timezone)))
			if et != None:
				event.add('dtend',  datetime(et.year,et.month,et.day,et.hour,et.second,tzinfo=pytz.timezone(timezone)))
			if is_location_model:
				event['location'] = vText(location.city + " " + location.country)
			else:
				event['location'] = vText(activity.location)
			event['description'] = strip_tags(activity.description)
			cal.add_component(event)
			response = HttpResponse(cal.to_ical(), content_type="text/calendar")
			activity_name = convert_unicode_string(activity.name)
			activity_name = activity_name.replace(" ","_")
			response['Filename'] = activity_name + ".ics"
			response['Content-Disposition'] = 'attachment; filename='+ activity_name[0:len(activity_name)] + ".ics"
			return response

@login_required
def update_activity_logo(request,activity_unique_id):
	if request.method == "POST":
		try:
			activity = Activity.objects.get(unique_id=activity_unique_id)
			if "logo" in request.FILES:
				if activity.logo == None:
					user_login = get_user_login_object(request)
					new_photo_caption = activity.name 
					new_photo = Photo.objects.create(caption=new_photo_caption,user_post=user_login,image=request.FILES['logo'],unique_id=generate_unique_id("photo"),photo_type='activity',object_unique_id=activity_unique_id)
					activity.logo = new_photo
				else:
					activity.logo.image = request.FILES['logo'] 
					activity.logo.save()
				activity.save()
			else:
				raise
			return HttpResponseRedirect("/activity/" + activity_unique_id)
		except Exception as e:
			logger.exception(e)
			request.session['is_show_request_message'] = True
			return HttpResponseRedirect("/activity/" + activity_unique_id + "?action=update_logo&result=error")
	return HttpResponseRedirect("/")	

@login_required
def publish_activity(request,activity_unique_id):
	if request.method == 'POST':
		request.session['is_show_request_message'] = True
		try:
			activity = Activity.objects.get(unique_id=activity_unique_id)
			activity.is_publish = True
			activity.save()
			return HttpResponseRedirect("/activity/" + activity_unique_id + "?action=publish_activity&result=success")
		except Exception as e:
			logger.exception(e)
			return HttpResponseRedirect("/activity/" + activity_unique_id + "?action=publish_activity&result=error")
	return HttpResponseRedirect("/")




