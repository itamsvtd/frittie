from django import forms
from django.contrib.auth.models import User

from frittie.apps.main.models import Location,UserProfile,Activity, Comment, Photo
from frittie.apps.main.constants import PRIVACY_STATUS, ACTIVITY_CATEGORY

import datetime

ACTIVITY_TYPE = (
    ("0", "Casual Event"),
    ("1", "Ticket Event"),
    ("2", "Sale Event"),
)

class ActivityForm(forms.Form):
    name = forms.CharField(max_length=200,widget=forms.TextInput(attrs={'class' : 'form-control', "required": "required"}))
    description = forms.CharField(widget=forms.Textarea(attrs={'cols': 50, 'rows': 3, "class": "form-control","required": "required"}))
    logo = forms.ImageField(label='Logo',required=False) 
    limit = forms.IntegerField(required=False,widget=forms.TextInput(attrs={'class' : 'form-control',"disabled":"disabled","numeric":"numeric"}))
    activity_type = forms.ChoiceField(choices=ACTIVITY_TYPE,widget=forms.Select(attrs={'class' : 'form-control'})) 
    privacy = forms.ChoiceField(choices=PRIVACY_STATUS,initial="1",widget=forms.Select(attrs={'class' : 'form-control'}))           
    activity_place = forms.CharField(max_length=100,widget=forms.TextInput(attrs={'class' : 'form-control',"required": "required"}))
    location_additional_info = forms.CharField(widget=forms.Textarea(attrs={'cols': 50, 'rows': 3, "class": "form-control"}))
    category = forms.ChoiceField(choices=ACTIVITY_CATEGORY,widget=forms.Select(attrs={'class' : 'form-control'}))
