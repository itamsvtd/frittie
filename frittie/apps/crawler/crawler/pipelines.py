# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/topics/item-pipeline.html
import os, sys, sqlite3, dj_database_url, urllib, json, csv

SETTING_PATH = os.path.abspath(__file__ + "/../../../../")
PROJECT_PATH = os.path.abspath(__file__ + "/../../../../../")

sys.path.append(PROJECT_PATH)
sys.path.append(SETTING_PATH)
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

from django.template import RequestContext
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.contrib.sites.models import Site
from django.contrib.auth.models import User
from django import db

from scrapy.conf import settings

from frittie.apps.main.models import Location,UserProfile,Activity, Comment, Photo
from frittie.apps.app_helper import generate_unique_id, get_any_admin_object
from frittie.apps.app_settings import DEFAULT_IMAGE_UNIQUE_ID

def geocode(addr):
		url = "http://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=false" %   (urllib.quote(addr.replace(' ', '+')))
		data = urllib.urlopen(url).read()
		info = json.loads(data).get("results")[0].get("geometry").get("location")  
    		return info

class CrawlerPipeline(object):
	def process_item(self, item, spider):
		print "Checkloc"
		locaddress = str(item['address1'])
		locname = str(item['name'])
		locaddress = locaddress[5:(len(locaddress)-5)] 

		decodeaddress = locaddress +  ", Philadelphia"
		default_restaurant_photo= None
		user = get_any_admin_object()

		try:
			default_restaurant_photo =  Photo.objects.get(unique_id=DEFAULT_IMAGE_UNIQUE_ID['location_category_1'])
		except ObjectDoesNotExist:
			print "This photo is not existed, from process photo"
		if default_restaurant_photo == None :
			#image_request = requests.get("https://frittie.s3.amazonaws.com/img/apps/location/RestaurantIcon.png", stream=True)
			#image_file = tempfile.NamedTemporaryFile()
			#for block in image_request.iter_content(1024 * 8):
			#	if not block:
			#		break
			#image_file.write(block)
			#image_file.name = "restaurant-icon.png"
			
			default_restaurant_photo = Photo.objects.create(unique_id=generate_unique_id("photo"),caption="default_restaurant_photo",user_post=user,image="default/img/location/RestaurantIcon.png")
			default_restaurant_photo.save()


		r = geocode(decodeaddress)
		locname = locname[3:(len(locname)-2)]
		loc = Location.objects.create(category="1",unique_id=generate_unique_id("location"),name=locname,address1=locaddress,lat=r['lat'],lng=r['lng'],city="Philadelphia",country="United States", description="test",state="PA",zip_code="19104",create_by= user, main_picture=default_restaurant_photo)
		loc.save()
        	return item
	
