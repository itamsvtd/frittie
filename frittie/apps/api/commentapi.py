from django.contrib.auth.models import User
from django.core.paginator import Paginator, InvalidPage
from django.db import models
from django.contrib.auth import authenticate, login  
from django.conf.urls import patterns, url, include

from tastypie.authorization import Authorization, DjangoAuthorization
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.serializers import Serializer  
from tastypie.authentication import Authentication , ApiKeyAuthentication, BasicAuthentication
from tastypie.models import create_api_key
from tastypie.models import ApiKey
from tastypie.utils import trailing_slash

from frittie.apps.main.models import Comment

from haystack.query import SearchQuerySet

from frittie.apps.pubnub.Pubnub import Pubnub
from frittie.settings import STATIC_URL

import json
import logging
import math


class CommentResource(ModelResource):
    user = fields.OneToOneField('frittie.apps.api.userapi.UserResource','user', null= True, blank = True)
    class Meta:
        queryset = Comment.objects.all()
        resource_name = 'Comment'
        allowed_methods = ['get','post','put','delete']
        serializer = Serializer(formats=['json', 'plist'])
        authorization= Authorization()
        #authentication = ApiKeyAuthentication()
        models.signals.post_save.connect(create_api_key, sender=User)