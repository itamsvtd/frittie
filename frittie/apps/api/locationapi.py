from django.contrib.auth.models import User
from django.core.paginator import Paginator, InvalidPage
from django.db import models
from django.contrib.auth import authenticate, login  
from django.conf.urls import patterns, url, include

from tastypie.authorization import Authorization, DjangoAuthorization
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.serializers import Serializer  
from tastypie.authentication import Authentication , ApiKeyAuthentication, BasicAuthentication
from tastypie.models import create_api_key
from tastypie.models import ApiKey
from tastypie.utils import trailing_slash

from frittie.apps.main.models import Location, Activity

from haystack.query import SearchQuerySet

from frittie.apps.pubnub.Pubnub import Pubnub

import json
import logging
import math
from frittie.apps.pubnub.Pubnub import Pubnub
from frittie.settings import STATIC_URL
from frittie.apps.app_helper import check_position_within_distance
class LocationResource(ModelResource):
    create_by = fields.OneToOneField('frittie.apps.api.userapi.UserResource', 'create_by' , null=True, blank=True)
    follow_by = fields.ManyToManyField('frittie.apps.api.userapi.UserResource', 'follow_by', null= True, blank = True)
    class Meta:
        queryset = Location.objects.all()
        resource_name = 'Location'
        allowed_methods = ['get','post','put','delete']
        serializer = Serializer(formats=['json', 'plist'])
        authorization= Authorization()
        filtering = {
            'name': ALL,
        }
        authentication = ApiKeyAuthentication()

    def prepend_urls(self):
        return [
        url(r"^(?P<resource_name>%s)/(?P<name>[\w\d_.-]+)/follow_by%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_follow_by'), name="api_get_follow_by"),
        url(r"^(?P<resource_name>%s)/(?P<name>[\w\d_.-]+)/create_by%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_create_by'), name="api_get_create_by"),
        url(r"^(?P<resource_name>%s)/(?P<name>[\w\d_.-]+)/$" % self._meta.resource_name, self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        url(r"^(?P<resource_name>%s)/d/search%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('search'), name="api_search"),
        url(r"^(?P<resource_name>%s)/d/nearby%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('nearby'), name="api_get_nearby"),
        #url(r"^(?P<resource_name>%s)%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('emptyurl'), name="api_get_emptyurl"),

        ]

    def get_follow_by(self, request, **kwargs):
        basic_bundle = self.build_bundle(request=request)
        obj = self.cached_obj_get(bundle=basic_bundle, **self.remove_api_resource_names(kwargs))
        user_resource = UserResource()
        try:
            user_resource._meta.queryset = obj.follow_by.all()
        except IndexError:
            user_resource._meta.queryset = UserProfile.objects.none()
        return user_resource.get_list(request)


    def get_create_by(self, request, **kwargs):
        basic_bundle = self.build_bundle(request=request)
        obj = self.cached_obj_get(bundle=basic_bundle, **self.remove_api_resource_names(kwargs))
        user_resource = UserResource()
        try:
            user_resource._meta.queryset = obj.create_by.all()
        except IndexError:
            user_resource._meta.queryset = UserProfile.objects.none()
        return user_resource.get_list(request)        
        models.signals.post_save.connect(create_api_key, sender=User)
        log = logging.getLogger(__name__)
    
    def search(self, request, **kwargs):   
        self.method_check(request, allowed=['post','get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        data = request.POST.dict()
        querry= data['querry']
        qpage= data['page']
        # Do the query.
        sqs = SearchQuerySet().models(Location).load_all().auto_query(querry)
        paginator = Paginator(sqs, 1)

        try:
            rpage = paginator.page(qpage)
        except InvalidPage:
            raise Http404("Sorry, no results on that page.")

        objects = []

        for result in rpage.object_list:
            bundle = self.build_bundle(obj=result.object, request=request)
            bundle = self.full_dehydrate(bundle)
            objects.append(bundle)

        object_list = {
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list)

    def nearby(self, request, **kwargs):   
        self.method_check(request, allowed=['post','get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        locations = Location.objects.filter(**self.remove_api_resource_names(kwargs))
        data = request.POST.dict()
        lat= data['lat']
        lng= data['lng']
        rad= int(data['rad'])
        objects = []
        radius = rad*1000
        for location in locations:
            if check_position_within_distance(math.radians(float(lat)), math.radians(float(lng)),math.radians(float(location.lat)), math.radians(float(location.lng)), radius): 
                bundle = self.build_bundle(obj=location, request=request)
                bundle = self.full_dehydrate(bundle)
                result = {}
                result['bundle'] = bundle
                activities = Activity.objects.filter(location=location.unique_id).order_by("-start_time")
                result['list_activitivies'] = activities
                objects.append(result)
        return self.create_response(request, objects)

    def emptyurl(self, request, **kwargs):   
        self.method_check(request, allowed=['post','get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        data = request.POST.dict()
        user_create= data['user_create']
        location = data['location']
        print user_create
        print location
        info = pubnub.publish({
        'channel' : 'activitychannel',
        'message' : {
        'user_create' : user_create ,
        'location' : location
        }
        })
        print(info)
    
        objects = []

        object_list = {
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list)
