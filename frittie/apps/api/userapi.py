from django.contrib.auth.models import User
from django.core.paginator import Paginator, InvalidPage
from django.db import models
from django.contrib.auth import authenticate, login  
from django.conf.urls import patterns, url, include

from tastypie.authorization import Authorization, DjangoAuthorization
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.serializers import Serializer  
from tastypie.authentication import Authentication , ApiKeyAuthentication, BasicAuthentication
from tastypie.models import create_api_key
from tastypie.models import ApiKey
from tastypie.utils import trailing_slash

from haystack.query import SearchQuerySet

from frittie.apps.pubnub.Pubnub import Pubnub
from frittie.apps.member.helper import set_readable_username

import json
import logging
import math
from frittie.apps.pubnub.Pubnub import Pubnub
from frittie.settings import STATIC_URL
from django.db import IntegrityError

class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'User'
        allowed_methods = ['get','post']
        serializer = Serializer(formats=['json', 'plist'])
        #authorization= DjangoAuthorization()
        #authentication = BasicAuthentication()
        authorization = Authorization()
        #authentication = ApiKeyAuthentication()
        excludes = ['id', 'email', 'password', 'is_staff', 'is_superuser']
        filtering = {
            'username': ALL,
        }

    def prepend_urls(self):
        return [
        url(r"^(?P<resource_name>%s)/signin%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('signin'), name="api_signin"),
        url(r"^(?P<resource_name>%s)/signup%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('signup'), name="api_signup"),
        url(r"^(?P<resource_name>%s)/(?P<username>[\w\d_.-]+)/$" % self._meta.resource_name, self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def signin(self, request, **kwargs):
        self.method_check(request, allowed=['post','get'])
        data = request.POST.dict()
        email= data['email']
        password=  data['password']
        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            return self.create_response(request, {'success': 0, 'api_key': None,'message': "email or password incorrect"})

        # Time to get the apikey for the current user
        from tastypie.models import ApiKey

        try:
            api_key = ApiKey.objects.get(user=user)
        except ApiKey.DoesNotExist:
            # Create key if user doesn't have one (not tested yet)
            api_key = ApiKey.objects.create(user=user)
        print api_key.key
        user = authenticate(username=user.username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return self.create_response(request, {'success': 1, 'api_key': api_key.key, 'message': "log in successfully", 'username':user.username, 'first_name':user.first_name, 'last_name':user.last_name})
            else:
                # Return a 'disabled account' error message
                return self.create_response(request, {'success': 0, 'api_key': None , 'message': "log in successfully"})
        else:
             #Return an 'invalid login' error message.
            return self.create_response(request, {'success': 0, 'api_key': None,'message': "email or password incorrect"})
        #authentication = ApiKeyAuthentication()

    def signup(self, request, **kwargs):
        try:
            self.method_check(request, allowed=['post','get'])
            data = request.POST.dict()
            password=  data['password']
            first_name = data['first_name']
            last_name = data['last_name']
            email = data['email']
            username= set_readable_username(email)
            User.objects.create_user(username,email,password)
            new_user = User.objects.get(username = username)
            new_user.first_name = first_name
            new_user.last_name = last_name
            new_user.save()
            return self.create_response(request, {'success': 1, 'message': "sign up successfully"})
        except IntegrityError:
            return self.create_response(request, {'success': 0, 'message': "username already exists"})

    def obj_create(self, bundle, request=None, **kwargs):
        username, password, first_name, last_name, email = bundle.data['username'], bundle.data['password'] , bundle.data['first_name'], bundle.data['last_name'], bundle.data['email']
        try:
            bundle.obj = User.objects.create_user(username,' ',password)
            new_user = User.objects.get(username = username)
            new_user.first_name = first_name
            new_user.last_name = last_name
            new_user.email = email
            new_user.save()
        except IntegrityError:
            raise BadRequest('That username already exists')
        return bundle
    models.signals.post_save.connect(create_api_key, sender=User)