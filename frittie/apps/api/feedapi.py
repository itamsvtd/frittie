from django.contrib.auth.models import User
from django.core.paginator import Paginator, InvalidPage
from django.db import models
from django.contrib.auth import authenticate, login  
from django.conf.urls import patterns, url, include

from tastypie.authorization import Authorization, DjangoAuthorization
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.serializers import Serializer  
from tastypie.authentication import Authentication , ApiKeyAuthentication, BasicAuthentication
from tastypie.models import create_api_key
from tastypie.models import ApiKey
from tastypie.utils import trailing_slash

from frittie.apps.main.models import Feed

from haystack.query import SearchQuerySet

from frittie.apps.pubnub.Pubnub import Pubnub

import json
import logging
import math
from frittie.apps.pubnub.Pubnub import Pubnub
from frittie.settings import STATIC_URL

# return feed story
class FeedResource(ModelResource):
    class Meta:
        queryset = Feed.objects.all()
        resource_name = 'Feed'
        allowed_methods = ['get','post']
        serializer = Serializer(formats=['json', 'plist'])
        authorization= Authorization()
        #authentication = ApiKeyAuthentication()
        excludes = ['id', 'email', 'password', 'is_staff', 'is_superuser']
        filtering = {
            'username': ALL,
        }
    def prepend_urls(self):
        return [
        url(r"^(?P<resource_name>%s)/detail%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('detail'), name="api_detail"),
        ]

    def detail(self, request, **kwargs):   
        self.method_check(request, allowed=['post','get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        data=json.loads(request.body)
        username= data['username']
        objects = []                  
        
        #bundle = self.build_bundle(obj=act, request=request)
        #bundle = self.full_dehydrate(bundle)
        #objects.append(bundle)

        object_list = {
            'objects': objects,
        }
        self.log_throttled_access(request)
        return self.create_response(request, object_list)