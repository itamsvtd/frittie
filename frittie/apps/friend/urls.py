from django.conf.urls import *
from frittie import settings

urlpatterns = patterns('frittie.apps.friend.views',
    url(r"^invite/$", "invite_friend",name="invite_friend"),
    url(r'^facebook/$',"get_facebook_friends",name="get_facebook_friends")
)
