from django.template import Context, loader
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError, Http404
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404
from django.utils import simplejson
from django.core import serializers
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import requires_csrf_token, csrf_exempt

from forms import LocationForm

from helper import get_location_category_photo

from frittie.apps.app_helper import convert_time, get_user_login_object, generate_unique_id
from frittie.apps.activity.helper import get_today_activity, get_upcoming_activity
from frittie.apps.activity.helper import get_past_activity, get_happening_activity
from frittie.apps.main.models import Location, Photo, Comment, Activity
from frittie.apps.main.constants import USA_COUNTRY_CODE

import logging

logger = logging.getLogger(__name__)

APP_NAME = "location"

@login_required
def main_page(request,location_unique_id):
	location = get_object_or_404(Location,unique_id=location_unique_id) 
	
	user_login = get_user_login_object(request)
	data = {
		"app_name": APP_NAME,
		"location": location,
		"is_user_follow": location.is_user_follow(user_login)
	}
	template = 'apps/location/page/normal/main_page.html'
	if location.is_user_create(user_login):
		template = 'apps/location/page/admin/main_page.html'
		location_data = {
			"name": location.name,
			"description": location.description,
			"category": location.category,
			"address1": location.address1,
			"address2": location.address2,
			"city": location.city,
			"state": location.state,
			"zip_code": location.zip_code,
			"country": location.country,
			"website": location.website_link,
			'phone': location.phone,
		}
		edit_location_form = LocationForm(location_data)
		data['edit_location_form'] = edit_location_form
	return render_to_response(template,data,context_instance=RequestContext(request))


@login_required
def create_location(request):
	if request.method == 'POST':
		user_login = get_user_login_object(request)
		request.session['is_show_request_message'] = True
		try:
			location_name = request.POST['name']
			description = request.POST['description']
			category = request.POST['category']
			address1 = request.POST['address1']
			address2 = request.POST['address2']
			city = request.POST['city']
			state = request.POST['state']
			zip_code = request.POST['zip_code']
			country = request.POST['country']
			website = request.POST['website']
			lat = request.POST['lat']
			lng = request.POST['lng']
			phone = request.POST['phone']

			google_place_id =  ""
			if "is_google_place" in request.POST and request.POST['is_google_place'] == "true":
				address1 = request.POST['google_place_address']
				google_place_id = request.POST['google_place_id']


			new_location = Location(unique_id=generate_unique_id(APP_NAME),name=location_name,description=description,
					category=category,address1=address1,address2=address2,city=city,country=country,
					lat=lat,lng=lng,website_link=website,create_by=user_login,phone=phone,google_place_id=google_place_id)

			if country == USA_COUNTRY_CODE: 
				new_location.state = state
				new_location.zip_code = zip_code

			if "main_picture" in request.FILES:
				new_photo_caption = location_name
				new_photo = Photo.objects.create(caption=new_photo_caption,user_post=user_login,image=request.FILES['main_picture'],unique_id=generate_unique_id("photo"),photo_type='location',object_unique_id=new_location.unique_id)
				new_location.main_picture = new_photo
			
			new_location.save()
			return HttpResponseRedirect("/location/" + new_location.unique_id + "?action=create_location&result=success")
		except Exception as e:
			logger.exception(e)
			return HttpResponseRedirect("/?action=create_location&result=error")
	return HttpResponseRedirect("/")


@login_required
def delete_location(request):
	if request.method == "POST":
		request.session['is_show_request_message'] = True
		try:
			location_unique_id = request.POST['location_unique_id']
			location = Location.objects.get(unique_id=location_unique_id)
			location.delete()
			return HttpResponseRedirect("/?action=delete_location&result=success")
		except Exception as e:
			logger.exception(e)
			return HttpResponseRedirect("/?action=delete_location&result=error")
	return HttpResponseRedirect("/")
	

@login_required
def edit_location(request, location_unique_id):
	if request.method == 'POST':
		user_login = get_user_login_object(request)
		location = get_object_or_404(Location,unique_id=location_unique_id)
		request.session['is_show_request_message'] = True
		try:
			location.name = request.POST['name']
			location.description = request.POST['description']
			location.category = request.POST['category']
			location.address1 = request.POST['address1']
			location.address2 = request.POST['address2']
			location.city = request.POST['city']
			location.state = request.POST['state']
			location.zip_code = request.POST['zip_code']
			location.country = request.POST['country']
			location.website_link = request.POST['website']
			location.lat = request.POST['lat']
			location.lng = request.POST['lng']
			location.phone = request.POST['phone']

			if location.country == USA_COUNTRY_CODE: 
				location.state = state
				location.zip_code = zip_code

			if "main_picture" in request.FILES:
				photo = location.main_picture
				photo.image = request.FILES['main_picture']
				photo.save()
			
			location.save()
			return HttpResponseRedirect("/location/" + location.unique_id + "?action=edit_location&result=success")
		except Exception as e:
			logger.exception(e)
			return HttpResponseRedirect("/location/" + location.unique_id + "?action=edit_location&result=error")
	return HttpResponseRedirect("/")
	
	

@login_required
def update_picture(request,location_unique_id):
	if request.method == "POST":
		location = Location.objects.get(unique_id=location_unique_id)
		if location.main_picture != None:
			photo = location.main_picture
			photo.image = request.FILES['picture']
			photo.save()
		else:
			user_login = get_user_login_object(request)
			new_photo_caption = location.name
			new_photo = Photo.objects.create(caption=new_photo_caption,user_post=user_login,image=request.FILES['picture'],unique_id=generate_unique_id("photo"),photo_type='location',object_unique_id=location.unique_id)
			location.main_picture = new_photo
			location.save()
		return HttpResponseRedirect("/location/" + location_unique_id)
	return HttpResponseRedirect("/")


