from django.utils import simplejson
from django.contrib.auth.models import User
from dajax.core import Dajax
from dajaxice.decorators import dajaxice_register

from frittie.apps.app_helper import generate_html_snippet, generate_message
from frittie.apps.main.models import Activity, Location, Photo
from frittie.apps.app_settings import NUM_PHOTO_PER_LOAD

import logging, json

logger = logging.getLogger(__name__)

@dajaxice_register
def load_more_photo(request,object_unique_id,object_type,num_photo_load):
	results = {}
	try:
		photos = []
		if object_type == "activity":
			activity = Activity.objects.get(unique_id=object_unique_id)
			photos = activity.list_photos.prefetch_related("user_post").all().order_by("-upload_date")[num_photo_load:num_photo_load + NUM_PHOTO_PER_LOAD]
		else:
			location = Location.objects.get(unique_id=object_unique_id)
			photos = location.list_photos.prefetch_related("user_post").all().order_by("-upload_date")[num_photo_load:num_photo_load + NUM_PHOTO_PER_LOAD]
		results['snippet_return'] = generate_html_snippet(request,"list_photo_thumbnail",{"photos":photos})
		results['num_photo_load'] = num_photo_load + NUM_PHOTO_PER_LOAD
		results['success'] = True
	except Exception as e:
		logger.exception(e)
		results['success'] = False
	return json.dumps({"results": results})


@dajaxice_register
def change_photo_caption(request,photo_unique_id,new_caption):
	results = {}
	try:
		photo = Photo.objects.get(unique_id=photo_unique_id)
		photo.caption = new_caption
		photo.save()
		results['success'] = True
		results['new_caption'] = new_caption
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})   
	return json.dumps({"results": results})





