from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.views.decorators.csrf import csrf_exempt, requires_csrf_token
from django.contrib.auth.models import User
from django.utils import simplejson
from django.core import serializers
from django.contrib.auth.decorators import login_required

from frittie.apps.main.models import Photo, Activity, Location
from frittie.apps.app_settings import NUM_PHOTO_PER_LOAD
from frittie.apps.app_helper import generate_unique_id, get_user_login_object
from frittie.apps.main.constants import VALID_FILE_SIZE, PRIVATE

import logging

logger = logging.getLogger(__name__)


def activity_photo_page(request,activity_unique_id):
	data = {}
	template = "apps/photo/page/photo_page.html"
	activity = get_object_or_404(Activity, unique_id=activity_unique_id)
	data['object'] = activity
	data['object_type'] = "activity"
	data['total_photos'] = activity.list_photos.count()

	user_login = get_user_login_object(request)
	data['is_user_login_can_add_photo'] = False
	if user_login == activity.user_create or user_login in activity.get_people_going():
		data['is_user_login_can_add_photo'] = True
	
	if data['total_photos'] == 0 and activity.logo == None:
		data['is_no_photo'] = True
	else:
		if activity.logo != None:
			data['total_photos'] = data['total_photos'] + 1

		if data['total_photos'] < NUM_PHOTO_PER_LOAD:
			data['photos'] = list(activity.list_photos.prefetch_related("user_post").all().order_by("-upload_date"))
			data['is_load_more'] = False
			data['num_photo_load'] = -1
		else:
			data['photos'] = list(activity.list_photos.prefetch_related("user_post").all().order_by("-upload_date")[:NUM_PHOTO_PER_LOAD])
			
			data['is_load_more'] = True
			data['num_photo_load'] = NUM_PHOTO_PER_LOAD
		
		if activity.logo != None:
			data['photos'].insert(0,activity.logo)

		current_photo = data['photos'][0]
		if "current_photo" in request.GET:
			try:
				current_photo = Photo.objects.get(unique_id=request.GET['current_photo'])
			except Photo.DoesNotExist:
				return HttpResponseRedirect("/photo/activity/" + activity_unique_id)

		data['current_photo'] = current_photo
		
		if current_photo.unique_id == data['photos'][0].unique_id:
			data['is_first_photo'] = True
		
		if current_photo.unique_id == data['photos'][len(data['photos'])-1].unique_id:
			data['is_last_photo'] = True

	return render_to_response(template,data,context_instance=RequestContext(request))


def location_photo_page(request,location_unique_id):
	data = {}
	template = "apps/photo/page/photo_page.html"
	location = get_object_or_404(Location, unique_id=location_unique_id)
	data['object'] = location
	data['object_type'] = "location"
	total_photos = location.list_photos.count() + 1
	data['total_photos'] = total_photos

	user_login = get_user_login_object(request)
	data['is_user_login_can_add_photo'] = False
	if user_login == location.create_by or user_login in location.follow_by.all():
		data['is_user_login_can_add_photo'] = True

	if total_photos < NUM_PHOTO_PER_LOAD:
		data['photos'] = list(location.list_photos.prefetch_related("user_post").all().order_by("-upload_date"))
		data['photos'].insert(0,location.main_picture)
		data['is_load_more'] = False
		data['num_photo_load'] = -1
	else:
		data['photos'] = list(location.list_photos.prefetch_related("user_post").all().order_by("-upload_date")[:NUM_PHOTO_PER_LOAD])
		data['photos'].insert(0,location.main_picture)
		data['is_load_more'] = True
		data['num_photo_load'] = NUM_PHOTO_PER_LOAD
	
	current_photo = data['photos'][0]
	if "current_photo" in request.GET:
		current_photo = get_object_or_404(Photo, unique_id=request.GET['current_photo'])   
	data['current_photo'] = current_photo
	
	if current_photo.unique_id == data['photos'][0].unique_id:
		data['is_first_photo'] = True
	
	if current_photo.unique_id == data['photos'][len(data['photos'])-1].unique_id:
		data['is_last_photo'] = True

	return render_to_response(template,data,context_instance = RequestContext(request))


@login_required
def add_photo(request):
	if request.method == "POST":
		user_login = get_user_login_object(request)
		request.session['is_show_request_message'] = True
		try:
			unique_id = request.POST['unique_id']
			photo_type = request.POST['photo_type']
			callback_url = request.POST['callback_url']
			success_url = request.POST['success_url']
			caption = ""
			if "caption" in request.POST:
				caption = request.POST['caption']
		
			if request.FILES['image'].size <= VALID_FILE_SIZE:
				new_photo = Photo.objects.create(unique_id=generate_unique_id("photo"),photo_type=photo_type,
					user_post=user_login,image=request.FILES['image'],caption=caption,object_unique_id=unique_id)

				if photo_type == "activity":
					activity = Activity.objects.get(unique_id=unique_id)
					activity.list_photos.add(new_photo)
					if activity.activity_type == PRIVATE:
						new_photo.privacy_status = PRIVATE
						new_photo.save()
					activity.save()
				elif photo_type == "location":
					location = Location.objects.get(unique_id=unique_id)
					location.list_photos.add(new_photo)
					location.save()  

				return HttpResponseRedirect(success_url + "/?current_photo=" + new_photo.unique_id)
			else:
				return HttpResponseRedirect(callback_url + "/?action=add_photo&result=file_size_error")
		except Exception as e:
			logger.exception(e)
			return HttpResponseRedirect(callback_url + "/?action=add_photo&result=error")
	return HttpResponseRedirect("/")

@login_required
def delete_photo(request,photo_unique_id):
	if request.method == "POST":
		user_login = get_user_login_object(request)
		request.session['is_show_request_message'] = True
		try:
			callback_url = request.POST['callback_url']
			photo = Photo.objects.get(unique_id=photo_unique_id)
			photo.delete()
			return HttpResponseRedirect(callback_url)
		except Exception as e:
			logger.exception(e)
			return HttpResponseRedirect(callback_url + "?action=delete_photo&result=error")
	return HttpResponseRedirect("/")





