from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseBadRequest
from django.utils import timezone

from frittie.apps.app_helper import convert_time, get_user_login_object
from frittie.apps.app_helper import  get_current_time_at_client_time_zone , get_current_time_zone
from frittie.apps.watchlist.helper import check_today, check_yesterday, check_last_week, check_upcoming, check_past, check_next_week ,check_next_month,check_tomorrow
from frittie.apps.main.constants import ACTIVITY_CATEGORY_MAP

from datetime import datetime, timedelta
import calendar


@login_required()
def show_watchlist(request):
    current_time = get_current_time_at_client_time_zone(request)
    current_timezone = get_current_time_zone(request)
    user_login = get_user_login_object(request)
    today_activities = []

    timezone.make_aware(datetime.now(), timezone.get_default_timezone())
    for activity in user_login.get_profile().watchlist.all():
        #if start time is in today then add to list
        if (check_today(activity,current_time,current_timezone)):
                today_activities.append(activity)
        else: #start time is not in today, check if endtime is in today
            if (activity.end_time != None):
                if (check_today(activity,current_time,current_timezone)):
                    today_activities.append(activity) 

        # if (activity.end_time != None):
        #     if (check_today(activity.end_time,datetime.now())):
        #         today_activities.append(activity)
        #         continue  
        # else:
        #     if (check_today(activity.start_time,datetime.now())):
        #         today_activities.append(activity)

    activities = []
    length_yesterday_activities = 0
    length_upcoming_activities = 0
    length_past_activities = 0
    length_last_week_activities =  0 
    length_today_activities = 0 
    length_tomorrow_activities = 0 
    length_next_week_activities = 0 
    length_next_month_activities = 0 
    

    activity_category_count = {}
    for key,value in ACTIVITY_CATEGORY_MAP.iteritems():
        activity_category_count[key] = 0

    watchlist_activities = user_login.get_profile().watchlist.all()
    length_all_activities = len(watchlist_activities)
    for activity in watchlist_activities:
        if (activity.end_time != None):
            if ((check_yesterday(activity,current_time,current_timezone))):
                length_yesterday_activities += 1

        if (check_today(activity,current_time,current_timezone)) :
            length_today_activities += 1
        if ((check_upcoming(activity.start_time,datetime.now()))):
            length_upcoming_activities += 1
        if ((check_past(activity.start_time, datetime.now()))):
            length_past_activities += 1
        
        if ((check_tomorrow(activity,current_time,current_timezone))):
            length_tomorrow_activities += 1
        if ((check_last_week(activity,current_time,current_timezone))):
            length_last_week_activities += 1
        if ((check_next_week(activity,current_time,current_timezone))):
            length_next_week_activities += 1
        if ((check_next_month(activity.start_time,datetime.now()))):
            length_next_month_activities += 1        

        activity_category_count[activity.category] = activity_category_count[activity.category] + 1   

    return render_to_response(
                    'apps/watchlist/page/main_page.html',
                    {
                        "today_activities" : today_activities,
                        "length_yesterday_activities" : length_yesterday_activities,
                        "length_upcoming_activities" : length_upcoming_activities,
                        "length_past_activities" : length_past_activities,
                        "length_today_activities" :length_today_activities,
                        "length_tomorrow_activities" : length_tomorrow_activities,
                        "length_next_week_activities" : length_next_week_activities,
                        "length_next_month_activities" : length_next_month_activities,
                        "length_all_activities" : length_all_activities,
                        "length_last_week_activities" : length_last_week_activities,
                        "activity_category_count": activity_category_count
                    },  
                    context_instance=RequestContext(request)
                    )
