$(function() {

	if ($("#is_activate_message").val() == "True") {
		$("#sidebar_left").css("margin-top","40px");
		$("#sidebar_right").css("margin-top","40px");
		$("#main_section #content").css("margin-top","40px");
	}

	$.each($('#watchlist_list .collapse'), function() {
	    $(this).on('show.bs.collapse', function() {
	        $('[href=#' + $(this).attr('id') + '] i').removeClass('fa-caret-right').addClass('fa-caret-down');
	    }).on('hide.bs.collapse', function() {
	        $('[href=#' + $(this).attr('id') + '] i').removeClass('fa-caret-down').addClass('fa-caret-right');
	    });
	});

	$('#content').scrollspy({ target: '#watchlist_content' });

	$('#watchlist_list>li>ul>li').on('click','a',function(){
		Dajaxice.frittie.apps.watchlist.get_watch_list_activities(getWatchListActivitiesCallback,{ 
			'filter_argument':$(this).attr('data-value')
		});
	    $('#watchlist_list').find('li').removeClass('active');
	    $(this).closest('li').addClass('active');
	    showAjaxLoadingIcon();
	});

})