$(function() {
  if ($("#is_activate_message").val() == "True") {
    $("#main_section .main-area").css("margin-top","60px");
  }
})

$(function() {

  $("#send_message_btn").click(function() {
    var message_content = $("#message_textarea").val();
    if (stripWhitespace(message_content).length != 0) {
        Dajaxice.frittie.apps.message.send_message(sendMessageCallback,{
              'conversation_unique_id': conversation_unique_id,
              'message_content': message_content
        })
    } 
  })

  var user_data = new Bloodhound({
      datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.label); },
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      prefetch: {
        url: "/search/autocomplete/people/",
        ttl: 600000,
      }
  });
     
  user_data.initialize();
     
  $('#find_people_input').typeahead(null, {
      displayKey: 'label',
      source: user_data.ttAdapter(),
      templates: {
        suggestion: Handlebars.compile([
            '<p class="picture pull-left"><img src="{{picture}}"/></p>',
            '<p class="name pull-left">{{label}}</p>'
        ].join(''))
      }
  })
  .on('typeahead:selected', function($e,datum) {
      $("#user_chat_username").val(datum['value']);
  });

  $("#compose_new_message_btn").click(function() {
      var new_message_content = $("#new_message_textarea").val();
      if (stripWhitespace(new_message_content).length != 0) {
          var user_chat_username = $("#user_chat_username").val();
          Dajaxice.frittie.apps.message.compose_new_message(composeNewMessageCallback,{
                'message_content': new_message_content,
                "user_chat_username": user_chat_username
          })
          $("#compose_new_message_modal").modal("hide");
          showAjaxLoadingIcon();
      } 
  })

})