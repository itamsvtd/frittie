function sendMessageCallback(data) {
	hideAjaxLoadingIcon();
	if (data['results']['success']) {
		var data = data['results']['snippet_return']
		var el = "ul#message_list";
		appendNewItem(el,data);
	    $('#message_textarea').val("")
	} else {
	    showAlertMessage(data['results']['error_message'])
	}
}

function composeNewMessageCallback(data) {
	hideAjaxLoadingIcon();
	if (data['results']['success']) {

		//sendEmail(data['results']['email_tracking_id']);

		// Clear previous data
		$("#find_people_input").val("");
		$('#new_message_textarea').val("")
		$("#user_chat_username").val("");

		// Show successfuly message and render the html 
		//showAlertMessage(data['results']['success_message'])
		if (data['results']['is_conversation_exist']) {
			var list_conversation = $(".conversation-id");
			var conversation_el;
			for (var i = 0; i < list_conversation.length; i++) {
				if ( $(list_conversation[i]).val() == data['results']['conversation_unique_id'] ) {
					conversation_el = list_conversation[i];
					break;
				}
			}
			$($(conversation_el).closest("li.conversation-item")).remove();
		} 

		var data = data['results']['snippet_return']
		var el = "ul#conversation_list";
		if (isExist("#no_message")) {
			$("#no_message").remove();
			$('.right-section').append("<ul id='conversation_list'></ul>");
		}
		prependNewItem(el,data);
      	scrollPageTop();
 	} else {
      	showAlertMessage(data['results']['error_message'])
  	}
}