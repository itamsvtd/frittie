$(window).scroll(function(){
    if  (isTriggerLoadingScroll()){
   		if ($("#media_tab").hasClass("active")) {
   			loadMoreMedia();
   		}
   	}
}); 

$(function() {


	stretchWindowMinHeight("#basic_profile");
	stretchWindowMinHeight("#profile_content");

	stretchToElementHeight("#basic_profile","#profile_content");


	var lastScrollTop = 0;
	$(window).scroll(function(event){
		if ($(window).scrollTop() > 200) {
			$("#scroll_top_btn").removeClass("hide");
		} else {
			$("#scroll_top_btn").addClass("hide");
		}
	});

	$("#scroll_top_btn").click(function() {
		scrollPageTop();
		$(this).addClass("hide");
	})


	$("#update_avatar_file").change(function() {
      $("#user_avatar_form").submit();
      showAjaxLoadingIcon()
    })

    $("#update_cover_picture_file").change(function() {
      $("#user_cover_picture_form").submit();
      showAjaxLoadingIcon();
    })
})

$(function() {
	$(".user-profile-tab").click(function() {
		if ($(this).hasClass("active") == false) {
			var action_type = $(this).attr("data-value");
			switch (action_type) {
			  	case 'host': 
			  		Dajaxice.frittie.apps.member.load_activity(loadContentCallback,{
				      	"load_type": "host", 
				      	"user_view_username": user_view_username,
				    });
				    showAjaxLoadingIcon();
			  		break;
			  	case 'attending': 
			  		Dajaxice.frittie.apps.member.load_activity(loadContentCallback,{
				      	"load_type": "attending", 
				      	"user_view_username": user_view_username,
				    });
				    showAjaxLoadingIcon();
			  		break;
			  	case 'watchlist': 
			  		Dajaxice.frittie.apps.member.load_activity(loadContentCallback,{
				      	"load_type": "watchlist", 
				      	"user_view_username": user_view_username,
				    });
				    showAjaxLoadingIcon();
			  		break;
			  	case 'my_places': 
			  		Dajaxice.frittie.apps.member.load_location(loadContentCallback,{
				      	"user_view_username": user_view_username,
				      	"location_type": "create",
				    });
				    showAjaxLoadingIcon();
			  		break;
			  	case "my_tickets":
			  		Dajaxice.frittie.apps.member.load_tickets(loadContentCallback, {})
			  		showAjaxLoadingIcon();
			  		break;
			  	case 'interest_places': 
			  		Dajaxice.frittie.apps.member.load_location(loadContentCallback,{
				      	"user_view_username": user_view_username,
				      	"location_type": "follow"
				    });
				    showAjaxLoadingIcon();
			  		break;
			  	case 'followers': 
			  		Dajaxice.frittie.apps.member.load_related_user(loadContentCallback,{
				      	"user_type": "followers",
				      	"user_view_username": user_view_username,
				    });
				    showAjaxLoadingIcon();
			  		break;
			  	case 'following': 
			  		Dajaxice.frittie.apps.member.load_related_user(loadContentCallback,{
				      	"user_type": "following",
				      	"user_view_username": user_view_username,
				    });
				    showAjaxLoadingIcon();
			  		break;
			  	case 'media':
			  		Dajaxice.frittie.apps.member.load_media(loadContentCallback,{
				      	"user_view_username": user_view_username,
				    });
				    showAjaxLoadingIcon();
				    break;
			  	default:  break;
			}
		}
	})
})

$(function() {
	$("#follow_handler_btn").hover(
		function() {
			if ($(this).val() == FOLLOWING) {
				$(this).html("Unfollow");
				$(this).removeClass("btn-primary");
				$(this).addClass("btn-danger");
			}
		},
		function() {
			if ($(this).val() == FOLLOWING) {
				$(this).html("Following");
				$(this).removeClass("btn-danger");
				$(this).addClass("btn-primary");
			}
		}
	)

	$("#follow_handler_btn").click(function() {
		checkLogin(redirect_link);
		if ($(this).val() == NOT_FOLLOW) {
			Dajaxice.frittie.apps.member.follow_user(followUserCallback, {
				"user_view_username": user_view_username,
			})
		} else {
			Dajaxice.frittie.apps.member.unfollow_user(unfollowUserCallback,{
				"user_view_username": user_view_username,
			})
		}
	})
});

$(function() {

	/* HANDLE MESSAGING TO EVENT HOST */
    $("#message_user_btn").click(function() {
      $("#compose_new_message_modal").modal("show");
      var user_view_username = $("#user_view_username").val();
      var user_view_fullname = stripWhitespace($("#user_fullname").text());
      $("#find_people_input").val(user_view_fullname);
      $("#find_people_input").attr("disabled","disabled");
      $("#user_chat_username").val(user_view_username);
    })


    $("#compose_new_message_btn").click(function() {
      var new_message_content = $("#new_message_textarea").val();
      if (stripWhitespace(new_message_content).length != 0) {
          var user_chat_username = $("#user_chat_username").val();
          Dajaxice.frittie.apps.message.compose_new_message(composeNewMessageCallback,{
                'message_content': new_message_content,
                "user_chat_username": user_chat_username
          })
          $("#compose_new_message_modal").modal("hide");
          showAjaxLoadingIcon();
      } 
    })
    /********************************/
})

$(function(){
    /* CLEAR MODAL ERROR AFTER MODAL IS HIDDEN */
    $(".clear-modal-error").on('hidden.bs.modal',function() {
        clearModalError(this);  
    })

    /**** REPORT MEMBER ****/
    $("#report_user_final_btn").click(function() {
        var report_content = $("#report_user_textarea").val();
        if (stripWhitespace(report_content).length != 0) {
            Dajaxice.frittie.apps.member.report_user(reportUserCallback,{
              "username": $("#report_user_username").val(),
              "report_content": report_content
            })
            $("#report_user_modal").modal("hide");
            showAjaxLoadingIcon();
        } else {
            $("#report_user_textarea").addClass("error");
            $("#report_user_modal .modal-body p").addClass("error");
        } 
    })
    /*************************/
})


$(function() {
	/* SETTING PAGE */
	$("#settings_form select#id_country").change(function(){
		if ($(this).val() == "US") {
			$("#settings_form .state").removeClass("hide");
		} else {
			$("#settings_form .state").addClass("hide");
		}
	})
})
