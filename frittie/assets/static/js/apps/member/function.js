var user_view_username = $("#user_view_username").val();
var redirect_link = "/people/" + user_view_username;
var total_media = $("#total_media").val();

function loadMoreMedia() {
	var num_media_load = parseInt($("#num_media_load").val());
	if (num_media_load != -1 ) {
		if (num_media_load < total_media) {
			showAjaxLoadingIcon();
			Dajaxice.frittie.apps.member.load_more_media(loadMoreMediaCallback,{
				"num_media_load": num_media_load,
				"user_view_username": user_view_username
			})
		}
	} 
}