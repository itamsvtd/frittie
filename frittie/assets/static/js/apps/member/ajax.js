function loadContentCallback(data) {
	hideAjaxLoadingIcon();
	if (data['results']['success']) {
		var load_type = data['results']['load_type']
		$(".user-profile-tab").removeClass("active");
		$("div.user-profile-tab[data-value='" + load_type + "']").addClass("active");
		$("#details_area").html(data['results']['snippet_return']);
		addDotToLongTextWithElementAttr(".add-dot","data-html-maxlength");
		stretchToElementHeight("#live_feed","#profile_content");
		stretchToElementHeight("#basic_profile","#profile_content");
	} else {
		showAlertMessage(data['results']['error_message']);
	}
}

function followUserCallback(data) {
	hideAjaxLoadingIcon();
	if (data['results']['success']) {
		$("#follow_handler_btn").html("Following");
		$("#follow_handler_btn").val(FOLLOWING);
		$("#follow_handler_btn").removeClass("btn-default btn-danger");
		$("#follow_handler_btn").addClass("btn-primary");
	} else {
		showAlertMessage(data['results']['error_message']);
	}
}

function unfollowUserCallback(data) {
	hideAjaxLoadingIcon();
	if (data['results']['success']) {
		$("#follow_handler_btn").html("Follow");
		$("#follow_handler_btn").val(NOT_FOLLOW);
		$("#follow_handler_btn").removeClass("btn-primary btn-danger");
		$("#follow_handler_btn").addClass("btn-default");
	} else {
		showAlertMessage(data['results']['error_message']);
	}
}

function reportUserCallback(data) {
	hideAjaxLoadingIcon();
	if (data['results']['success']) {
	  	showAlertMessage(data['results']['success_message'])
	} else {
	   	showAlertMessage(data['results']['error_message'])
	}
}

function composeNewMessageCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
    Dajaxice.frittie.apps.message.send_email_notification(Dajax.process, {
      "message_unique_id": data['results']['message_unique_id'],
      "conversation_unique_id": data['results']['conversation_unique_id'],
      "email_to": data['results']['email_to']
    });

    // Clear previous data
    $("#find_people_input").val("");
    $('#new_message_textarea').val("")
    $("#user_chat_username").val("");

    // Show successfuly message and render the html 
    showAlertMessage(data['results']['success_message'])
    
  } else {
    showAlertMessage(data['results']['error_message'])
  }
}

function loadMoreMediaCallback(data) {
	hideAjaxLoadingIcon();
	if (data['results']['success']) {
		var snippet_return = data['results']['snippet_return'];
		$("#photo_list").append(snippet_return);
		$("#num_media_load").val(data['results']['num_media_load']);		
		stretchToElementHeight("#live_feed","#profile_content");
		stretchToElementHeight("#basic_profile","#profile_content");
		$("#scroll_top_btn").removeClass("hide");
	} 
}