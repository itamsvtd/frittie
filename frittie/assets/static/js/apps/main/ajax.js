function filterHomePageActivityCallback(data) {
	hideAjaxLoadingIcon();
    if (data['results']['success']) {
      $("#content_area").html(data['results']['snippet_return']);
      $("#total_activities_count").html(data['results']['num_activities']);
      if (data['results']['num_activities'] == 0) {
        $("#no_activity_found").removeClass("hide");
      } else {
        $("#no_activity_found").addClass("hide");
      }
      addDotToLongTextWithElementAttr(".add-dot","data-html-maxlength");
      //cropImages(".image-container");
    } else {
      showAlertMessage(data['results']['error_message'])
    }
}

function filterHomePageLocationCallback(data) {
	hideAjaxLoadingIcon();
    if (data['results']['success']) {
      $("#content_area").html(data['results']['snippet_return']);
      $("#total_locations_count").html(data['results']['num_locations']);
      if (data['results']['num_locations'] == 0) {
        $("#no_location_found").removeClass("hide");
      } else {
        $("#no_location_found").addClass("hide");
      }
      addDotToLongTextWithElementAttr(".add-dot","data-html-maxlength");
      //cropImages(".image-container");
    } else {
      showAlertMessage(data['results']['error_message'])
    }
}

function loadMoreActivityCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
    var snippet_return = data['results']['snippet_return'];
    $("#content_area").append(snippet_return);
    $("#is_loading_item").val("False");
    $("#num_activities_load").val(data['results']['num_activities_load']);    
    //cropImages(".image-container");
    addDotToLongTextWithElementAttr(".add-dot","data-html-maxlength");
    $("#scroll_top_btn").removeClass("hide");
  } 
}

function loadMoreLocationCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
    var snippet_return = data['results']['snippet_return'];
    $("#content_area").append(snippet_return);
    $("#is_loading_item").val("False");
    $("#num_locations_load").val(data['results']['num_locations_load']);    
    //cropImages(".image-container");
    addDotToLongTextWithElementAttr(".add-dot","data-html-maxlength");
    $("#scroll_top_btn").removeClass("hide");
  } 
}