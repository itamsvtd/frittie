// $(window).scroll(function(){
//     if  (isTriggerLoadingScroll()){
//         if (isExist("#activity_listing_section")) {
//           if ($("#is_loading_item").val() == "False") {
//             loadMoreActivity();
//           } 
//         } else if (isExist("#location_listing_section")) { 
//           if ($("#is_loading_item").val() == "False") {
//             loadMoreLocation();
//           }       
//         }
//     }
// }); 

$(function(){
	  $("ul.dropdown-menu li.dropdown-item").click(function() {
		  var el = $($(this).closest('div.dropdown')[0]).find(".dropdown-value")[0];
		  $(el).html($(this).text())
	  })

	  // if ($("#is_activate_message").val() == "True") {
   //    	$("#activities_toolbar").css("margin-top","40px");
  	// }
}) 

$(function() {

    var lastScrollTop = 0;
    $(window).scroll(function(event){
      if ($(window).scrollTop() > 200) {
        $("#scroll_top_btn").removeClass("hide");
      } else {
        $("#scroll_top_btn").addClass("hide");
      }
    });

    $("#scroll_top_btn").click(function() {
      scrollPageTop();
      $(this).addClass("hide");
    })
})

$(function(){
    if (isExist("#num_activities_load")) {
      if (parseInt($("#num_activities_load").val()) == 0) {
        $("#no_activity_found").removeClass("hide");
      } else {
        $("#no_activity_found").addClass("hide");
      }
    }

    if (isExist("#num_locations_load")) {
      if (parseInt($("#num_locations_load").val()) == 0) {
        $("#no_location_found").removeClass("hide");
      } else {
        $("#no_location_found").addClass("hide");
      }
    }
})
    

$(function() {
    $("#find_place_link").click(function() {
      runTopProgressBar();
      var link = window.location.href;
      if (link.indexOf("lat") != -1 ) {
        link = link + "&location=True";
      } else {
        link = "/?location=True";
      }
      window.location.href = link;
    });

    $("#find_activity_link").click(function() {
      runTopProgressBar();
      var link = window.location.href;
      if (link.indexOf("lat") != -1 ) {
        link = link.substring(0,link.indexOf("&location"));
      } else {
        link = "/";
      }
      window.location.href = link;
    })
})


