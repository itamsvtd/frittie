 $(function() {

 // 	if ($("#is_activate_message").val() == "True") {
	// 	$("#activity_section").css("margin-top","42px");
	// 	$("#show_feed_btn").css("margin-top","42px");
	// 	$("#live_feed").css("margin-top","42px");
	// 	$("#zoom_in").css("margin-top","42px");
	// 	$("#zoom_out").css("margin-top","42px");
	// }

  	$("#activity_category_dropdown li").click(function() {
    	var el = $($(this).closest('div.dropdown')[0]).find(".dropdown-value")[0];
    	$(el).html($(this).text())
  	})

 	$(".activity-logo a").click(function() {
	    var parent_el = $(this).closest("li.activity-item")[0];
	    var activity_unique_id = $(parent_el).attr("data-unique-id");
	    var lat = $($(parent_el).find(".activity-location-lat")[0]).val();
	    var lng = $($(parent_el).find(".activity-location-lng")[0]).val();
	    showActivityOverlayInfo(activity_unique_id,lat,lng)
	})

	if ($("#is_guest_explore").val() == "True") {
		$(".map-content-section").css({"right":"0px"});
	}

})
  