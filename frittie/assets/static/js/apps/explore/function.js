var map;
var activity_overlays_data = getListActivityLocationDataMapping();

google.maps.event.addDomListener(window, 'load', function() {
    
    map = initializeMap("map_section");
    var zoom_control_div = document.createElement('div');
    var zoom_control = new zoomControlHandler(zoom_control_div, map);

    if (location.search.indexOf("lat") == -1 ) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var my_position =  new google.maps.LatLng(position.coords.latitude.toFixed(2), position.coords.longitude.toFixed(2));
            map.setCenter(my_position);
        });
    } 

    Dajaxice.frittie.apps.activity.get_activity_overlays(getActivityOverlaysCallback,{
    	'data': activity_overlays_data
    });

    google.maps.event.addListener(map, 'dragend', function() {
        var lat = map.getCenter().lat();
        var lng = map.getCenter().lng();
        Dajaxice.frittie.apps.activity.get_nearby_activity(getNearbyActivityCallback,{
            "lat": lat,
            "lng": lng,
            "radius": 8000,
        })
        showAjaxLoadingIcon();
    });
}); 
 

