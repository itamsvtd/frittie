var activity_unique_id = $("#activity_unique_id").val();
var redirect_link = "/activity/" + activity_unique_id;

function initDeleteComment(el) {
  var parent_el = $(el).closest("li.comment-item");
  var comment_pk = $(parent_el).attr("data-unique-id");
  $("#delete_comment_unique_id").val(comment_pk);
}

function initEditComment(el) {
  var parent_el = $(el).closest("li.comment-item");
  var comment_pk = $(parent_el).attr("data-unique-id");
  var text_el = $($(parent_el).find("p.comment-text")[0]).html();
  $("#publish_comment_btn").val("edit");
  $("#publish_comment_btn").html("Submit");
  $("#cancel_edit_comment_btn").removeClass("hide");
  $("#selected_comment_unique_id").val(comment_pk);
  $(".add-comment-area").css("width",475);
  $("#comment_textarea").val(text_el);
  $("#comment_textarea").focus();
  scrollPagePos($("#comment_tab").offset().top);
}

function showActivityRequestModal() {
  var value = $("#request_join_btn").val();
  if (value == "request_join") {
    $('#request_join_activity_modal').modal('show');
  } else if ( value == "cancel_request") {
    $('#cancel_request_join_activity_modal').modal('show');
  } else if ( value == "cancel_attending") {
    $('#cancel_attending_activity_modal').modal('show');
  }
}

function handleJoinRequestButtonHover() {
    $("#request_join_btn").hover(
      function() {
        if ($(this).val() == "waiting") {
          $(this).val("cancel_request");
          $(this).html("Cancel my request");
          $(this).addClass("btn-danger");
        } else if ($(this).val() == "attending") {
          $(this).val("cancel_attending");
          $(this).html("Cancel my attending");
          $(this).addClass("btn-danger");
        }
      },
      function() {
        if ($(this).val() == "cancel_request") {
          $(this).val("waiting");
          $(this).html("Waiting for accepted");
          $(this).removeClass("btn-danger");
        } else if ( $(this).val() == "cancel_attending" ) {
          $(this).val("attending");
          $(this).html('<i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;Attending');
          $(this).removeClass("btn-danger");
        }
      }
    )
}

// function join_activity_callback(data) {
// 	var waiting = "<input type='button' class='btn btn-primary disabled activity-waiting' href='#' value='Waiting Response'>"
// 	$(".handler-button").html(waiting);
// 	var s = "<p class='notice-message'>Your request has been sent to the host. Please wait for his/her response</p>"
// 	var el = '#show_notice_message'
// 	show_message(s,el)
// }

// function cancel_joining_callback(data) {
// 	$(".activity-accepted").remove();
// 	$("#join_handler").val("Join");
// 	var s = "<p class='notice-message'>You have canceled your attendance in this activity. Your message has been sent to the host</p>"
// 	var el = '#show_notice_message'
// 	show_message(s,el)
// }

// function recommend_friend_callback(data) {
// 	$("#modal_friend").modal('hide');
// 	$('#modal_friend input[type=checkbox]').attr('checked', false);
// 	var s = '<p class="notice-message">Your recommendation has been sent to your friends</p>'
// 	var el = '#show_notice_message'
// 	show_message(s,el)
// }

// function upload_activity_photo_callback(data) {
// 	$('#modal_upload_photo').modal('show')
// }

if ($("#is_location_model").val() == "True") {
  var map;
  var activity_location_lat = $("#activity_location_lat").val();
  var activity_location_lng = $("#activity_location_lng").val();
  google.maps.event.addDomListener(window, 'load', function() {
      var map_options = {
        center: new google.maps.LatLng(activity_location_lat,activity_location_lng)
      }
      map = initializeMap("map_canvas",map_options);
      var marker = new google.maps.Marker({
          position : new google.maps.LatLng(activity_location_lat,activity_location_lng),
          map: map
      });
  }); 
}


 

