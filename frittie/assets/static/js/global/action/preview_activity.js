function previewActivityWatchlistHandler(el) {
  var watch_value = $($(el).find(".watch-value")[0]).val();
  var unique_id = $(el).attr("data-unique-id");

  var redirect_link = "/activity/" + unique_id;
  checkLogin(redirect_link);

  if (watch_value == NOT_WATCH) {
      Dajaxice.frittie.apps.activity.watch_activity(previewActivityWatchCallback,{
          'unique_id':unique_id, 
      });
  } else {
      Dajaxice.frittie.apps.activity.unwatch_activity(previewActivityUnwatchCallback,{
          'unique_id':unique_id,
      });
  }
}

function showPreviewActivity(unique_id) {
    showAjaxLoadingIcon();
    var parent_el = $(".activity-item[data-unique-id='" + unique_id + "']");
    var prev_unique_id = setNullForUndefined($(parent_el).prev().attr("data-unique-id"));
    var next_unique_id = setNullForUndefined($(parent_el).next().attr("data-unique-id"));
    var current_pos = $(parent_el).attr("data-item-number");
    var total_count = $("#num_activities_load").val();
    Dajaxice.frittie.apps.activity.get_preview_activity(getPreviewActivityCallback,{
        "unique_id": unique_id,
        "prev_unique_id": prev_unique_id,
        "next_unique_id": next_unique_id,
        'current_pos': current_pos,
        'total_count': total_count,
    })
}

