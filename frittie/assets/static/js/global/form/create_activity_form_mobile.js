var location_data = new Bloodhound({
    datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.label); },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    limit: 100,
    prefetch: {
        url: "/search/autocomplete/location/",
        ttl: 0,
    }
});
                 
location_data.initialize();

$('#create_activity_modal_mobile').on('shown.bs.modal', function (e) {
    var map_options = {
      zoom: 16,
      scrollwheel: true,
      zoomControl: true,
    }
    var create_activity_map = initializeMap("create_activity_map_canvas",map_options);
    if (location.search.indexOf("lat") == -1 ) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var my_position =  new google.maps.LatLng(position.coords.latitude.toFixed(2), position.coords.longitude.toFixed(2));
            create_activity_map.setCenter(my_position);
        });
    } 
}); 
 

$(function() {
	  
    var validate_input_change = function(input) {
        var is_form_clicked = $("#is_create_activity_form_mobile_clicked").val(); 
        if (is_form_clicked == "true") {
              
          // Clear previous error field
          handleInputError(true,input,"required");
          handleInputError(true,input,"numeric");
          handleInputError(true,input,"unique-id-correct");

          // Validate the input
          validateInput(this,false,"tt-hint")

          // Make change to the group header 
          var group = $(this).closest(".form-group");
          var is_valid = validateGroupInput(group,true,"tt-hint");
          if (is_valid) {
              $("#create_activity_form_mobile .error-notice-mobile").addClass("hide");
          } else {
              $("#create_activity_form_mobile .error-notice-mobile").removeClass("hide");
              if ( ($("#create_activity_form_mobile .activity-location-mobile .error-field.required").hasClass("hide") == false) &&
                ($("#create_activity_form_mobile .activity-location-mobile .error-field.unique-id-correct").hasClass("hide") == false)) {
                $("#create_activity_form_mobile .activity-location-mobile .error-field.unique-id-correct").addClass("hide");
              }
          }
        }
    }

    var create_activity_form_mobile = $("#create_activity_form_mobile");


  	/****** VALIDATE FORM AND INPUT	*******/
    $("#create_activity_btn_mobile").click(function() {
        $("#is_create_activity_form_mobile_clicked").val("true");
        var is_valid = true;
        var groups = $("#create_activity_form_mobile .form-group");
        for (var i = 0; i < groups.length; i++) {
        	var group_header = $(groups[i]).find(".form-header");
        	var is_group_valid = validateGroupInput(groups[i],false,"tt-hint");
        	if (is_group_valid) {
        		$(group_header).removeClass("has-error"); 
        	} else {
        		$(group_header).addClass("has-error");
        		is_valid = false;
        	}
        }
        if (is_valid) {
          $("#create_activity_form_mobile .error-notice-mobile").addClass("hide");
          $(create_activity_form_mobile).submit();
          showAjaxLoadingIcon();
        } else {
          $("#create_activity_form_mobile .error-notice-mobile").removeClass("hide");
          if ( ($("#create_activity_form_mobile .activity-location-mobile .error-field.required").hasClass("hide") == false) &&
            ($("#create_activity_form_mobile .activity-location-mobile .error-field.unique-id-correct").hasClass("hide") == false)) {
            $("#create_activity_form_mobile .activity-location-mobile .error-field.unique-id-correct").addClass("hide");
          }
        }

    })  

    $("#create_activity_form_mobile input").each(function() {
        $(this).change(function() {
          	validate_input_change(this);
        })
    }); 

    $("#create_activity_form_mobile textarea").each(function() {
        $(this).change(function() {
            validate_input_change(this);
        })
    }); 
})


$(function() {

    /* HANDLE LIMIT PEOPLE INPUT */
    $("#create_activity_form_mobile #unlimited_people_checkbox").change(function() {
       	if($(this).is(":checked")) {
          	$("#create_activity_form_mobile #id_limit").attr("disabled","disabled");
           	$("#create_activity_form_mobile #id_limit").removeAttr("required");
           	$("#create_activity_form_mobile #id_limit").removeAttr("numeric");
           	$("#create_activity_form_mobile #id_limit").val("");
           	handleInputError(true,this,"required");
           	handleInputError(true,this,"numeric");
           	$($(this).closest(".form-group").find(".form-header")[0]).removeClass("has-error");
      	} else {
           	$("#create_activity_form_mobile #id_limit").removeAttr("disabled")
           	$("#create_activity_form_mobile #id_limit").attr("required","required");
           	$("#create_activity_form_mobile #id_limit").attr("numeric","numeric");
       	}      
   	});
})
   	

/*** LOCATION FIELD AUTOCOMPLETE AND TAG ***/
$(function() {

    var address_picker = new AddressPicker();

    $('#create_activity_form_mobile #id_activity_place').typeahead(null, 
      {
        name: "frittie-place",
        displayKey: 'label',
        source: location_data.ttAdapter(),
        templates: {
        	suggestion: Handlebars.compile([
            	'<p class="picture pull-left"><img src="{{picture}}"/></p>',
            	'<p class="name pull-left">{{label}}</p>'
        	].join(''))
    	  }
      },
      {
        name: 'google-place',
        displayKey: 'description',
        source: address_picker.ttAdapter(),
        templates: {
            suggestion: Handlebars.compile([
              '<p class="picture pull-left"><i class="fa fa-map-marker"></i></p>',
              '<p class="name pull-left">{{description}}</p>'
            ].join(''))
        }
      }
    )
    .on('typeahead:selected', function($e,datum) {
        var remove_location_btn = '<div class="glyphicon glyphicon-remove remove-location-btn" onclick="' + 
                  'enableInputField(' + "'" +  '#create_activity_form_mobile #id_activity_place' + "'" + ');' + 
                  '$(this).remove();' +
                  "$('#create_activity_form_mobile .location-unique-id').val('');" + 
                  '"></div>'
        $("#create_activity_form_mobile .activity-location-mobile .input-field").append(remove_location_btn);
        $("#create_activity_form_mobile #id_activity_place").attr("disabled","disabled");
        $("#create_activity_form_mobile #id_activity_place").css("background-color","#eee");

        var map_options = {
            zoom: 16,
            scrollwheel: true,
            zoomControl: true,
        }

        var activity_map = initializeMap("create_activity_map_canvas",map_options);
        var position;

        if (typeof datum['value'] !== 'undefined') {
          $("#create_activity_form_mobile .is-google-place").val(false);
          $("#create_activity_form_mobile .location-unique-id").val(datum['value']);
          position = new google.maps.LatLng(datum['lat'],datum['lng']);
        } else {
          $("#create_activity_form_mobile .is-google-place").val(true);
          $("#create_activity_form_mobile .google-place-id").val(datum['id']);
          var address = datum['description'];
          geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              var address_components = results[0].address_components;
              var address_info = getAddressInfo(address_components);
              position = results[0].geometry.location;
              var lat = position.lat();
              var lng = position.lng();
              $("#create_activity_form_mobile .google-place-lat").val(lat);
              $("#create_activity_form_mobile .google-place-lng").val(lng);
              $("#create_activity_form_mobile .google-place-address").val(address_info.address);
              $("#create_activity_form_mobile .google-place-city").val(address_info.city);
              $("#create_activity_form_mobile .google-place-country").val(address_info.country);
              if (isUndefined(address_info.state) == false) $("#create_activity_form_mobile .google-place-state").val(address_info.state);
              if (isUndefined(address_info.state) == false)$("#create_activity_form_mobile .google-place-zipcode").val(address_info.zip_code);
            }
          });
        }
        
        activity_map.setCenter(position);
        var marker = new google.maps.Marker({
            map: activity_map,
            position: position,
            animation: google.maps.Animation.DROP,
        });

    });
})

/********* HANDLE TIME **********/ 
$(function() {

    var start_date = $("#create_activity_form_mobile #start_date").datepicker({ 
      todayBtn: 'linked'
      }).on('changeDate', function(ev) {
      	var select_date = new Date(ev.date);
        end_date.setStartDate(select_date);
        start_date.hide();
  	}).data('datepicker');

  	var end_date = $("#create_activity_form_mobile #end_date").datepicker({
      todayBtn: 'linked'
    }).on('changeDate', function(ev) {
	      end_date.hide();
  	}).data('datepicker');


    $("#create_activity_form_mobile #show_end_time_link").click(function() {
        $(this).hide();
        $("#create_activity_form_mobile .activity-start-time label").html("From")
        $("#create_activity_form_mobile .activity-end-time").removeClass("hide");
        $("#create_activity_form_mobile .activity-end-time fieldset").removeClass("optional");
        $("#create_activity_form_mobile #end_date").attr("required","required");
        $("#create_activity_form_mobile #end_time").attr("required","required");
    })

   	$("#create_activity_form_mobile #remove_end_time_btn").click(function() {
   		var end_time_fieldset = $("#create_activity_form_mobile .activity-end-time").find("fieldset")[0];
        $("#create_activity_form_mobile #end_date").val("");
        $("#create_activity_form_mobile .activity-start-time label").html("When*")
        $("#create_activity_form_mobile .activity-end-time").addClass("hide");
        $("#create_activity_form_mobile .activity-end-time fieldset").addClass("optional");
        $("#create_activity_form_mobile #show_end_time_link").show();
        $("#create_activity_form_mobile #end_date").removeAttr("required");
        $("#create_activity_form_mobile #end_time").removeAttr("required");
   	})
   	
})

/* HANDLE CLOSING MODAL */
$(function() {

   	$("#create_activity_modal").on("hidden.bs.modal",function(e) {
      if ($("#create_activity_form_mobile .error-notice-mobile").hasClass("hide") == false) {
        enableInputField($("#id_activity_place"));
        $("#create_activity_form_mobile #remove_location_btn").hide(); 
        $("#create_activity_form_mobile .error-notice-mobile").addClass("hide");
        $("#create_activity_form_mobile .error-field").addClass("hide");
        $("#create_activity_form_mobile .has-error").removeClass("has-error");
      } 
   	})
})




