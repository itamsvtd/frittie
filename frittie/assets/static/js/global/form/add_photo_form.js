$(function() {
	$("#submit_photo_form_btn").click(function(e) {
		e.preventDefault();
		if ($("#photo_image").val() != "") {
			$("#add_photo_form").submit();
		} else {
			$("#add_photo_form .photo-image .error-field.required").removeClass("hide");
		}
	})

	$('#photo_image').bind('change', function() {
		$("#add_photo_form .photo-image .error-field").addClass("hide");
		if (this.files[0].size > VALID_FILE_SIZE) {
			$("#add_photo_form .photo-image .error-field.image-size").removeClass("hide");
			$("#submit_photo_form_btn").attr("disabled","disabled");
		} else {
			$("#submit_photo_form_btn").removeAttr("disabled");
			showThumbnail('add_photo_thumbnail',this.files[0])
		}
	})
})

$(function(){
	if (isExist("#add_photo_upload_area") && isExist("#add_photo_image")) {
		dragAndShowThumbnail("add_photo_upload_area","add_photo_image","add_photo_thumbnail");
	}
});