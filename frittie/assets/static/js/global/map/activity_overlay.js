ActivitySingleOverlay.prototype = new google.maps.OverlayView();

function ActivitySingleOverlay(data){
	this.unique_id = data['activity_unique_id'];
	this.number = data['activity_number'];
	this.div_ = null;
	this.pos = new google.maps.LatLng(data['lat'],data['lng']);
}
					
//init your html element here
ActivitySingleOverlay.prototype.onAdd= function(){
	var me = this;
	var div = document.createElement('div');
	div.id = "single_overlay_" + this.number;
	div.className = "overlay single-overlay";
	div.setAttribute("data-position",this.pos.lat() + "," + this.pos.lng());
	div.setAttribute("data-activity-unique-id",me.unique_id);
	div.onclick = function() {
		hideInfoBox('.group-info-box');
		if (isExist(".single-info-box")) {
			var infobox_el = $(".single-info-box")[0];
			var box_id = $($(infobox_el).find(".info-box-wrapper")[0]).attr("id");
			if (box_id == "box_" + me.number) {
				hideInfoBox(infobox_el);
			} else {
				hideInfoBox(".single-info-box");
				showActivityInfoBox(me,me.unique_id);
				highlightActivity(me.number);
			}
		} else {
			showActivityInfoBox(me,me.unique_id);
			highlightActivity(me.number);
		}
	}
	div.innerHTML = this.number;
	this.div_ = div;
	var panes = this.getPanes();
	panes.overlayMouseTarget.appendChild(div);
}
					
ActivitySingleOverlay.prototype.draw = function(){
	var overlay_projection = this.getProjection();
	var position = overlay_projection.fromLatLngToDivPixel(this.pos);
	var div = this.div_;
	div.style.left = position.x + 'px';
	div.style.top = position.y + 'px';
}

ActivitySingleOverlay.prototype.onRemove= function(){
	this.div_.parentNode.removeChild(this.div_);
	this.div_ = null;
}


ActivityGroupOverlay.prototype = new google.maps.OverlayView();

function ActivityGroupOverlay(data,group_id){
	this.activities = data;
	this.div_ = null;
	this.group_id = group_id;
	this.pos = new google.maps.LatLng(data[0]['lat'],data[0]['lng']);
}
					
ActivityGroupOverlay.prototype.onAdd= function(){
	var me = this;
	var div = document.createElement('div');
	div.id = "group_overlay_" + this.group_id;
	div.className = "overlay group-overlay";
	div.setAttribute("data-position",this.pos.lat() + "," + this.pos.lng());
	var activity_unique_ids = "";
	for (var i  = 0; i < me.activities.length; i++) {
		activity_unique_ids += me.activities[i]['activity_unique_id'] + ","
	}
	div.setAttribute("data-activity-unique-id",activity_unique_ids.substring(0,activity_unique_ids.length-1));
	div.onclick = function() {
		hideInfoBox(".single-info-box");
		if (isExist(".group-info-box")) {
			var infobox_el = $(".group-info-box")[0];
			var box_id = $($(infobox_el).find(".info-box-wrapper")[0]).attr("id");
			if (box_id == "box_" + me.group_id) {
				hideInfoBox(infobox_el);
			} else {
				hideInfoBox(".group-info-box");
				showGroupActivitiesInfoBox(me);
			}
		} else {
			showGroupActivitiesInfoBox(me);
		}
		
	}
	div.innerHTML = this.activities.length;
	this.div_ = div;
	var panes = this.getPanes();
	panes.overlayMouseTarget.appendChild(div);
}
					
ActivityGroupOverlay.prototype.draw = function(){
	var overlay_projection = this.getProjection();
	var position = overlay_projection.fromLatLngToDivPixel(this.pos);
	var div = this.div_;
	div.style.left = position.x + 'px';
	div.style.top = position.y + 'px';
}

ActivityGroupOverlay.prototype.onRemove= function(){
	this.div_.parentNode.removeChild(this.div_);
	this.div_ = null;
}



