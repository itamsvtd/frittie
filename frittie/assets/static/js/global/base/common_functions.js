/*function emoticonize(text) {
    var result = text;
    var emoticons = new Array();
    emoticons.push ('<img witdh ="5px" height="5px" src="http://127.0.0.1:8000/media/images/emoticon/Favorite.ico"/>');
    emoticons.push ('<img witdh ="5px" height="5px" src="http://127.0.0.1:8000/media/images/emoticon/Refresh.ico"/> ');
    emoticons.push ('<img witdh ="5px" height="5px" src="http://127.0.0.1:8000/media/images/emoticon/Security.ico"/> ');   

    result = result.replace(/\:D/g, emoticons[0]);
    result = result.replace(/\:R/g, emoticons[1]);
    result = result.replace(/\:S/g, emoticons[2]);
    return result;
}*/

/*
function cropImageLogic(image_width, image_height, container_width, container_height) {
    
    var is_image_smaller_than_container = false;
    if (container_height >= image_height && container_width >= image_width) is_image_smaller_than_container = true;

    var css_width, csss_height, css_margin_top, css_margin_left; 
    if (is_image_smaller_than_container) {
      css_width = container_width;
      css_height = container_height;
      css_margin_top = 0;
      css_margin_left = 0;
    } else {
      var height_ratio = image_height / container_height;
      var width_ratio = image_width / container_width;
      if (height_ratio > width_ratio) {
        var scale_image_height = image_height / width_ratio;
        css_width = container_width;
        css_height = "auto"
        css_margin_left = "0px";
        css_margin_top = ( scale_image_height - container_height ) / 2 * -1;
      } else {
        var scale_image_width = image_width / height_ratio;
        css_width = "auto";
        css_height = container_height;
        css_margin_top = "0px";
        css_margin_left = (( scale_image_width - container_width ) / 2.0) * -1;
      }  
    }
     
    return  {
        width: css_width,
        height: css_height,
        marginTop: css_margin_top,
        marginLeft: css_margin_left
    }
}

function cropImages(el) {
  $(el).each(function(){
      var item = this;
      var image_item = $($(item).find("img.crop-image")[0]);

      var image = new Image();
      image.src = $(image_item).attr("src");

      var container_width = $(item).width();
      var container_height = $(item).height();
      var image_width = image.width;
      var image_height = image.height;

      var result = cropImageLogic(image_width,image_height,container_width,container_height);
      
      $(image_item).css({
        "width": result.width,
        "height": result.height,
        "margin-top": result.marginTop,
        "margin-left": result.marginLeft
      });     
  })
}

function cropIndividualImage(image_item) {
      var item = $(image_item).closest("div.image-container");
      var image = new Image();
      image.src = $(image_item).attr("src");

      var container_width = $(item).width();
      var container_height = $(item).height();
      var image_width = image.width;
      var image_height = image.height;

      var result = cropImageLogic(image_width,image_height,container_width,container_height);

      $(image_item).css({
        "width": result.width,
        "height": result.height,
        "margin-top": result.marginTop,
        "margin-left": result.marginLeft
      });     
}
*/

function listFilter(items,query) {
    var rg = new RegExp(query,'i');
    $(items).each(function(){
        if($.trim($(this).text()).search(rg) == -1) {
        $(this).closest("li").css('display', 'none');
      } 
      else {
        $(this).closest('li').css('display', '');
      }
    });
}

function htmlUtf8Decode(input){
  var e = document.createElement('div');
  e.innerHTML = input;
  return e.childNodes[0].nodeValue;
}

function runTopProgressBar() {
    NProgress.set(Math.random());
    NProgress.start();
    NProgress.inc();
    setTimeout(function(){
      NProgress.done();
    }, 15000);
}

function checkLogin(redirect_link) {
  if (is_login == "False") {
    window.location.href = WEBSITE_HOMEPAGE + "/accounts/login/?next=" + redirect_link;
  }
}

function isTriggerLoadingScroll() {
    // var wintop = $(window).scrollTop(), docheight = $(document).height(), winheight = $(window).height();
    // var  scrolltrigger = 0.95;
 
    // if  ((wintop/(docheight-winheight)) > scrolltrigger) {
    //   return true;
    // } 
    // return false;
    if ($(window).scrollTop() == $(document).height() - $(window).height()) {
      return true;
    }
    return false;
}

function showAlertMessage(message) {
  $("#alert_message").html(message);
  $("#alert_message").slideDown();
  setTimeout(function() {
    $("#alert_message").slideUp('slow');
  },10000)
}

function handleInputError(is_valid,input,validator_type) {
  if (!is_valid) {
      var error_field = $(input).closest("fieldset").find(".error-field." + validator_type);
      for (var i = 0; i < error_field.length; i++) {
          var error_el = error_field[i];
          ($(error_el).hasClass(validator_type)) ? $(error_el).removeClass("hide") : $(error_el).addClass("hide");
      }
      $(input).closest("div.input-field").addClass("has-error");
  } else {
      $($(input).closest("fieldset").find(".error-field." + validator_type)[0]).addClass("hide");
      $(input).closest("div.input-field").removeClass("has-error");
  }
}

function requiredValidator(input,is_result_only) {
    var is_valid = true;
    var validator_type = "required";
    if (stripWhitespace($(input).val()).length == 0) {
        is_valid = false;
    }
    if (!is_result_only) handleInputError(is_valid,input,validator_type)
    return is_valid
}

function urlValidator(input,is_result_only) {
    var is_valid = true;
    var validator_type = "valid-url";
    if (stripWhitespace($(input).val()).length != 0) {
        var regExp =/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;
        is_valid = regExp.test($(input).val());
    }
    if (!is_result_only) handleInputError(is_valid,input,validator_type)
    return is_valid
}

function numericValidator(input,is_result_only) {
    var is_valid = true;
    var validator_type = "numeric";
    if (isNumber($(input).val()) == false) {
        is_valid = false;
    } 
    if (!is_result_only) handleInputError(is_valid,input,validator_type)
    return is_valid;
}

function uniqueIdValidator(input,is_result_only) {
  var is_valid = true;
  var validator_type = "unique-id-correct";
  var value = $(input).val();
  if ( value.length != 27 || isNumber(value.substring(24,27)) == false || PREFIX_UNIQUE_ID.indexOf(value.substring(0,2)) == -1) {
    is_valid = false;
  }

  if (!is_result_only) handleInputError(is_valid,input,validator_type)
  return is_valid;
}

function phoneNumberValidator(input,is_result_only) {
  var is_valid = true;
  var validator_type = "valid-phone-number";
  var value = $(input).val();
  if (stripWhitespace(value).length != 0) {
      if (value.substring(0,1) == "+") {
          value = value.substring(1);
      } 
      if (isNumber(value) == false) {
        is_valid = false;
      }
  }
  if (!is_result_only) handleInputError(is_valid,input,validator_type)
  return is_valid;
}

function validateInput(input,is_result_only,ignore_class) {
    console.log(input);
    var is_valid = true;
    var is_validate_input = true;
    if ($(input).hasClass(ignore_class)) is_validate_input = false;
    if ($(input).closest("fieldset").hasClass('optional')) is_validate_input = false;

    if (is_validate_input) {

      if ($(input).attr("required") == "required") {
        var required_validate_result = requiredValidator(input,is_result_only);
        if (!required_validate_result) {
            is_valid = false;
        } else {
            if ($(input).attr("numeric") == "numeric") {
                var numeric_validate_result = numericValidator(input,is_result_only);
                if (!numeric_validate_result) is_valid = false;
            }
        }     
      }    
      if ($(input).attr("valid-phone-number") == "valid-phone-number") {
          var phone_number_validate_result = phoneNumberValidator(input,is_result_only);
          if (!phone_number_validate_result) {
              is_valid = false;
          }
      }   
      if ($(input).attr("unique-id-correct") == "unique-id-correct") {
          var unique_id_correct_validate_result = uniqueIdValidator(input,is_result_only);
          if (!unique_id_correct_validate_result) {
              is_valid = false;
          }
      }    
      if ($(input).attr("valid-url") == "valid-url") {
          var url_validate_result = urlValidator(input,is_result_only);
          if (!url_validate_result) {
              is_valid = false;
          }
      } 
    }
    return is_valid;
}

function validateGroupInput(group,is_result_only,ignore_class) {
    var is_valid = true;
    var group_validate_fields = $(group).find("input").toArray().concat($(group).find("textarea").toArray());
    for (var i = 0; i < group_validate_fields.length; i++) {
        var input = group_validate_fields[i]; 
        var input_validation_result = validateInput(input,is_result_only,ignore_class);
        if (!input_validation_result) is_valid = false;     
    }
    
    return is_valid;
}

function validateForm(form,is_result_only,ignore_class) {
    var is_valid = true;
    var validate_fields = $(form).find("input").toArray().concat($(form).find("textarea").toArray());
    for (var i = 0; i < validate_fields.length; i++) {
        var input = validate_fields[i]; 
        var input_validation = validateInput($(input),is_result_only,ignore_class);
        if (!input_validation) is_valid = false;
    }
    return is_valid;
}

function clearForm(form) {
    $(form).find("input").each(function() {
        $(this).val("");
    })

    $(form).find(".error-field").each(function() {
        $(this).addClass("hide");
    })

    $(form).find(".has-error").each(function() {
        $(this).removeClass("has-error");
    })
}

function clearFieldSet(fieldset) {
    $(fieldset).find("input").each(function() {
        $(this).val("");
    })

    $(fieldset).find(".error-field").each(function() {
        $(this).addClass("hide");
    })

    $(fieldset).find(".has-error").each(function() {
        $(this).removeClass("has-error");
    })
}

function clearModalError(modal_el) {
    var error_el = $(modal_el).find(".error");
    for (var i = 0; i < error_el.length; i++) {
        var el = error_el[i];
        $(el).removeClass("error");
    }
}

function disableInputField(el) {
    $(el).attr("disabled","disabled");
    $(el).css("background-color","rgb(238, 238, 238)");
}

function enableInputField(el) {
    $(el).removeAttr("disabled");
    $(el).css({"background-color":"transparent"});
    $(el).val("");
}

function getListActivityLocationDataMapping() {
    var output = [];
    $("li.activity-item").each(function() {
        if ($($(this).find(".is-location-model")[0]).val() == 'True') {
          var activity_unique_id = $(this).attr("data-unique-id");
          var activity_number = $(this).attr("data-item-number");
          var location_unique_id = $($(this).find(".location-unique-id")[0]).val();
          var data = {
              "activity_unique_id": activity_unique_id,
              "location_unique_id": location_unique_id,
              "activity_number": activity_number,
          }
          output.push(data);
        }      
    })
    return output;
}

function getListLocationUniqueId() {
   var output = [];
    $("li.location-search-item").each(function() {
        var location_unique_id = $(this).attr("data-unique-id");
        var location_number = $(this).attr("data-item-number");
        var data = {
            "location_unique_id": location_unique_id,
            "location_number": location_number,
        }
        output.push(data);
    })
    return output;
}

function getAddressInfo(address_components) {
  var state, country, zip_code, city;
  var address = "";
  for (var i = 0; i < address_components.length; i++) {
    var component = address_components[i];
    var component_type = component.types[0];
    if (component_type == "country") {
      country = component.short_name;
      break;
    }
  }

  for (var i = 0; i < address_components.length; i++) {
    var component = address_components[i];
    var component_type = component.types[0];
    
    if (component_type == "street_number") address = address + component.short_name + " ";
    if (component_type == "route") address = address + component.short_name;

    if (country == "US") {
      if (component_type == "administrative_area_level_1") state = component.short_name;
      if (component_type == "postal_code") zip_code = component.short_name;
      if (component_type == "locality") city = component.short_name;
    } else {
      if (component_type == "administrative_area_level_1") city = component.short_name;
      
      if (component_type == "sublocality") address = address + ", " + component.short_name;
      if (component_type == "administrative_area_level_2") address = address + ", " + component.short_name;
    }
  }
  return {state: state, country: country, zip_code: zip_code, city: city, address: address};
}
