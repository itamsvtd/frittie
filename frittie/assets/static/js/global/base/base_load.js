$(function() {
	
	$("a").click(function() {
		if ($(this).attr("href") !== undefined) {
			var href_value = $(this).attr("href");
			var target_value = $(this).attr("target");
			if (href_value.length != 0 && href_value.substring(0,1) != "#" && href_value.indexOf("addcalendar") == -1 
				&& href_value.indexOf("mailto") == -1 && target_value != "_blank") {
				runTopProgressBar();
			}
		}
	})

	$("div").click(function() {
		if ($(this).attr("onclick") !== undefined && $(this).attr("onclick").indexOf("window.location.href") != -1) {
			runTopProgressBar();
		}
	})

  	$('.prevent-default').click(function(event) {event.preventDefault()})

	stretchWindowMinHeight(".map-content-section");

	addDotToLongTextWithElementAttr(".add-dot","data-html-maxlength");
	
	$("#add_photo_form").submit(function() {
		showAjaxLoadingIcon();
	})

	$('.popover-item').popover({
		trigger: "hover focus"
	})

	$(".tooltip-item").tooltip();

});


