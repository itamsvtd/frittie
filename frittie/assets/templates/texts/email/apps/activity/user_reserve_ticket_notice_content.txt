Hello,

Your activity {{activity.name}} has new guests who just reserve their ticket.

To view more details and manage your guest, please visit:
{{url}}

Thanks,
The Frittie Team
