--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: account_emailaddress; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE account_emailaddress (
    id integer NOT NULL,
    user_id integer NOT NULL,
    email character varying(75) NOT NULL,
    verified boolean NOT NULL,
    "primary" boolean NOT NULL
);


ALTER TABLE public.account_emailaddress OWNER TO bobzovgeshjqfj;

--
-- Name: account_emailaddress_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE account_emailaddress_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_emailaddress_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: account_emailaddress_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE account_emailaddress_id_seq OWNED BY account_emailaddress.id;


--
-- Name: account_emailconfirmation; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE account_emailconfirmation (
    id integer NOT NULL,
    email_address_id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    sent timestamp with time zone,
    key character varying(64) NOT NULL
);


ALTER TABLE public.account_emailconfirmation OWNER TO bobzovgeshjqfj;

--
-- Name: account_emailconfirmation_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE account_emailconfirmation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_emailconfirmation_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: account_emailconfirmation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE account_emailconfirmation_id_seq OWNED BY account_emailconfirmation.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO bobzovgeshjqfj;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO bobzovgeshjqfj;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO bobzovgeshjqfj;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone NOT NULL,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(75) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO bobzovgeshjqfj;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO bobzovgeshjqfj;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO bobzovgeshjqfj;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: celery_taskmeta; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE celery_taskmeta (
    id integer NOT NULL,
    task_id character varying(255) NOT NULL,
    status character varying(50) NOT NULL,
    result text,
    date_done timestamp with time zone NOT NULL,
    traceback text,
    hidden boolean NOT NULL,
    meta text
);


ALTER TABLE public.celery_taskmeta OWNER TO bobzovgeshjqfj;

--
-- Name: celery_taskmeta_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE celery_taskmeta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.celery_taskmeta_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: celery_taskmeta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE celery_taskmeta_id_seq OWNED BY celery_taskmeta.id;


--
-- Name: celery_tasksetmeta; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE celery_tasksetmeta (
    id integer NOT NULL,
    taskset_id character varying(255) NOT NULL,
    result text NOT NULL,
    date_done timestamp with time zone NOT NULL,
    hidden boolean NOT NULL
);


ALTER TABLE public.celery_tasksetmeta OWNER TO bobzovgeshjqfj;

--
-- Name: celery_tasksetmeta_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE celery_tasksetmeta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.celery_tasksetmeta_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: celery_tasksetmeta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE celery_tasksetmeta_id_seq OWNED BY celery_tasksetmeta.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    user_id integer NOT NULL,
    content_type_id integer,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO bobzovgeshjqfj;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO bobzovgeshjqfj;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO bobzovgeshjqfj;

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO bobzovgeshjqfj;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE django_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_site_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE django_site_id_seq OWNED BY django_site.id;


--
-- Name: djcelery_crontabschedule; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE djcelery_crontabschedule (
    id integer NOT NULL,
    minute character varying(64) NOT NULL,
    hour character varying(64) NOT NULL,
    day_of_week character varying(64) NOT NULL,
    day_of_month character varying(64) NOT NULL,
    month_of_year character varying(64) NOT NULL
);


ALTER TABLE public.djcelery_crontabschedule OWNER TO bobzovgeshjqfj;

--
-- Name: djcelery_crontabschedule_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE djcelery_crontabschedule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.djcelery_crontabschedule_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: djcelery_crontabschedule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE djcelery_crontabschedule_id_seq OWNED BY djcelery_crontabschedule.id;


--
-- Name: djcelery_intervalschedule; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE djcelery_intervalschedule (
    id integer NOT NULL,
    every integer NOT NULL,
    period character varying(24) NOT NULL
);


ALTER TABLE public.djcelery_intervalschedule OWNER TO bobzovgeshjqfj;

--
-- Name: djcelery_intervalschedule_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE djcelery_intervalschedule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.djcelery_intervalschedule_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: djcelery_intervalschedule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE djcelery_intervalschedule_id_seq OWNED BY djcelery_intervalschedule.id;


--
-- Name: djcelery_periodictask; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE djcelery_periodictask (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    task character varying(200) NOT NULL,
    interval_id integer,
    crontab_id integer,
    args text NOT NULL,
    kwargs text NOT NULL,
    queue character varying(200),
    exchange character varying(200),
    routing_key character varying(200),
    expires timestamp with time zone,
    enabled boolean NOT NULL,
    last_run_at timestamp with time zone,
    total_run_count integer NOT NULL,
    date_changed timestamp with time zone NOT NULL,
    description text NOT NULL,
    CONSTRAINT djcelery_periodictask_total_run_count_check CHECK ((total_run_count >= 0))
);


ALTER TABLE public.djcelery_periodictask OWNER TO bobzovgeshjqfj;

--
-- Name: djcelery_periodictask_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE djcelery_periodictask_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.djcelery_periodictask_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: djcelery_periodictask_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE djcelery_periodictask_id_seq OWNED BY djcelery_periodictask.id;


--
-- Name: djcelery_periodictasks; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE djcelery_periodictasks (
    ident smallint NOT NULL,
    last_update timestamp with time zone NOT NULL
);


ALTER TABLE public.djcelery_periodictasks OWNER TO bobzovgeshjqfj;

--
-- Name: djcelery_taskstate; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE djcelery_taskstate (
    id integer NOT NULL,
    state character varying(64) NOT NULL,
    task_id character varying(36) NOT NULL,
    name character varying(200),
    tstamp timestamp with time zone NOT NULL,
    args text,
    kwargs text,
    eta timestamp with time zone,
    expires timestamp with time zone,
    result text,
    traceback text,
    runtime double precision,
    retries integer NOT NULL,
    worker_id integer,
    hidden boolean NOT NULL
);


ALTER TABLE public.djcelery_taskstate OWNER TO bobzovgeshjqfj;

--
-- Name: djcelery_taskstate_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE djcelery_taskstate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.djcelery_taskstate_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: djcelery_taskstate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE djcelery_taskstate_id_seq OWNED BY djcelery_taskstate.id;


--
-- Name: djcelery_workerstate; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE djcelery_workerstate (
    id integer NOT NULL,
    hostname character varying(255) NOT NULL,
    last_heartbeat timestamp with time zone
);


ALTER TABLE public.djcelery_workerstate OWNER TO bobzovgeshjqfj;

--
-- Name: djcelery_workerstate_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE djcelery_workerstate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.djcelery_workerstate_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: djcelery_workerstate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE djcelery_workerstate_id_seq OWNED BY djcelery_workerstate.id;


--
-- Name: main_activity; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_activity (
    id integer NOT NULL,
    unique_id character varying(100) NOT NULL,
    name character varying(100) NOT NULL,
    description text NOT NULL,
    start_time timestamp with time zone NOT NULL,
    end_time timestamp with time zone,
    logo_id integer,
    user_create_id integer NOT NULL,
    location_id integer NOT NULL,
    "limit" integer NOT NULL,
    activity_type character varying(20) NOT NULL,
    category character varying(3) NOT NULL,
    sell_ticket character varying(10) NOT NULL
);


ALTER TABLE public.main_activity OWNER TO bobzovgeshjqfj;

--
-- Name: main_activity_activity_request; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_activity_activity_request (
    id integer NOT NULL,
    activity_id integer NOT NULL,
    activityrequest_id integer NOT NULL
);


ALTER TABLE public.main_activity_activity_request OWNER TO bobzovgeshjqfj;

--
-- Name: main_activity_activity_request_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_activity_activity_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_activity_activity_request_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_activity_activity_request_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_activity_activity_request_id_seq OWNED BY main_activity_activity_request.id;


--
-- Name: main_activity_comments; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_activity_comments (
    id integer NOT NULL,
    activity_id integer NOT NULL,
    comment_id integer NOT NULL
);


ALTER TABLE public.main_activity_comments OWNER TO bobzovgeshjqfj;

--
-- Name: main_activity_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_activity_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_activity_comments_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_activity_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_activity_comments_id_seq OWNED BY main_activity_comments.id;


--
-- Name: main_activity_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_activity_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_activity_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_activity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_activity_id_seq OWNED BY main_activity.id;


--
-- Name: main_activity_list_photos; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_activity_list_photos (
    id integer NOT NULL,
    activity_id integer NOT NULL,
    photo_id integer NOT NULL
);


ALTER TABLE public.main_activity_list_photos OWNER TO bobzovgeshjqfj;

--
-- Name: main_activity_list_photos_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_activity_list_photos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_activity_list_photos_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_activity_list_photos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_activity_list_photos_id_seq OWNED BY main_activity_list_photos.id;


--
-- Name: main_activity_posts; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_activity_posts (
    id integer NOT NULL,
    activity_id integer NOT NULL,
    notification_id integer NOT NULL
);


ALTER TABLE public.main_activity_posts OWNER TO bobzovgeshjqfj;

--
-- Name: main_activity_posts_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_activity_posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_activity_posts_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_activity_posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_activity_posts_id_seq OWNED BY main_activity_posts.id;


--
-- Name: main_activity_reports; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_activity_reports (
    id integer NOT NULL,
    activity_id integer NOT NULL,
    report_id integer NOT NULL
);


ALTER TABLE public.main_activity_reports OWNER TO bobzovgeshjqfj;

--
-- Name: main_activity_reports_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_activity_reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_activity_reports_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_activity_reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_activity_reports_id_seq OWNED BY main_activity_reports.id;


--
-- Name: main_activity_tags; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_activity_tags (
    id integer NOT NULL,
    activity_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public.main_activity_tags OWNER TO bobzovgeshjqfj;

--
-- Name: main_activity_tags_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_activity_tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_activity_tags_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_activity_tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_activity_tags_id_seq OWNED BY main_activity_tags.id;


--
-- Name: main_activity_ticket; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_activity_ticket (
    id integer NOT NULL,
    activity_id integer NOT NULL,
    ticket_id integer NOT NULL
);


ALTER TABLE public.main_activity_ticket OWNER TO bobzovgeshjqfj;

--
-- Name: main_activity_ticket_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_activity_ticket_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_activity_ticket_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_activity_ticket_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_activity_ticket_id_seq OWNED BY main_activity_ticket.id;


--
-- Name: main_activityrequest; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_activityrequest (
    id integer NOT NULL,
    activity_id integer NOT NULL,
    user_request_id integer NOT NULL,
    date timestamp with time zone NOT NULL,
    request_status character varying(10) NOT NULL,
    greet character varying(100) NOT NULL,
    description character varying(100) NOT NULL
);


ALTER TABLE public.main_activityrequest OWNER TO bobzovgeshjqfj;

--
-- Name: main_activityrequest_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_activityrequest_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_activityrequest_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_activityrequest_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_activityrequest_id_seq OWNED BY main_activityrequest.id;


--
-- Name: main_activityuserrelation; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_activityuserrelation (
    id integer NOT NULL,
    activity_id integer NOT NULL,
    user_id integer NOT NULL,
    status character varying(20) NOT NULL
);


ALTER TABLE public.main_activityuserrelation OWNER TO bobzovgeshjqfj;

--
-- Name: main_activityuserrelation_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_activityuserrelation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_activityuserrelation_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_activityuserrelation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_activityuserrelation_id_seq OWNED BY main_activityuserrelation.id;


--
-- Name: main_comment; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_comment (
    id integer NOT NULL,
    unique_id character varying(100) NOT NULL,
    comment_type character varying(20) NOT NULL,
    user_id integer NOT NULL,
    content text NOT NULL,
    create_date timestamp with time zone NOT NULL,
    edit_date timestamp with time zone
);


ALTER TABLE public.main_comment OWNER TO bobzovgeshjqfj;

--
-- Name: main_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_comment_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_comment_id_seq OWNED BY main_comment.id;


--
-- Name: main_comment_reports; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_comment_reports (
    id integer NOT NULL,
    comment_id integer NOT NULL,
    report_id integer NOT NULL
);


ALTER TABLE public.main_comment_reports OWNER TO bobzovgeshjqfj;

--
-- Name: main_comment_reports_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_comment_reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_comment_reports_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_comment_reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_comment_reports_id_seq OWNED BY main_comment_reports.id;


--
-- Name: main_conversation; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_conversation (
    id integer NOT NULL,
    unique_id character varying(100) NOT NULL,
    user1_id integer NOT NULL,
    user2_id integer NOT NULL,
    latest_message_id integer
);


ALTER TABLE public.main_conversation OWNER TO bobzovgeshjqfj;

--
-- Name: main_conversation_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_conversation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_conversation_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_conversation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_conversation_id_seq OWNED BY main_conversation.id;


--
-- Name: main_conversation_messages; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_conversation_messages (
    id integer NOT NULL,
    conversation_id integer NOT NULL,
    message_id integer NOT NULL
);


ALTER TABLE public.main_conversation_messages OWNER TO bobzovgeshjqfj;

--
-- Name: main_conversation_messages_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_conversation_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_conversation_messages_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_conversation_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_conversation_messages_id_seq OWNED BY main_conversation_messages.id;


--
-- Name: main_feed; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_feed (
    id integer NOT NULL,
    unique_id character varying(100) NOT NULL,
    content text NOT NULL,
    create_by_id integer NOT NULL,
    object_type character varying(30) NOT NULL,
    object_unique_id character varying(100) NOT NULL,
    date timestamp with time zone NOT NULL
);


ALTER TABLE public.main_feed OWNER TO bobzovgeshjqfj;

--
-- Name: main_feed_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_feed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_feed_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_feed_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_feed_id_seq OWNED BY main_feed.id;


--
-- Name: main_feed_user_receive; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_feed_user_receive (
    id integer NOT NULL,
    feed_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.main_feed_user_receive OWNER TO bobzovgeshjqfj;

--
-- Name: main_feed_user_receive_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_feed_user_receive_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_feed_user_receive_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_feed_user_receive_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_feed_user_receive_id_seq OWNED BY main_feed_user_receive.id;


--
-- Name: main_frit; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_frit (
    id integer NOT NULL,
    frit_type character varying(20) NOT NULL,
    user_id integer NOT NULL,
    date timestamp with time zone NOT NULL,
    object_id integer NOT NULL
);


ALTER TABLE public.main_frit OWNER TO bobzovgeshjqfj;

--
-- Name: main_frit_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_frit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_frit_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_frit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_frit_id_seq OWNED BY main_frit.id;


--
-- Name: main_invitation; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_invitation (
    id integer NOT NULL,
    unique_id character varying(100) NOT NULL,
    message text NOT NULL,
    activity_id integer NOT NULL,
    send_from_id integer NOT NULL
);


ALTER TABLE public.main_invitation OWNER TO bobzovgeshjqfj;

--
-- Name: main_invitation_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_invitation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_invitation_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_invitation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_invitation_id_seq OWNED BY main_invitation.id;


--
-- Name: main_invitation_list_photos; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_invitation_list_photos (
    id integer NOT NULL,
    invitation_id integer NOT NULL,
    photo_id integer NOT NULL
);


ALTER TABLE public.main_invitation_list_photos OWNER TO bobzovgeshjqfj;

--
-- Name: main_invitation_list_photos_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_invitation_list_photos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_invitation_list_photos_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_invitation_list_photos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_invitation_list_photos_id_seq OWNED BY main_invitation_list_photos.id;


--
-- Name: main_invitation_list_videos; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_invitation_list_videos (
    id integer NOT NULL,
    invitation_id integer NOT NULL,
    video_id integer NOT NULL
);


ALTER TABLE public.main_invitation_list_videos OWNER TO bobzovgeshjqfj;

--
-- Name: main_invitation_list_videos_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_invitation_list_videos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_invitation_list_videos_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_invitation_list_videos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_invitation_list_videos_id_seq OWNED BY main_invitation_list_videos.id;


--
-- Name: main_invitation_send_to; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_invitation_send_to (
    id integer NOT NULL,
    invitation_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.main_invitation_send_to OWNER TO bobzovgeshjqfj;

--
-- Name: main_invitation_send_to_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_invitation_send_to_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_invitation_send_to_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_invitation_send_to_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_invitation_send_to_id_seq OWNED BY main_invitation_send_to.id;


--
-- Name: main_location; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_location (
    id integer NOT NULL,
    unique_id character varying(100) NOT NULL,
    name character varying(50) NOT NULL,
    description text NOT NULL,
    category character varying(2) NOT NULL,
    address1 character varying(100) NOT NULL,
    address2 character varying(100) NOT NULL,
    lat numeric(19,10) NOT NULL,
    lng numeric(19,10) NOT NULL,
    city character varying(50) NOT NULL,
    state character varying(2) NOT NULL,
    zip_code integer NOT NULL,
    country character varying(50) NOT NULL,
    website_link character varying(200) NOT NULL,
    main_picture_id integer,
    create_by_id integer NOT NULL,
    preference text NOT NULL,
    rating double precision NOT NULL
);


ALTER TABLE public.main_location OWNER TO bobzovgeshjqfj;

--
-- Name: main_location_comments; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_location_comments (
    id integer NOT NULL,
    location_id integer NOT NULL,
    comment_id integer NOT NULL
);


ALTER TABLE public.main_location_comments OWNER TO bobzovgeshjqfj;

--
-- Name: main_location_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_location_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_location_comments_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_location_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_location_comments_id_seq OWNED BY main_location_comments.id;


--
-- Name: main_location_follow_by; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_location_follow_by (
    id integer NOT NULL,
    location_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.main_location_follow_by OWNER TO bobzovgeshjqfj;

--
-- Name: main_location_follow_by_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_location_follow_by_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_location_follow_by_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_location_follow_by_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_location_follow_by_id_seq OWNED BY main_location_follow_by.id;


--
-- Name: main_location_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_location_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_location_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_location_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_location_id_seq OWNED BY main_location.id;


--
-- Name: main_location_list_photos; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_location_list_photos (
    id integer NOT NULL,
    location_id integer NOT NULL,
    photo_id integer NOT NULL
);


ALTER TABLE public.main_location_list_photos OWNER TO bobzovgeshjqfj;

--
-- Name: main_location_list_photos_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_location_list_photos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_location_list_photos_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_location_list_photos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_location_list_photos_id_seq OWNED BY main_location_list_photos.id;


--
-- Name: main_location_posts; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_location_posts (
    id integer NOT NULL,
    location_id integer NOT NULL,
    notification_id integer NOT NULL
);


ALTER TABLE public.main_location_posts OWNER TO bobzovgeshjqfj;

--
-- Name: main_location_posts_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_location_posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_location_posts_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_location_posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_location_posts_id_seq OWNED BY main_location_posts.id;


--
-- Name: main_location_reports; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_location_reports (
    id integer NOT NULL,
    location_id integer NOT NULL,
    report_id integer NOT NULL
);


ALTER TABLE public.main_location_reports OWNER TO bobzovgeshjqfj;

--
-- Name: main_location_reports_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_location_reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_location_reports_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_location_reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_location_reports_id_seq OWNED BY main_location_reports.id;


--
-- Name: main_message; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_message (
    id integer NOT NULL,
    unique_id character varying(100) NOT NULL,
    user_send_id integer NOT NULL,
    user_receive_id integer NOT NULL,
    content text NOT NULL,
    status character varying(1) NOT NULL,
    date timestamp with time zone NOT NULL
);


ALTER TABLE public.main_message OWNER TO bobzovgeshjqfj;

--
-- Name: main_message_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_message_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_message_id_seq OWNED BY main_message.id;


--
-- Name: main_notification; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_notification (
    id integer NOT NULL,
    unique_id character varying(100) NOT NULL,
    content text NOT NULL,
    status character varying(1) NOT NULL,
    notification_type character varying(30),
    notify_from_id integer NOT NULL,
    date timestamp with time zone NOT NULL
);


ALTER TABLE public.main_notification OWNER TO bobzovgeshjqfj;

--
-- Name: main_notification_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_notification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_notification_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_notification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_notification_id_seq OWNED BY main_notification.id;


--
-- Name: main_notification_notify_to; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_notification_notify_to (
    id integer NOT NULL,
    notification_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.main_notification_notify_to OWNER TO bobzovgeshjqfj;

--
-- Name: main_notification_notify_to_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_notification_notify_to_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_notification_notify_to_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_notification_notify_to_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_notification_notify_to_id_seq OWNED BY main_notification_notify_to.id;


--
-- Name: main_photo; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_photo (
    id integer NOT NULL,
    unique_id character varying(100) NOT NULL,
    filename character varying(60) NOT NULL,
    image character varying(100) NOT NULL,
    upload_date timestamp with time zone NOT NULL,
    user_post_id integer NOT NULL,
    photo_type character varying(3) NOT NULL
);


ALTER TABLE public.main_photo OWNER TO bobzovgeshjqfj;

--
-- Name: main_photo_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_photo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_photo_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_photo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_photo_id_seq OWNED BY main_photo.id;


--
-- Name: main_report; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_report (
    id integer NOT NULL,
    unique_id character varying(100) NOT NULL,
    report_type character varying(20) NOT NULL,
    user_report_id integer NOT NULL,
    date timestamp with time zone NOT NULL,
    report_content text NOT NULL
);


ALTER TABLE public.main_report OWNER TO bobzovgeshjqfj;

--
-- Name: main_report_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_report_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_report_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_report_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_report_id_seq OWNED BY main_report.id;


--
-- Name: main_siteglobaldata; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_siteglobaldata (
    id integer NOT NULL,
    users_with_fb_id text NOT NULL,
    site character varying(100) NOT NULL
);


ALTER TABLE public.main_siteglobaldata OWNER TO bobzovgeshjqfj;

--
-- Name: main_siteglobaldata_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_siteglobaldata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_siteglobaldata_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_siteglobaldata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_siteglobaldata_id_seq OWNED BY main_siteglobaldata.id;


--
-- Name: main_tag; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_tag (
    id integer NOT NULL,
    user_tag_id integer,
    content character varying(20) NOT NULL
);


ALTER TABLE public.main_tag OWNER TO bobzovgeshjqfj;

--
-- Name: main_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_tag_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_tag_id_seq OWNED BY main_tag.id;


--
-- Name: main_ticket; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_ticket (
    id integer NOT NULL,
    price numeric(10,2) NOT NULL,
    fee numeric(10,2) NOT NULL,
    ticket_status character varying(1) NOT NULL,
    ticket_type character varying(3) NOT NULL
);


ALTER TABLE public.main_ticket OWNER TO bobzovgeshjqfj;

--
-- Name: main_ticket_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_ticket_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_ticket_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_ticket_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_ticket_id_seq OWNED BY main_ticket.id;


--
-- Name: main_tickettransaction; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_tickettransaction (
    id integer NOT NULL,
    user_id integer NOT NULL,
    ticket_id integer NOT NULL,
    quantity integer NOT NULL,
    transaction_date timestamp with time zone NOT NULL
);


ALTER TABLE public.main_tickettransaction OWNER TO bobzovgeshjqfj;

--
-- Name: main_tickettransaction_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_tickettransaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_tickettransaction_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_tickettransaction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_tickettransaction_id_seq OWNED BY main_tickettransaction.id;


--
-- Name: main_userprofile; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_userprofile (
    id integer NOT NULL,
    user_id integer NOT NULL,
    basic_info text NOT NULL,
    gender character varying(1) NOT NULL,
    avatar_id integer,
    city character varying(50) NOT NULL,
    state character varying(2) NOT NULL,
    country character varying(100) NOT NULL,
    privacy_status character varying(1) NOT NULL,
    date_of_birth date,
    facebook_id character varying(40)
);


ALTER TABLE public.main_userprofile OWNER TO bobzovgeshjqfj;

--
-- Name: main_userprofile_followers; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_userprofile_followers (
    id integer NOT NULL,
    userprofile_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.main_userprofile_followers OWNER TO bobzovgeshjqfj;

--
-- Name: main_userprofile_followers_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_userprofile_followers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_userprofile_followers_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_userprofile_followers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_userprofile_followers_id_seq OWNED BY main_userprofile_followers.id;


--
-- Name: main_userprofile_following; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_userprofile_following (
    id integer NOT NULL,
    userprofile_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.main_userprofile_following OWNER TO bobzovgeshjqfj;

--
-- Name: main_userprofile_following_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_userprofile_following_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_userprofile_following_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_userprofile_following_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_userprofile_following_id_seq OWNED BY main_userprofile_following.id;


--
-- Name: main_userprofile_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_userprofile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_userprofile_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_userprofile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_userprofile_id_seq OWNED BY main_userprofile.id;


--
-- Name: main_userprofile_invitations; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_userprofile_invitations (
    id integer NOT NULL,
    userprofile_id integer NOT NULL,
    invitation_id integer NOT NULL
);


ALTER TABLE public.main_userprofile_invitations OWNER TO bobzovgeshjqfj;

--
-- Name: main_userprofile_invitations_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_userprofile_invitations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_userprofile_invitations_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_userprofile_invitations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_userprofile_invitations_id_seq OWNED BY main_userprofile_invitations.id;


--
-- Name: main_userprofile_watchlist; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_userprofile_watchlist (
    id integer NOT NULL,
    userprofile_id integer NOT NULL,
    activity_id integer NOT NULL
);


ALTER TABLE public.main_userprofile_watchlist OWNER TO bobzovgeshjqfj;

--
-- Name: main_userprofile_watchlist_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_userprofile_watchlist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_userprofile_watchlist_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_userprofile_watchlist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_userprofile_watchlist_id_seq OWNED BY main_userprofile_watchlist.id;


--
-- Name: main_video; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE main_video (
    id integer NOT NULL,
    unique_id character varying(100) NOT NULL,
    filename character varying(60),
    video character varying(100) NOT NULL,
    key_data character varying(90),
    upload_date timestamp with time zone NOT NULL,
    user_post_id integer NOT NULL,
    video_type character varying(20) NOT NULL
);


ALTER TABLE public.main_video OWNER TO bobzovgeshjqfj;

--
-- Name: main_video_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE main_video_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_video_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: main_video_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE main_video_id_seq OWNED BY main_video.id;


--
-- Name: socialaccount_socialaccount; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE socialaccount_socialaccount (
    id integer NOT NULL,
    user_id integer NOT NULL,
    last_login timestamp with time zone NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    provider character varying(30) NOT NULL,
    uid character varying(255) NOT NULL,
    extra_data text NOT NULL
);


ALTER TABLE public.socialaccount_socialaccount OWNER TO bobzovgeshjqfj;

--
-- Name: socialaccount_socialaccount_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE socialaccount_socialaccount_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialaccount_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: socialaccount_socialaccount_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE socialaccount_socialaccount_id_seq OWNED BY socialaccount_socialaccount.id;


--
-- Name: socialaccount_socialapp; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE socialaccount_socialapp (
    id integer NOT NULL,
    provider character varying(30) NOT NULL,
    name character varying(40) NOT NULL,
    key character varying(100) NOT NULL,
    secret character varying(100) NOT NULL,
    client_id character varying(100) NOT NULL
);


ALTER TABLE public.socialaccount_socialapp OWNER TO bobzovgeshjqfj;

--
-- Name: socialaccount_socialapp_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE socialaccount_socialapp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialapp_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: socialaccount_socialapp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE socialaccount_socialapp_id_seq OWNED BY socialaccount_socialapp.id;


--
-- Name: socialaccount_socialapp_sites; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE socialaccount_socialapp_sites (
    id integer NOT NULL,
    socialapp_id integer NOT NULL,
    site_id integer NOT NULL
);


ALTER TABLE public.socialaccount_socialapp_sites OWNER TO bobzovgeshjqfj;

--
-- Name: socialaccount_socialapp_sites_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE socialaccount_socialapp_sites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialapp_sites_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: socialaccount_socialapp_sites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE socialaccount_socialapp_sites_id_seq OWNED BY socialaccount_socialapp_sites.id;


--
-- Name: socialaccount_socialtoken; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE socialaccount_socialtoken (
    id integer NOT NULL,
    app_id integer NOT NULL,
    account_id integer NOT NULL,
    token text NOT NULL,
    token_secret character varying(200) NOT NULL,
    expires_at timestamp with time zone
);


ALTER TABLE public.socialaccount_socialtoken OWNER TO bobzovgeshjqfj;

--
-- Name: socialaccount_socialtoken_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE socialaccount_socialtoken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialtoken_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: socialaccount_socialtoken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE socialaccount_socialtoken_id_seq OWNED BY socialaccount_socialtoken.id;


--
-- Name: south_migrationhistory; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE south_migrationhistory (
    id integer NOT NULL,
    app_name character varying(255) NOT NULL,
    migration character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.south_migrationhistory OWNER TO bobzovgeshjqfj;

--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE south_migrationhistory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.south_migrationhistory_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE south_migrationhistory_id_seq OWNED BY south_migrationhistory.id;


--
-- Name: tastypie_apiaccess; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE tastypie_apiaccess (
    id integer NOT NULL,
    identifier character varying(255) NOT NULL,
    url character varying(255) NOT NULL,
    request_method character varying(10) NOT NULL,
    accessed integer NOT NULL,
    CONSTRAINT tastypie_apiaccess_accessed_check CHECK ((accessed >= 0))
);


ALTER TABLE public.tastypie_apiaccess OWNER TO bobzovgeshjqfj;

--
-- Name: tastypie_apiaccess_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE tastypie_apiaccess_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tastypie_apiaccess_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: tastypie_apiaccess_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE tastypie_apiaccess_id_seq OWNED BY tastypie_apiaccess.id;


--
-- Name: tastypie_apikey; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE tastypie_apikey (
    id integer NOT NULL,
    user_id integer NOT NULL,
    key character varying(256) NOT NULL,
    created timestamp with time zone NOT NULL
);


ALTER TABLE public.tastypie_apikey OWNER TO bobzovgeshjqfj;

--
-- Name: tastypie_apikey_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE tastypie_apikey_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tastypie_apikey_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: tastypie_apikey_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE tastypie_apikey_id_seq OWNED BY tastypie_apikey.id;


--
-- Name: user_streams_single_table_backend_streamitem; Type: TABLE; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE TABLE user_streams_single_table_backend_streamitem (
    id integer NOT NULL,
    user_id integer NOT NULL,
    content text NOT NULL,
    created_at timestamp with time zone NOT NULL
);


ALTER TABLE public.user_streams_single_table_backend_streamitem OWNER TO bobzovgeshjqfj;

--
-- Name: user_streams_single_table_backend_streamitem_id_seq; Type: SEQUENCE; Schema: public; Owner: bobzovgeshjqfj
--

CREATE SEQUENCE user_streams_single_table_backend_streamitem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_streams_single_table_backend_streamitem_id_seq OWNER TO bobzovgeshjqfj;

--
-- Name: user_streams_single_table_backend_streamitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bobzovgeshjqfj
--

ALTER SEQUENCE user_streams_single_table_backend_streamitem_id_seq OWNED BY user_streams_single_table_backend_streamitem.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY account_emailaddress ALTER COLUMN id SET DEFAULT nextval('account_emailaddress_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY account_emailconfirmation ALTER COLUMN id SET DEFAULT nextval('account_emailconfirmation_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY celery_taskmeta ALTER COLUMN id SET DEFAULT nextval('celery_taskmeta_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY celery_tasksetmeta ALTER COLUMN id SET DEFAULT nextval('celery_tasksetmeta_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY django_site ALTER COLUMN id SET DEFAULT nextval('django_site_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY djcelery_crontabschedule ALTER COLUMN id SET DEFAULT nextval('djcelery_crontabschedule_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY djcelery_intervalschedule ALTER COLUMN id SET DEFAULT nextval('djcelery_intervalschedule_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY djcelery_periodictask ALTER COLUMN id SET DEFAULT nextval('djcelery_periodictask_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY djcelery_taskstate ALTER COLUMN id SET DEFAULT nextval('djcelery_taskstate_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY djcelery_workerstate ALTER COLUMN id SET DEFAULT nextval('djcelery_workerstate_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity ALTER COLUMN id SET DEFAULT nextval('main_activity_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_activity_request ALTER COLUMN id SET DEFAULT nextval('main_activity_activity_request_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_comments ALTER COLUMN id SET DEFAULT nextval('main_activity_comments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_list_photos ALTER COLUMN id SET DEFAULT nextval('main_activity_list_photos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_posts ALTER COLUMN id SET DEFAULT nextval('main_activity_posts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_reports ALTER COLUMN id SET DEFAULT nextval('main_activity_reports_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_tags ALTER COLUMN id SET DEFAULT nextval('main_activity_tags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_ticket ALTER COLUMN id SET DEFAULT nextval('main_activity_ticket_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activityrequest ALTER COLUMN id SET DEFAULT nextval('main_activityrequest_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activityuserrelation ALTER COLUMN id SET DEFAULT nextval('main_activityuserrelation_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_comment ALTER COLUMN id SET DEFAULT nextval('main_comment_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_comment_reports ALTER COLUMN id SET DEFAULT nextval('main_comment_reports_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_conversation ALTER COLUMN id SET DEFAULT nextval('main_conversation_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_conversation_messages ALTER COLUMN id SET DEFAULT nextval('main_conversation_messages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_feed ALTER COLUMN id SET DEFAULT nextval('main_feed_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_feed_user_receive ALTER COLUMN id SET DEFAULT nextval('main_feed_user_receive_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_frit ALTER COLUMN id SET DEFAULT nextval('main_frit_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_invitation ALTER COLUMN id SET DEFAULT nextval('main_invitation_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_invitation_list_photos ALTER COLUMN id SET DEFAULT nextval('main_invitation_list_photos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_invitation_list_videos ALTER COLUMN id SET DEFAULT nextval('main_invitation_list_videos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_invitation_send_to ALTER COLUMN id SET DEFAULT nextval('main_invitation_send_to_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_location ALTER COLUMN id SET DEFAULT nextval('main_location_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_location_comments ALTER COLUMN id SET DEFAULT nextval('main_location_comments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_location_follow_by ALTER COLUMN id SET DEFAULT nextval('main_location_follow_by_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_location_list_photos ALTER COLUMN id SET DEFAULT nextval('main_location_list_photos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_location_posts ALTER COLUMN id SET DEFAULT nextval('main_location_posts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_location_reports ALTER COLUMN id SET DEFAULT nextval('main_location_reports_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_message ALTER COLUMN id SET DEFAULT nextval('main_message_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_notification ALTER COLUMN id SET DEFAULT nextval('main_notification_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_notification_notify_to ALTER COLUMN id SET DEFAULT nextval('main_notification_notify_to_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_photo ALTER COLUMN id SET DEFAULT nextval('main_photo_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_report ALTER COLUMN id SET DEFAULT nextval('main_report_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_siteglobaldata ALTER COLUMN id SET DEFAULT nextval('main_siteglobaldata_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_tag ALTER COLUMN id SET DEFAULT nextval('main_tag_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_ticket ALTER COLUMN id SET DEFAULT nextval('main_ticket_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_tickettransaction ALTER COLUMN id SET DEFAULT nextval('main_tickettransaction_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_userprofile ALTER COLUMN id SET DEFAULT nextval('main_userprofile_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_userprofile_followers ALTER COLUMN id SET DEFAULT nextval('main_userprofile_followers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_userprofile_following ALTER COLUMN id SET DEFAULT nextval('main_userprofile_following_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_userprofile_invitations ALTER COLUMN id SET DEFAULT nextval('main_userprofile_invitations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_userprofile_watchlist ALTER COLUMN id SET DEFAULT nextval('main_userprofile_watchlist_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_video ALTER COLUMN id SET DEFAULT nextval('main_video_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY socialaccount_socialaccount ALTER COLUMN id SET DEFAULT nextval('socialaccount_socialaccount_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY socialaccount_socialapp ALTER COLUMN id SET DEFAULT nextval('socialaccount_socialapp_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY socialaccount_socialapp_sites ALTER COLUMN id SET DEFAULT nextval('socialaccount_socialapp_sites_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY socialaccount_socialtoken ALTER COLUMN id SET DEFAULT nextval('socialaccount_socialtoken_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY south_migrationhistory ALTER COLUMN id SET DEFAULT nextval('south_migrationhistory_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY tastypie_apiaccess ALTER COLUMN id SET DEFAULT nextval('tastypie_apiaccess_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY tastypie_apikey ALTER COLUMN id SET DEFAULT nextval('tastypie_apikey_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY user_streams_single_table_backend_streamitem ALTER COLUMN id SET DEFAULT nextval('user_streams_single_table_backend_streamitem_id_seq'::regclass);


--
-- Data for Name: account_emailaddress; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY account_emailaddress (id, user_id, email, verified, "primary") FROM stdin;
1	2	vuthanhduc92@yahoo.com	f	t
3	3	dangnguyen_1712@yahoo.com	f	f
4	4	vthanhduc92@gmail.com	f	t
5	5	batman_begin_a10@yahoo.com	f	t
6	6	coldkiller131@gmail.com	f	t
7	7	tunguyen243@gmail.com	f	t
8	8	tdz0110@gmail.com	f	t
9	9	hachicha.taya@yahoo.fr	f	t
10	10	vuongthanhhue@gmail.com	f	t
11	11	etuleu@gmail.com	f	t
12	12	kinhquoc89@gmail.com	f	t
2	3	dtn1712@gmail.com	t	t
\.


--
-- Name: account_emailaddress_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('account_emailaddress_id_seq', 12, true);


--
-- Data for Name: account_emailconfirmation; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY account_emailconfirmation (id, email_address_id, created, sent, key) FROM stdin;
1	1	2014-02-13 22:51:29.760267+00	2014-02-13 22:51:31.218462+00	01be3314065f5bc009a89490c4638d159929613ec60689bb4147a209b7f16d8b
2	2	2014-02-13 23:17:40.486195+00	2014-02-13 23:17:41.518044+00	d964223645e3c05238c4137b8a9d87de961dfa20e9319456be0e832ce57bee6c
3	4	2014-02-13 23:49:29.224859+00	2014-02-13 23:49:30.385173+00	abff518614b5b96bcbf2314b913fda2e9a756d9c75b8557cb65b901d9e494e54
4	5	2014-02-14 04:19:53.336507+00	2014-02-14 04:19:56.02644+00	9b4e3b91ed438ffd2df9f962f6e42b2d6220a7005ecf73fa334ea82a1329ae61
5	6	2014-02-15 14:21:34.007505+00	2014-02-15 14:21:36.649059+00	19dfaefd772b0db7ac8c543e7416a0387323812815f7f6468af4bc5a201de686
6	7	2014-02-15 17:18:09.804257+00	2014-02-15 17:18:12.32611+00	4e8a7e478fee1929250e8d915ae65753917f830b5e3662fe9f3fc2a53383376e
7	8	2014-02-15 17:18:23.458329+00	2014-02-15 17:18:25.458266+00	087474b7441b9f6cd7567ecc981fbe294af5b4a8c0c8cb6f3c35733760cc6cfa
8	1	2014-02-16 00:09:13.885705+00	2014-02-16 00:09:15.843751+00	822d964b52a31ec15b8713be8a1cea536f0ed5ce20cda09d4cd57a6f164d0ec1
9	9	2014-02-17 21:52:23.112504+00	2014-02-17 21:52:24.973801+00	efae0a4292c876f08f501b643bd68835106cfb59496e743da18789a09db13ae1
10	10	2014-02-18 01:25:52.53898+00	2014-02-18 01:25:55.150152+00	57434d6d9198704d06d0b6bb7ad5d30eba961e0881cc949049a24e6a5cccc165
11	11	2014-02-18 22:08:18.207639+00	2014-02-18 22:08:20.311098+00	98822cd2a4362fa34990560f47c0f8d745287970e071f64dd4c5d1c60c98be39
12	12	2014-02-22 17:46:03.908265+00	2014-02-22 17:46:06.4784+00	9ad4231a6bad334735aded733e577eeebbffd1030c54206669f298bb17e7210d
13	2	2014-02-24 22:11:20.46676+00	2014-02-24 22:11:22.421138+00	d3237c8140aef444c80e99d605e0edf9208fe3b54d148fba81ea34514071873f
\.


--
-- Name: account_emailconfirmation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('account_emailconfirmation_id_seq', 13, true);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add permission	1	add_permission
2	Can change permission	1	change_permission
3	Can delete permission	1	delete_permission
4	Can add group	2	add_group
5	Can change group	2	change_group
6	Can delete group	2	delete_group
7	Can add user	3	add_user
8	Can change user	3	change_user
9	Can delete user	3	delete_user
10	Can add site global data	4	add_siteglobaldata
11	Can change site global data	4	change_siteglobaldata
12	Can delete site global data	4	delete_siteglobaldata
13	Can add frit	5	add_frit
14	Can change frit	5	change_frit
15	Can delete frit	5	delete_frit
16	Can add photo	6	add_photo
17	Can change photo	6	change_photo
18	Can delete photo	6	delete_photo
19	Can add video	7	add_video
20	Can change video	7	change_video
21	Can delete video	7	delete_video
22	Can add ticket	8	add_ticket
23	Can change ticket	8	change_ticket
24	Can delete ticket	8	delete_ticket
25	Can add ticket transaction	9	add_tickettransaction
26	Can change ticket transaction	9	change_tickettransaction
27	Can delete ticket transaction	9	delete_tickettransaction
28	Can add message	10	add_message
29	Can change message	10	change_message
30	Can delete message	10	delete_message
31	Can add conversation	11	add_conversation
32	Can change conversation	11	change_conversation
33	Can delete conversation	11	delete_conversation
34	Can add comment	12	add_comment
35	Can change comment	12	change_comment
36	Can delete comment	12	delete_comment
37	Can add report	13	add_report
38	Can change report	13	change_report
39	Can delete report	13	delete_report
40	Can add feed	14	add_feed
41	Can change feed	14	change_feed
42	Can delete feed	14	delete_feed
43	Can add notification	15	add_notification
44	Can change notification	15	change_notification
45	Can delete notification	15	delete_notification
46	Can add invitation	16	add_invitation
47	Can change invitation	16	change_invitation
48	Can delete invitation	16	delete_invitation
49	Can add activity request	17	add_activityrequest
50	Can change activity request	17	change_activityrequest
51	Can delete activity request	17	delete_activityrequest
52	Can add activity user relation	18	add_activityuserrelation
53	Can change activity user relation	18	change_activityuserrelation
54	Can delete activity user relation	18	delete_activityuserrelation
55	Can add tag	19	add_tag
56	Can change tag	19	change_tag
57	Can delete tag	19	delete_tag
58	Can add activity	20	add_activity
59	Can change activity	20	change_activity
60	Can delete activity	20	delete_activity
61	Can add location	21	add_location
62	Can change location	21	change_location
63	Can delete location	21	delete_location
64	Can add user profile	22	add_userprofile
65	Can change user profile	22	change_userprofile
66	Can delete user profile	22	delete_userprofile
67	Can add social app	23	add_socialapp
68	Can change social app	23	change_socialapp
69	Can delete social app	23	delete_socialapp
70	Can add social account	24	add_socialaccount
71	Can change social account	24	change_socialaccount
72	Can delete social account	24	delete_socialaccount
73	Can add social token	25	add_socialtoken
74	Can change social token	25	change_socialtoken
75	Can delete social token	25	delete_socialtoken
76	Can add api access	26	add_apiaccess
77	Can change api access	26	change_apiaccess
78	Can delete api access	26	delete_apiaccess
79	Can add api key	27	add_apikey
80	Can change api key	27	change_apikey
81	Can delete api key	27	delete_apikey
82	Can add task state	28	add_taskmeta
83	Can change task state	28	change_taskmeta
84	Can delete task state	28	delete_taskmeta
85	Can add saved group result	29	add_tasksetmeta
86	Can change saved group result	29	change_tasksetmeta
87	Can delete saved group result	29	delete_tasksetmeta
88	Can add interval	30	add_intervalschedule
89	Can change interval	30	change_intervalschedule
90	Can delete interval	30	delete_intervalschedule
91	Can add crontab	31	add_crontabschedule
92	Can change crontab	31	change_crontabschedule
93	Can delete crontab	31	delete_crontabschedule
94	Can add periodic tasks	32	add_periodictasks
95	Can change periodic tasks	32	change_periodictasks
96	Can delete periodic tasks	32	delete_periodictasks
97	Can add periodic task	33	add_periodictask
98	Can change periodic task	33	change_periodictask
99	Can delete periodic task	33	delete_periodictask
100	Can add worker	34	add_workerstate
101	Can change worker	34	change_workerstate
102	Can delete worker	34	delete_workerstate
103	Can add task	35	add_taskstate
104	Can change task	35	change_taskstate
105	Can delete task	35	delete_taskstate
106	Can add confirmation activity	36	add_confirmationactivity
107	Can change confirmation activity	36	change_confirmationactivity
108	Can delete confirmation activity	36	delete_confirmationactivity
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('auth_permission_id_seq', 108, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
12	!	2014-02-22 17:46:06.520593+00	f	baobao32	Quoc	Le	kinhquoc89@gmail.com	f	t	2014-02-22 17:46:02.681761+00
4	pbkdf2_sha256$10000$A1eM8LnPs8Nc$plAWkDE68uKWQfjvN4TMmxG5Sg86DEIaK6WNQTLBjxQ=	2014-02-22 23:06:53.886201+00	f	duc	Duc	Vu	vthanhduc92@gmail.com	f	t	2014-02-13 23:49:28.828639+00
7	!	2014-02-23 09:50:40.113224+00	f	de606b	Tu	Nguyen	tunguyen243@gmail.com	f	t	2014-02-15 17:18:08.869269+00
3	!	2014-02-24 11:33:24.804002+00	f	Danny.Nguyen1712	Dang	Nguyen	dtn1712@gmail.com	f	t	2014-02-13 23:17:33.566+00
2	!	2014-02-24 19:17:37.0157+00	t	vuthanhduc	Duc	Vu	vuthanhduc92@yahoo.com	t	t	2014-02-13 22:51:28+00
1	pbkdf2_sha256$10000$IXgjUfkLEfBf$gwwjVr8LUfskzI3/MZPlaX+F3fF/TfMDmcUrBiH4trM=	2014-02-24 02:59:16.785732+00	t	dtn29	Admin	Dang Nguyen	dangnguyen_1712@yahoo.com	t	t	2014-02-13 22:45:18.359347+00
6	!	2014-02-15 14:21:36.69743+00	f	knightus	Dương	Lê Khánh	coldkiller131@gmail.com	f	t	2014-02-15 14:21:33.005338+00
8	!	2014-02-15 17:18:25.498425+00	f	tdz0110	Thanh Tùng	Vũ	tdz0110@gmail.com	f	t	2014-02-15 17:18:22.480116+00
9	!	2014-02-17 21:52:25.091511+00	f	adabou	Mohamed Taya	Hachicha	hachicha.taya@yahoo.fr	f	t	2014-02-17 21:52:21.671082+00
10	!	2014-02-18 01:25:55.199382+00	f	huetvuong	Hue	Vuong	vuongthanhhue@gmail.com	f	t	2014-02-18 01:25:51.295236+00
11	!	2014-02-18 22:08:20.366036+00	f	etuleu	Erdal	Tuleu	etuleu@gmail.com	f	t	2014-02-18 22:08:17.128278+00
5	pbkdf2_sha256$10000$ZGJDHi0vSEVr$mTJDZPE/mLsYYyUZoPislryUlxuLfs7aB8HWxFECfXo=	2014-02-22 05:05:59.745809+00	f	lekinh.hieu	Le Kinh	Hieu	batman_begin_a10@yahoo.com	f	t	2014-02-14 04:19:50.974446+00
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('auth_user_id_seq', 12, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: celery_taskmeta; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY celery_taskmeta (id, task_id, status, result, date_done, traceback, hidden, meta) FROM stdin;
\.


--
-- Name: celery_taskmeta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('celery_taskmeta_id_seq', 1, false);


--
-- Data for Name: celery_tasksetmeta; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY celery_tasksetmeta (id, taskset_id, result, date_done, hidden) FROM stdin;
\.


--
-- Name: celery_tasksetmeta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('celery_tasksetmeta_id_seq', 1, false);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY django_admin_log (id, action_time, user_id, content_type_id, object_id, object_repr, action_flag, change_message) FROM stdin;
1	2014-02-13 22:54:28.875787+00	1	3	2	vuthanhduc	2	Changed password, is_staff and is_superuser.
2	2014-02-13 22:56:05.085163+00	2	6	51	uploads/img/share/photo/2014/02/13/childrenexhibition.jpg	1	
3	2014-02-13 22:56:26.36082+00	2	20	1	moon festival	1	
4	2014-02-13 22:58:53.111903+00	2	20	1	moon festival	2	Changed unique_id.
5	2014-02-13 23:00:29.534296+00	1	20	1	moon festival	2	Changed unique_id.
6	2014-02-13 23:01:00.362031+00	1	20	1	moon festival	2	Changed start_time and end_time.
7	2014-02-17 02:45:48.817166+00	2	20	1	moon festival	2	Changed start_time and end_time.
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 7, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY django_content_type (id, name, app_label, model) FROM stdin;
1	permission	auth	permission
2	group	auth	group
3	user	auth	user
4	site global data	main	siteglobaldata
5	frit	main	frit
6	photo	main	photo
7	video	main	video
8	ticket	main	ticket
9	ticket transaction	main	tickettransaction
10	message	main	message
11	conversation	main	conversation
12	comment	main	comment
13	report	main	report
14	feed	main	feed
15	notification	main	notification
16	invitation	main	invitation
17	activity request	main	activityrequest
18	activity user relation	main	activityuserrelation
19	tag	main	tag
20	activity	main	activity
21	location	main	location
22	user profile	main	userprofile
23	social app	socialaccount	socialapp
24	social account	socialaccount	socialaccount
25	social token	socialaccount	socialtoken
26	api access	tastypie	apiaccess
27	api key	tastypie	apikey
28	task state	djcelery	taskmeta
29	saved group result	djcelery	tasksetmeta
30	interval	djcelery	intervalschedule
31	crontab	djcelery	crontabschedule
32	periodic tasks	djcelery	periodictasks
33	periodic task	djcelery	periodictask
34	worker	djcelery	workerstate
35	task	djcelery	taskstate
36	confirmation activity	main	confirmationactivity
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('django_content_type_id_seq', 36, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
elryjov75t2r9kaz6mhh40kl7l90efva	NDU4MTlhOTgxYmNiMWQ0MTVhYTNiZjU3OGUyYjUyYWZjZGNiM2JlMzqAAn1xAShVDV9hdXRoX3VzZXJfaWRxAksFWA8AAABfc2Vzc2lvbl9leHBpcnlxA0sAVRJfYXV0aF91c2VyX2JhY2tlbmRxBFUzYWxsYXV0aC5hY2NvdW50LmF1dGhfYmFja2VuZHMuQXV0aGVudGljYXRpb25CYWNrZW5kcQV1Lg==	2014-03-03 01:57:32.842418+00
7kyd0sr86tah6eychwysdx2g7rbo61ez	Njc0Njc5MDUwYzE0MjM5ZjIwYmQ3Yzk1NmMyYjQzMjkwODgxZTQ3NDqAAn1xAVgKAAAAdGVzdGNvb2tpZXECWAYAAAB3b3JrZWRxA3Mu	2014-03-03 02:35:04.082471+00
aizgbr13nr2keux519lbjga7ugx75afm	NWJkYzQ5M2YxNzU5ODQ3NzUwYjIwNGI2M2ZiYjQ4MjQyMWZiOGNhZDqAAn1xAShVDV9hdXRoX3VzZXJfaWRLBFUMYWNjb3VudF91c2VySwRVFmFjY291bnRfdmVyaWZpZWRfZW1haWxOVRJfYXV0aF91c2VyX2JhY2tlbmRVM2FsbGF1dGguYWNjb3VudC5hdXRoX2JhY2tlbmRzLkF1dGhlbnRpY2F0aW9uQmFja2VuZFUTYWxsb3dfdXBsb2FkX3N0cmVhbYh1Lg==	2014-03-02 20:23:35.957059+00
7zj54c8yi40awe1e1361sfpv96tt20n5	NTk1OGNhODAxM2JkNjJjN2JkNDEyNDc4ODhmMDlmODhjMzBjZDE4YTqAAn1xAShVDV9hdXRoX3VzZXJfaWRLBVUTYWxsb3dfdXBsb2FkX3N0cmVhbYhVEl9hdXRoX3VzZXJfYmFja2VuZFUzYWxsYXV0aC5hY2NvdW50LmF1dGhfYmFja2VuZHMuQXV0aGVudGljYXRpb25CYWNrZW5kdS4=	2014-03-01 05:13:01.386096+00
j2jmqt7bja5fo9jenrhhzsptpbey5av2	OTZlZjYzNDExNjAwZTkzNTQ4MzA4MzNhZDJhYzJkZTVhZGE1MWZiZTqAAn1xAShVE2FsbG93X3VwbG9hZF9zdHJlYW2IWA8AAABfc2Vzc2lvbl9leHBpcnlLAFUNX2F1dGhfdXNlcl9pZEsEVRJfYXV0aF91c2VyX2JhY2tlbmRVM2FsbGF1dGguYWNjb3VudC5hdXRoX2JhY2tlbmRzLkF1dGhlbnRpY2F0aW9uQmFja2VuZHUu	2014-03-02 00:08:12.295378+00
v3kvgc987mkxmx64h02wnvspu29fvhlx	YWQ3ZmY3Y2VjZTIxZTU3MWE1NDVmNDdiYTBiN2QxYWEzMDkwYjYyNTqAAn1xAShVFmFjY291bnRfdmVyaWZpZWRfZW1haWxxAk5VDV9hdXRoX3VzZXJfaWRxA0sFVQxhY2NvdW50X3VzZXJxBEsFVRJfYXV0aF91c2VyX2JhY2tlbmRxBVUzYWxsYXV0aC5hY2NvdW50LmF1dGhfYmFja2VuZHMuQXV0aGVudGljYXRpb25CYWNrZW5kcQZ1Lg==	2014-02-28 04:19:56.271755+00
jj7l96uz6ndd1spfbbkdzmgcx20o1qvw	OTQ0MWJjMzRlMDc4YzYxNTVkZDdhMmZjODg1MzZmMGU2ZWZhNjM3OTqAAn1xAShVDGFjY291bnRfdXNlcnECSwlVDV9hdXRoX3VzZXJfaWRxA0sJVRZhY2NvdW50X3ZlcmlmaWVkX2VtYWlscQROVRJfYXV0aF91c2VyX2JhY2tlbmRxBVUzYWxsYXV0aC5hY2NvdW50LmF1dGhfYmFja2VuZHMuQXV0aGVudGljYXRpb25CYWNrZW5kcQZ1Lg==	2014-03-03 21:52:25.126146+00
xmlp8vz2kp2d780oy7rvqcqtzx9xz2fc	Njc0Njc5MDUwYzE0MjM5ZjIwYmQ3Yzk1NmMyYjQzMjkwODgxZTQ3NDqAAn1xAVgKAAAAdGVzdGNvb2tpZXECWAYAAAB3b3JrZWRxA3Mu	2014-03-03 02:35:11.434588+00
rx275mud4t9wi52jxmfcf91y6g72uuwb	OWRkOTVmOGI2MWE3OWExZjNjZmI5N2Q1MzY3YmY3ODMyMDQ0NTdkYzqAAn1xAShVDGFjY291bnRfdXNlcnECSwZVDV9hdXRoX3VzZXJfaWRxA0sGVRZhY2NvdW50X3ZlcmlmaWVkX2VtYWlscQROVRJfYXV0aF91c2VyX2JhY2tlbmRxBVUzYWxsYXV0aC5hY2NvdW50LmF1dGhfYmFja2VuZHMuQXV0aGVudGljYXRpb25CYWNrZW5kcQZ1Lg==	2014-03-01 14:21:36.71323+00
x5lmzaq61rq7efy3u5bfutyb978gj7su	OWU1YTQxOTY0NmExMjVhMjgxOTY1YzNmOWU1ZWUxMGI2YmM2MmI1MjqAAn1xAShVDGFjY291bnRfdXNlcnECSwdVDV9hdXRoX3VzZXJfaWRxA0sHVRZhY2NvdW50X3ZlcmlmaWVkX2VtYWlscQROVRJfYXV0aF91c2VyX2JhY2tlbmRxBVUzYWxsYXV0aC5hY2NvdW50LmF1dGhfYmFja2VuZHMuQXV0aGVudGljYXRpb25CYWNrZW5kcQZ1Lg==	2014-03-01 17:18:12.379344+00
xdnm9mfkydk5s24bib3kfet0wyuq7c3f	ZmQ2ZDI5ZGI2OTE4NzU3YjE1M2FhM2Q5OWVkZTZjMTYzNWQwMzhlMTqAAn1xAShVDGFjY291bnRfdXNlcksIVQ1fYXV0aF91c2VyX2lkSwhVFmFjY291bnRfdmVyaWZpZWRfZW1haWxOVRJfYXV0aF91c2VyX2JhY2tlbmRVM2FsbGF1dGguYWNjb3VudC5hdXRoX2JhY2tlbmRzLkF1dGhlbnRpY2F0aW9uQmFja2VuZFUPZGphbmdvX2xhbmd1YWdlcQJYAgAAAGVucQN1Lg==	2014-03-01 17:18:38.125524+00
yine7nz3n8x7rzjeuxr9g1pmcelydkxh	OGE3MGZiYmEyNmM4N2U2M2MwMTJjNTQ0M2I2MmY1ZmE3Mzg1NGQ0NjqAAn1xAShVDV9hdXRoX3VzZXJfaWRxAksEWA8AAABfc2Vzc2lvbl9leHBpcnlxA0sAVRJfYXV0aF91c2VyX2JhY2tlbmRxBFUzYWxsYXV0aC5hY2NvdW50LmF1dGhfYmFja2VuZHMuQXV0aGVudGljYXRpb25CYWNrZW5kcQV1Lg==	2014-03-01 17:43:18.152149+00
vif1kiaehm32u12u26iu2bvggn9gwbbh	NDYxYWQwZThjNTIzNWIyZTNlMmVmMDU2ODBiMDQ4NzA2MTg4NmQyYzqAAn1xAShVDV9hdXRoX3VzZXJfaWRxAksFVRJfYXV0aF91c2VyX2JhY2tlbmRxA1UzYWxsYXV0aC5hY2NvdW50LmF1dGhfYmFja2VuZHMuQXV0aGVudGljYXRpb25CYWNrZW5kcQR1Lg==	2014-03-02 18:59:56.795524+00
y1d8xldefhzxm30fcvpmu7daacyz0kbp	NDYxYWQwZThjNTIzNWIyZTNlMmVmMDU2ODBiMDQ4NzA2MTg4NmQyYzqAAn1xAShVDV9hdXRoX3VzZXJfaWRxAksFVRJfYXV0aF91c2VyX2JhY2tlbmRxA1UzYWxsYXV0aC5hY2NvdW50LmF1dGhfYmFja2VuZHMuQXV0aGVudGljYXRpb25CYWNrZW5kcQR1Lg==	2014-03-08 05:05:59.787382+00
3kt3mldnh0t1y9jx6vn47rh4pypgcsma	YWViZjY2MTgzZjA3MzkwODNmMWZiYjkzOTNmMDEwMzVhOGI4Mjc5MDqAAn1xAShVFmFjY291bnRfdmVyaWZpZWRfZW1haWxxAk5VDGFjY291bnRfdXNlcnEDSwtVDV9hdXRoX3VzZXJfaWRxBEsLVRJfYXV0aF91c2VyX2JhY2tlbmRxBVUzYWxsYXV0aC5hY2NvdW50LmF1dGhfYmFja2VuZHMuQXV0aGVudGljYXRpb25CYWNrZW5kcQZ1Lg==	2014-03-04 22:08:20.382313+00
lu3e35woocg0zn0au13jkoclt3bfomht	Y2ExM2IyNmNlMDVjY2Q4YjY1NTIxMzY2NTliMzYyMjIwMTRmNjBlZjqAAn1xAShVDGFjY291bnRfdXNlcksKVQ1fYXV0aF91c2VyX2lkSwpVFmFjY291bnRfdmVyaWZpZWRfZW1haWxOVRJfYXV0aF91c2VyX2JhY2tlbmRVM2FsbGF1dGguYWNjb3VudC5hdXRoX2JhY2tlbmRzLkF1dGhlbnRpY2F0aW9uQmFja2VuZFUTYWxsb3dfdXBsb2FkX3N0cmVhbYh1Lg==	2014-03-04 01:28:05.876981+00
14iysjs8s8ss3men0pshd2le9byoim5u	ZDBjY2I4MzdhODllMTk0ZjA5M2Q0MDFiNGRmMjc4ZjhjMGZjM2NiMzqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVTNhbGxhdXRoLmFjY291bnQuYXV0aF9iYWNrZW5kcy5BdXRoZW50aWNhdGlvbkJhY2tlbmRxA1UNX2F1dGhfdXNlcl9pZHEESwN1Lg==	2014-03-05 21:03:28.445101+00
c7gavhm9mgnl8sc1bmdt6vhz5s0odirs	MDc5NjMwYWY4NGViYmMwNmM3YjRjM2VlZjA1YzY2MjQ3ZTQyZDA3NDqAAn1xAS4=	2014-03-08 08:27:18.243291+00
flkvsf1a4w2zu1aerndk0xhhoqv3f3j0	NGQ0YjdlMThlY2IxMTZhZWIzMTlkMzcyNWY5MjdmZjczYTg0NzU4ZDqAAn1xAShVDV9hdXRoX3VzZXJfaWRxAksMVQxhY2NvdW50X3VzZXJxA0sMVRZhY2NvdW50X3ZlcmlmaWVkX2VtYWlscQROVRJfYXV0aF91c2VyX2JhY2tlbmRxBVUzYWxsYXV0aC5hY2NvdW50LmF1dGhfYmFja2VuZHMuQXV0aGVudGljYXRpb25CYWNrZW5kcQZ1Lg==	2014-03-08 17:46:06.532033+00
sii0ab7khwk0s3xi399a1hjl08saop20	NWE0MjMwMzViOTFhZDIzZmJhMjNkNWNjZTVjYzFmYmE1NzUyMWE5OTqAAn1xAShVDV9hdXRoX3VzZXJfaWRxAksHVRJfYXV0aF91c2VyX2JhY2tlbmRxA1UzYWxsYXV0aC5hY2NvdW50LmF1dGhfYmFja2VuZHMuQXV0aGVudGljYXRpb25CYWNrZW5kcQR1Lg==	2014-03-09 09:50:40.130693+00
zwonyaknev4fud68hdiv7sd6vpt32k4f	ZDlhZDY5MjQ0YTg5NWEwYTExOTZhZmVhODI5ZjEyMDJmMTM3N2ZiMTqAAn1xAShVDV9hdXRoX3VzZXJfaWRxAksCVRJfYXV0aF91c2VyX2JhY2tlbmRxA1UzYWxsYXV0aC5hY2NvdW50LmF1dGhfYmFja2VuZHMuQXV0aGVudGljYXRpb25CYWNrZW5kcQR1Lg==	2014-03-09 17:59:26.992851+00
oq0xs5hae6nv7661wtrc81bcydxtsjnl	MGQ5ZDU4Y2FhOWUwMGQ2ZmNhMGRlNGYyOGMzNmVlODMxYWRjYmJjMzqAAn1xAShVDV9hdXRoX3VzZXJfaWRLBFgKAAAAdGVzdGNvb2tpZXECWAYAAAB3b3JrZWRxA1gPAAAAX3Nlc3Npb25fZXhwaXJ5SwBVEl9hdXRoX3VzZXJfYmFja2VuZFUzYWxsYXV0aC5hY2NvdW50LmF1dGhfYmFja2VuZHMuQXV0aGVudGljYXRpb25CYWNrZW5kdS4=	2014-03-08 23:06:58.87275+00
fl6t8wfnvfkzn2g038tlkxfbn6pecga4	Njc0Njc5MDUwYzE0MjM5ZjIwYmQ3Yzk1NmMyYjQzMjkwODgxZTQ3NDqAAn1xAVgKAAAAdGVzdGNvb2tpZXECWAYAAAB3b3JrZWRxA3Mu	2014-03-10 02:57:18.046141+00
4hmwfukkc2zi8a1drhlos6djtgzowd0y	MTk5MTU0ZjU1ZWYyNTViNGQ0ZjkwZDAwMGNiMGZiMDYyMWEzMzc1NjqAAn1xAShVDV9hdXRoX3VzZXJfaWRxAksBVRJfYXV0aF91c2VyX2JhY2tlbmRxA1UzYWxsYXV0aC5hY2NvdW50LmF1dGhfYmFja2VuZHMuQXV0aGVudGljYXRpb25CYWNrZW5kcQR1Lg==	2014-03-10 02:59:16.796575+00
i8xrnfue55cgk9fzu5s0wy4mbshbvisc	NDBhMzk4NjAyN2VlZjM5YjQwN2IzYzc5YTgyOWM5Y2VhODAxYjE3MDqAAn1xAShVDV9hdXRoX3VzZXJfaWRxAksDVRJfYXV0aF91c2VyX2JhY2tlbmRxA1UzYWxsYXV0aC5hY2NvdW50LmF1dGhfYmFja2VuZHMuQXV0aGVudGljYXRpb25CYWNrZW5kcQR1Lg==	2014-03-10 11:33:24.819254+00
v0fqmexfp4mysia4etcyv37f1ta7731b	ODFmNjM0NDM5NGZhMzRmYjBhZDNhYjQ5MjI2M2E4ZDgyZGU1NTZjOTqAAn1xAShVDV9hdXRoX3VzZXJfaWRLAlUTYWxsb3dfdXBsb2FkX3N0cmVhbXECiFUSX2F1dGhfdXNlcl9iYWNrZW5kVTNhbGxhdXRoLmFjY291bnQuYXV0aF9iYWNrZW5kcy5BdXRoZW50aWNhdGlvbkJhY2tlbmR1Lg==	2014-03-10 19:17:51.230492+00
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY django_site (id, domain, name) FROM stdin;
1	frittie.com	frittie
\.


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('django_site_id_seq', 1, true);


--
-- Data for Name: djcelery_crontabschedule; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY djcelery_crontabschedule (id, minute, hour, day_of_week, day_of_month, month_of_year) FROM stdin;
\.


--
-- Name: djcelery_crontabschedule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('djcelery_crontabschedule_id_seq', 1, false);


--
-- Data for Name: djcelery_intervalschedule; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY djcelery_intervalschedule (id, every, period) FROM stdin;
\.


--
-- Name: djcelery_intervalschedule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('djcelery_intervalschedule_id_seq', 1, false);


--
-- Data for Name: djcelery_periodictask; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY djcelery_periodictask (id, name, task, interval_id, crontab_id, args, kwargs, queue, exchange, routing_key, expires, enabled, last_run_at, total_run_count, date_changed, description) FROM stdin;
\.


--
-- Name: djcelery_periodictask_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('djcelery_periodictask_id_seq', 1, false);


--
-- Data for Name: djcelery_periodictasks; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY djcelery_periodictasks (ident, last_update) FROM stdin;
\.


--
-- Data for Name: djcelery_taskstate; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY djcelery_taskstate (id, state, task_id, name, tstamp, args, kwargs, eta, expires, result, traceback, runtime, retries, worker_id, hidden) FROM stdin;
\.


--
-- Name: djcelery_taskstate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('djcelery_taskstate_id_seq', 1, false);


--
-- Data for Name: djcelery_workerstate; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY djcelery_workerstate (id, hostname, last_heartbeat) FROM stdin;
\.


--
-- Name: djcelery_workerstate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('djcelery_workerstate_id_seq', 1, false);


--
-- Data for Name: main_activity; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_activity (id, unique_id, name, description, start_time, end_time, logo_id, user_create_id, location_id, "limit", activity_type, category, sell_ticket) FROM stdin;
1	acxj8eLo5VwnVkN9TGAvEyY8042	moon festival	Come to meet children	2014-02-20 18:00:48+00	2014-02-21 18:00:51+00	51	2	3	-1	1	1	1
\.


--
-- Data for Name: main_activity_activity_request; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_activity_activity_request (id, activity_id, activityrequest_id) FROM stdin;
3	1	2
\.


--
-- Name: main_activity_activity_request_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_activity_activity_request_id_seq', 3, true);


--
-- Data for Name: main_activity_comments; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_activity_comments (id, activity_id, comment_id) FROM stdin;
2	1	1
\.


--
-- Name: main_activity_comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_activity_comments_id_seq', 2, true);


--
-- Name: main_activity_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_activity_id_seq', 1, true);


--
-- Data for Name: main_activity_list_photos; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_activity_list_photos (id, activity_id, photo_id) FROM stdin;
\.


--
-- Name: main_activity_list_photos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_activity_list_photos_id_seq', 1, false);


--
-- Data for Name: main_activity_posts; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_activity_posts (id, activity_id, notification_id) FROM stdin;
\.


--
-- Name: main_activity_posts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_activity_posts_id_seq', 1, false);


--
-- Data for Name: main_activity_reports; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_activity_reports (id, activity_id, report_id) FROM stdin;
\.


--
-- Name: main_activity_reports_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_activity_reports_id_seq', 1, false);


--
-- Data for Name: main_activity_tags; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_activity_tags (id, activity_id, tag_id) FROM stdin;
\.


--
-- Name: main_activity_tags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_activity_tags_id_seq', 1, false);


--
-- Data for Name: main_activity_ticket; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_activity_ticket (id, activity_id, ticket_id) FROM stdin;
\.


--
-- Name: main_activity_ticket_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_activity_ticket_id_seq', 1, false);


--
-- Data for Name: main_activityrequest; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_activityrequest (id, activity_id, user_request_id, date, request_status, greet, description) FROM stdin;
2	1	5	2014-02-15 05:13:00.335249+00	0		
\.


--
-- Name: main_activityrequest_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_activityrequest_id_seq', 2, true);


--
-- Data for Name: main_activityuserrelation; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_activityuserrelation (id, activity_id, user_id, status) FROM stdin;
\.


--
-- Name: main_activityuserrelation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_activityuserrelation_id_seq', 1, false);


--
-- Data for Name: main_comment; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_comment (id, unique_id, comment_type, user_id, content, create_date, edit_date) FROM stdin;
1	cmnHT6SFonS7WnKMJkAGKLnL008	activity	2	Hello I am Duckie	2014-02-13 23:48:25.450741+00	\N
\.


--
-- Name: main_comment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_comment_id_seq', 1, true);


--
-- Data for Name: main_comment_reports; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_comment_reports (id, comment_id, report_id) FROM stdin;
\.


--
-- Name: main_comment_reports_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_comment_reports_id_seq', 1, false);


--
-- Data for Name: main_conversation; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_conversation (id, unique_id, user1_id, user2_id, latest_message_id) FROM stdin;
3	cnUHbrCBYPCmEHmbXGyztmDP068	2	9	4
4	cnzzLYEbZ3y7mJpVv6Nx5xX6057	10	2	5
5	CNMGzA59ZqRdfpsvUPcdz3FE080	8	2	8
6	CNegWnDesAQpByWeY64o99MY095	12	9	9
2	cngtyBDWwF8uKJ9JfA3XscWb098	3	2	12
1	cndGzHGnRTNdFF6x8DBUsP9Z063	3	4	15
\.


--
-- Name: main_conversation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_conversation_id_seq', 6, true);


--
-- Data for Name: main_conversation_messages; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_conversation_messages (id, conversation_id, message_id) FROM stdin;
1	1	1
2	2	2
3	3	3
4	3	4
5	4	5
6	5	6
7	5	7
8	5	8
9	6	9
10	2	10
11	2	11
12	2	12
13	1	13
14	1	14
15	1	15
\.


--
-- Name: main_conversation_messages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_conversation_messages_id_seq', 15, true);


--
-- Data for Name: main_feed; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_feed (id, unique_id, content, create_by_id, object_type, object_unique_id, date) FROM stdin;
\.


--
-- Name: main_feed_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_feed_id_seq', 1, false);


--
-- Data for Name: main_feed_user_receive; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_feed_user_receive (id, feed_id, user_id) FROM stdin;
\.


--
-- Name: main_feed_user_receive_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_feed_user_receive_id_seq', 1, false);


--
-- Data for Name: main_frit; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_frit (id, frit_type, user_id, date, object_id) FROM stdin;
\.


--
-- Name: main_frit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_frit_id_seq', 1, false);


--
-- Data for Name: main_invitation; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_invitation (id, unique_id, message, activity_id, send_from_id) FROM stdin;
\.


--
-- Name: main_invitation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_invitation_id_seq', 1, false);


--
-- Data for Name: main_invitation_list_photos; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_invitation_list_photos (id, invitation_id, photo_id) FROM stdin;
\.


--
-- Name: main_invitation_list_photos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_invitation_list_photos_id_seq', 1, false);


--
-- Data for Name: main_invitation_list_videos; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_invitation_list_videos (id, invitation_id, video_id) FROM stdin;
\.


--
-- Name: main_invitation_list_videos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_invitation_list_videos_id_seq', 1, false);


--
-- Data for Name: main_invitation_send_to; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_invitation_send_to (id, invitation_id, user_id) FROM stdin;
\.


--
-- Name: main_invitation_send_to_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_invitation_send_to_id_seq', 1, false);


--
-- Data for Name: main_location; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_location (id, unique_id, name, description, category, address1, address2, lat, lng, city, state, zip_code, country, website_link, main_picture_id, create_by_id, preference, rating) FROM stdin;
1	loYpbDg5SJgKxFLtB4B833FY077	Sabrina's Cafe @ Powelton	test	0	3636 Sansom S		39.9538020000	-75.1961705000	Philadelphia	PA	19104	United States		1	1		0
2	lo4pseBoayrSdFZLRhitXHmT022	Pod	test	0	3945 Chestnut S		39.9557105000	-75.2019318000	Philadelphia	PA	19104	United States		2	1		0
3	loV3vJjAwL8fxRpB7PZ4yquC034	Distrito	test	0	3420 Sansom S		39.9534188000	-75.1929704000	Philadelphia	PA	19104	United States		3	1		0
4	loUQs8mdQbkNiLpL8LAQTePM087	White Dog Cafe	test	0	4004 Chestnut S		39.9553910000	-75.2026140000	Philadelphia	PA	19104	United States		4	1		0
5	lorRpc2AYpNS6AmF6ydZvTp9039	New Delhi	test	0	3925 Walnut S		39.9540281000	-75.2018645000	Philadelphia	PA	19104	United States		5	1		0
6	lozqLw2Hpp6YRGkwa4FvPwqW065	Bobby's Burger Palace	test	0	4728 Baltimore Av		39.9483087000	-75.2178905000	Philadelphia	PA	19104	United States		6	1		0
7	lo22xp3iHUcBr38gAPyjmvuN094	Vientiane Caf\\xe9	test	0	3408 Sansom S		39.9535171000	-75.1926123000	Philadelphia	PA	19104	United States		7	1		0
8	loHexeuwtLhyVaxeg7hPTRaX024	New Deck Tavern	test	0	229 S 45th S		39.9541570000	-75.2116170000	Philadelphia	PA	19104	United States		8	1		0
9	lopJM6NH3YNhTQsPtd46oKw9090	Abyssinia Ethiopian	test	0	227 N 34th S		39.9591549000	-75.1908344000	Philadelphia	PA	19104	United States		9	1		0
10	lorzsPe3xs4ufnFWo2LqaNQX083	Sabrina's Cafe @ Powelton	test	0	4333 Spruce S		39.9526493000	-75.2102845000	Philadelphia	PA	19104	United States		10	1		0
11	louYRgjMqSpWyPjUsC88KGBF008	Local 44	test	0	3925 Walnut S		39.9540281000	-75.2018645000	Philadelphia	PA	19104	United States		11	1		0
12	lojGKmLf5icghhf46Qs2i2D7019	City Tap House	test	0	501 S 45th S		39.9501204000	-75.2123812000	Philadelphia	PA	19104	United States		12	1		0
13	lok6PQYikzGocrqazCa2c66c005	Marigold Kitchen	test	0	701 S 50th S		39.9477050000	-75.2226990000	Philadelphia	PA	19104	United States		13	1		0
14	loXptgRQMkCYoBVQ63TVVYRg064	Dock Street Brewery	test	0	3801 Chestnut S		39.9555869000	-75.1985394000	Philadelphia	PA	19104	United States		14	1		0
15	lokvfee2MqvPSxamuXhgFnGm023	Koreana	test	0	4500 Walnut S		39.9549900000	-75.2119139000	Philadelphia	PA	19104	United States		15	1		0
16	logCh7xo7piguZbVNPMTD58n040	Saad's Halal Place	test	0	4201 Chestnut S		39.9564920000	-75.2065805000	Philadelphia	PA	19104	United States		16	1		0
17	lore7Kk3jVPZsfDNU8tM5hC3030	Kabobeesh	test	0	816 S 47th S		39.9480870000	-75.2170970000	Philadelphia	PA	19104	United States		17	1		0
18	loUxgHeg34LfNtfgTtqj7hgj088	Vietnam Cafe	test	0	3333 Market Stree		39.9559049000	-75.1906218000	Philadelphia	PA	19104	United States		18	1		0
19	lojurerwRPvpmzA4gjaEJec5082	Landmark Americana - University City	test	0	4317 Chestnut S		39.9565079000	-75.2092518000	Philadelphia	PA	19104	United States		19	1		0
20	loW7DG7cqrkGWMQf6XaPJFS6001	Kilimandjaro	test	0	S 34th St & Walnut S		39.9528123000	-75.1920992000	Philadelphia	PA	19104	United States		20	1		0
21	loRLTyztuBH8SSULGmLnZsPj098	Magic Carpet Foods	test	0	222 S 40th S		39.9533806000	-75.2028459000	Philadelphia	PA	19104	United States		21	1		0
22	lopp5XqbQFuHdqUgcLARCooh052	Greek Lady	test	0	60 S 38th S		39.9556768000	-75.1985260000	Philadelphia	PA	19104	United States		22	1		0
23	loEnPCu8VvxMKP6VFi8pQ8YS096	Sitar India	test	0	3000 Market S		39.9548017000	-75.1839334000	Philadelphia	PA	19104	United States		23	1		0
24	loedBgMwpr7jWt9YcRkAKace080	Slainte Pub & Grill	test	0	4006 Chestnut S		39.9554000000	-75.2026840000	Philadelphia	PA	19104	United States		24	1		0
25	loDJFoJKRXZcmSWtS9tDFzdR069	Pattaya Grill	test	0	3925 Walnut S		39.9540281000	-75.2018645000	Philadelphia	PA	19104	United States		25	1		0
26	lotWEv84mMCXFpwn4cBLZzab006	Capogiro Gelateria	test	0	3402 Sansom Stree		39.9535060000	-75.1924351000	Philadelphia	PA	19104	United States		26	1		0
27	loLHN9vVA2EmcfgmM9zweyL9090	Baby Blues BBQ	test	0	4700 Baltimore Av		39.9484223000	-75.2170858000	Philadelphia	PA	19104	United States		27	1		0
28	lodLyM9AvT46haAWHDBysa48093	Lee's Deli	test	0	4420 Walnut Stree		39.9549520000	-75.2115230000	Philadelphia	PA	19104	United States		28	1		0
29	loLxMScLhFbwpibvvdVQ25t5010	Manakeesh Cafe Bakery	test	0	3505 Lancaster Av		39.9583099000	-75.1926601000	Philadelphia	PA	19104	United States		29	1		0
30	loZ8sh2tkMqhFnYABTPUn9fR068	Savas Brick Oven Pizza	test	0	4708 Baltimore Av		39.9484115000	-75.2173425000	Philadelphia	PA	19104	United States		30	1		0
31	louhsYfggF3tBaanSxSUikHC086	Dahlak	test	0	4309 Locust S		39.9540420000	-75.2097070000	Philadelphia	PA	19104	United States		31	1		0
32	loakU5McdtoPYJzGvNo2BKvW041	Koch's Deli	test	0	261 S 44th S		39.9532429000	-75.2102722000	Philadelphia	PA	19104	United States		32	1		0
33	loCVawa3L6py4UUVE5vZqYdC074	Honest Tom's Taco Shop	test	0	3630 Lancaster Av		39.9588880000	-75.1947628000	Philadelphia	PA	19104	United States		33	1		0
34	lojzgzmgvUa97C4KTynHbRcH015	Lemon Grass Thai	test	0	4248 Spruce S		39.9522229000	-75.2091130000	Philadelphia	PA	19104	United States		34	1		0
35	lojweTS3zCsFoAex2Ynk9Z9g068	Pho Cafe Saigon	test	0	3549 Chestnut stree		39.9546220000	-75.1940770000	Philadelphia	PA	19104	United States		35	1		0
36	locy5y29bw89fyr4aNLUEauT022	Sang Kee Noodle House	test	0	2929 Arch Stree		39.9572584000	-75.1824709000	Philadelphia	PA	19104	United States		36	1		0
37	loJPJrUFmLnDAsTLx3c29kSb053	JG Domestic	test	0	4423 Chestnut S		39.9567714000	-75.2104636000	Philadelphia	PA	19104	United States		37	1		0
38	loCqxFYbJtuzfnuWJfe7dCDV055	Kaffa Crossing	test	0	4239 Baltimore Av		39.9497678000	-75.2090680000	Philadelphia	PA	19104	United States		38	1		0
39	lojGfwWThJp7AanMcze4V3JX035	Green Line Cafe	test	0	4447 Chestnut S		39.9568576000	-75.2112572000	Philadelphia	PA	19104	United States		39	1		0
40	loxMZ3XcET5ugMviWGB5toxG090	Wah-Gi-Wah	test	0	3931 Walnut S		39.9543700000	-75.2021120000	Philadelphia	PA	19104	United States		40	1		0
41	loKfrWJkockFqqcD4PdN4anH037	Hummus	test	0	4345 Lancaster Av		39.9687323000	-75.2109918000	Philadelphia	PA	19104	United States		41	1		0
42	loRnkbfnwMnZurLhZFahREva064	Dwight's Southern Bar-B-Q	test	0	3401 Walnut S		39.9531920000	-75.1928820000	Philadelphia	PA	19104	United States		42	1		0
43	lorHumeWpiCTrPebp2DYFyJK035	Mad Mex	test	0	3606 Chestnut S		39.9545368000	-75.1950208000	Philadelphia	PA	19104	United States		43	1		0
44	lo35vf5vAeWZnBpCowBZdoTn060	Kiwi Yogurt	test	0	4013 Walnut S		39.9542753000	-75.2033728000	Philadelphia	PA	19104	United States		44	1		0
45	loauQtFwxd5G2ZXMzieMAc6Y078	Metropolitan Bakery & Cafe	test	0	700 N 43rd S		39.9647200000	-75.2096320000	Philadelphia	PA	19104	United States		45	1		0
46	lo3fiUpLkHXVNSvfdm4SQVPQ005	Bottom of the Sea	test	0	3942 Spruce S		39.9513910000	-75.2029570000	Philadelphia	PA	19104	United States		46	1		0
47	lofuJVnRnwkHLk5uY7npdH49043	Allegro Pizza	test	0	106 S 40th S		39.9551144000	-75.2026090000	Philadelphia	PA	19104	United States		47	1		0
48	locu2hujKMFWa7WxqsNSi6d8052	Tandoor India	test	0	3604 Chestnut S		39.9545306000	-75.1949702000	Philadelphia	PA	19104	United States		48	1		0
49	lohohBtUdXKtUkZwYB9JproR002	Wawa Food Market	test	0	4002 Spruce S		39.9515270000	-75.2033450000	Philadelphia	PA	19104	United States		49	1		0
\.


--
-- Data for Name: main_location_comments; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_location_comments (id, location_id, comment_id) FROM stdin;
\.


--
-- Name: main_location_comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_location_comments_id_seq', 1, false);


--
-- Data for Name: main_location_follow_by; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_location_follow_by (id, location_id, user_id) FROM stdin;
1	3	9
\.


--
-- Name: main_location_follow_by_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_location_follow_by_id_seq', 1, true);


--
-- Name: main_location_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_location_id_seq', 49, true);


--
-- Data for Name: main_location_list_photos; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_location_list_photos (id, location_id, photo_id) FROM stdin;
\.


--
-- Name: main_location_list_photos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_location_list_photos_id_seq', 1, false);


--
-- Data for Name: main_location_posts; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_location_posts (id, location_id, notification_id) FROM stdin;
\.


--
-- Name: main_location_posts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_location_posts_id_seq', 1, false);


--
-- Data for Name: main_location_reports; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_location_reports (id, location_id, report_id) FROM stdin;
\.


--
-- Name: main_location_reports_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_location_reports_id_seq', 1, false);


--
-- Data for Name: main_message; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_message (id, unique_id, user_send_id, user_receive_id, content, status, date) FROM stdin;
1	mgiE6ntKtLr65iRedagGMgYc073	3	4	alo	0	2014-02-15 10:20:01.216625+00
2	mg8MYQv4sKiDk2sTMvop99Gn086	3	2	safdsfsd	0	2014-02-16 22:27:36.485532+00
3	mgpqN5XuvuwEJghQQoind4TF091	2	9	Hello mohammed	0	2014-02-17 22:13:55.131628+00
4	mgBLsMJWGAZ8P6RDWDsQ9UEo058	9	2	Hi Duckk	0	2014-02-17 22:15:26.477573+00
5	mg7NNg2xePHuPhAa4qH4HBj8036	10	2	hello	0	2014-02-18 01:27:44.193131+00
6	MGag89bigzNdjtncTkD7nJYR081	8	2	ú ù	0	2014-02-20 11:09:04.424597+00
7	MGW9sGKeNJxZB6JQ8gWJmrtU019	2	8	u' u` 	0	2014-02-20 11:09:42.124625+00
8	MGX3HYU7GEoxdfYnEv4zLEgX053	2	8	hello	0	2014-02-20 11:09:52.572057+00
9	MG8taWgWWV5DgxLLKMFtUq4V077	12	9	Hello dude	0	2014-02-22 17:46:48.43509+00
10	MGrnKRSYS9bC6d2VEc732fyK024	3	2	fuck you	0	2014-02-22 23:03:17.881927+00
13	MGMjTfnAGUjXj2yzchwZUwTT028	4	3	what sup	0	2014-02-22 23:07:25.488994+00
14	MGzdXva3xTdZ2pYs4vEBbTdQ090	3	4	gdgdfd	0	2014-02-22 23:12:51.549617+00
11	MGcjt5Mf3Fm9vVnTdjY2zkch021	3	2	dadsadsq	0	2014-02-22 23:04:18.014106+00
12	MGmR6TkDqhSXeSQGbz5QMCJe002	3	2	fsdfsd	0	2014-02-22 23:04:38.331378+00
15	MGr3uPc2Hg8McWSAMXtG9Lc5040	3	4	gffgjfgh	1	2014-02-22 23:15:01.581895+00
\.


--
-- Name: main_message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_message_id_seq', 15, true);


--
-- Data for Name: main_notification; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_notification (id, unique_id, content, status, notification_type, notify_from_id, date) FROM stdin;
\.


--
-- Name: main_notification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_notification_id_seq', 1, false);


--
-- Data for Name: main_notification_notify_to; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_notification_notify_to (id, notification_id, user_id) FROM stdin;
\.


--
-- Name: main_notification_notify_to_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_notification_notify_to_id_seq', 1, false);


--
-- Data for Name: main_photo; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_photo (id, unique_id, filename, image, upload_date, user_post_id, photo_type) FROM stdin;
1	ptJaGDGSqAz9wiZFznWHhBfV008	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:04.1931+00	1	
2	ptnVqC4NhGVx85vsijDqAziD066	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:04.574906+00	1	
3	ptbXcgxTd5oVbbtWr4kfaMH9001	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:04.836698+00	1	
4	pt9dxDBSqzFVDXSB8NWuvKTm075	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:05.100239+00	1	
5	pt8YTp8ZDhaKMHMsVWEvEHoS026	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:05.364818+00	1	
6	ptrwoTGqYPEGsknbhLdHSRM6004	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:05.64528+00	1	
7	pt5v6Q8huw5bkdbfETmCzNkj022	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:05.924002+00	1	
8	ptCMDUvkxgBrAsLYpS39sg9o007	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:06.277328+00	1	
9	ptEfQZQvoBQWMqDDHK6Z65kj051	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:06.557571+00	1	
10	ptydDrF7TqENYmwUKrQGrevT010	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:06.886688+00	1	
11	ptr63G95Um74C9bCZ7iYZfjj097	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:07.14655+00	1	
12	pt7hgsZ5h3xDYjWwygYbNah3032	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:07.479423+00	1	
13	ptn8LTaLEHNKMDPGkUEHjMrH094	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:07.772351+00	1	
14	ptBH5DEGXh4Mh2yEJn6dAkzk053	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:08.064339+00	1	
15	ptHH53BmaepALDDhF9VdaTe4022	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:08.37201+00	1	
16	ptwG8rZPAWEtoyTLqmv9RFKA052	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:08.648125+00	1	
17	ptB8BCSCg79yaqjyqNt4ikkB004	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:08.973308+00	1	
18	pt7gsMdfRbvSVEezKEvyRB8a021	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:09.272366+00	1	
19	ptNTyEbEj29Cti4ZRxaqR5pH087	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:09.561018+00	1	
20	pttHQEAqDeWjmG532knjoLRN058	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:09.829671+00	1	
21	ptcT5DzxRU7mNZGQSGaDPrmX050	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:10.129398+00	1	
22	ptQREtxiWLaTpBUVAU9fxNZU086	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:10.419073+00	1	
23	ptoSAwTVFdf88UqLGVZrpyRL086	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:10.731688+00	1	
24	ptU7QyJgWddkqsR7WYyGodtM029	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:11.061676+00	1	
25	ptBsQRChmAfuDvomSayBpkUA013	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:11.495661+00	1	
26	ptoStxjswKPQ3uTZo4fwZ9uT056	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:11.807999+00	1	
27	ptcJBHENgzW4nv9Fc7TRkR94031	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:12.0806+00	1	
28	pteN8FYbkmX3uRAWGXNczVV5078	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:12.377689+00	1	
29	pt3KbeEyx4KzKAqiaws5sHeE020	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:12.647364+00	1	
30	ptRGDEi39dChMAvsjzdS4Ted070	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:12.920768+00	1	
31	ptL8T6cykHncxHEycHaa85iU031	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:13.213658+00	1	
32	ptDyy4W3rW5in7gGkpu7AHVo037	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:13.577524+00	1	
33	ptATtJiK8tPiLK4swM6GrYBA050	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:13.862833+00	1	
34	ptDUHunVJXuAK5huzWKfVYPX058	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:14.125858+00	1	
35	ptQrhwBBwozYjzsG2dXRgRe073	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:14.403633+00	1	
36	pty8m7HPtydJ2L2MfjCePgvE098	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:14.660567+00	1	
37	ptpaJjpzqGeRkjrfsFMz3fDN065	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:14.937755+00	1	
38	ptXpDNU2r4CZXsR4FCpJvN4F076	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:15.227931+00	1	
39	ptKQ9xV8NWHWBkjhzKVSuWL056	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:15.517157+00	1	
40	ptL2koGDRWQHJw7GADjeSGXS013	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:15.764967+00	1	
41	ptHX7sxZQMok88qzzniQbKDR004	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:16.027377+00	1	
42	ptxM2Pzw5hkwKe8dNPuREtiY099	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:16.2942+00	1	
43	ptPDa7tK53aUNSphFpxGnAxS086	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:16.55094+00	1	
44	pt7omdbKSqZPk3EPQjGWPuEb084	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:16.874139+00	1	
45	ptxTyjSGKwRG8J8AZVQu7JL6082	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:17.129619+00	1	
46	ptvLcjUoUrgXr4YC8cvVivDc025	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:17.42984+00	1	
47	ptg2MtMNWhFsQc3Q7DYShoDa032	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:17.741994+00	1	
48	ptsWk5XUPtNojqRXUjp4b4Ek058	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:18.02214+00	1	
49	ptjpWYXAcLvxpX8BXQXdSqzU030	default restaurant photo	/media/default/img/location/RestaurantIcon.png	2014-02-13 22:47:18.349728+00	1	
50	ptutFZ9Gv94TZJZej7cFZ6uj011	vuthanhduc_avatar	uploads/img/share/photo/2014/02/13/facebook_user_702871491_avatar.jpg	2014-02-13 22:51:29.688642+00	2	
51	4jl1kj3lkj213	Moon festival	uploads/img/share/photo/2014/02/13/childrenexhibition.jpg	2014-02-13 22:56:05.080664+00	2	1
52	ptaqS9pyzuPPCXx3j9tvmCZQ096	Danny.Nguyen1712_avatar	uploads/img/share/photo/2014/02/13/facebook_user_1062759651_avatar.jpg	2014-02-13 23:17:40.394774+00	3	
53	ptEQuSA69q426ysiaTbX3jA7017	Duc_Vu_avatar	default/img/user/male_icon.png	2014-02-13 23:49:29.159221+00	4	
54	ptyjtGtoY9uJtz5a3dMG8qwJ086	lekinh.hieu_avatar	uploads/img/share/photo/2014/02/14/facebook_user_100000074240587_avatar.jpg	2014-02-14 04:19:52.894772+00	5	
55	ptN57op4xSPN3hRKu2LuTBKZ086	knightus_avatar	uploads/img/share/photo/2014/02/15/facebook_user_1077534707_avatar.jpg	2014-02-15 14:21:33.908131+00	6	
56	ptRm5HotA2DHsJpNXFxfL4PX016	de606b_avatar	uploads/img/share/photo/2014/02/15/facebook_user_1436051371_avatar.jpg	2014-02-15 17:18:09.721365+00	7	
57	ptnu5dfJsJNAYNiK5bKg9Jn3068	tdz0110_avatar	uploads/img/share/photo/2014/02/15/facebook_user_1676239881_avatar.jpg	2014-02-15 17:18:23.324767+00	8	
58	pt7ND5SA5op7atQLCxD47dGG093	adabou_avatar	uploads/img/share/photo/2014/02/17/facebook_user_100001505924367_avatar.jpg	2014-02-17 21:52:22.870005+00	9	
59	ptmumgxgvyUTKVUzgk4ymznP099	huetvuong_avatar	uploads/img/share/photo/2014/02/18/facebook_user_1528519675_avatar.jpg	2014-02-18 01:25:52.393568+00	10	
60	ptWdyZY8zkUJC8UD8s88Cifh088	etuleu_avatar	uploads/img/share/photo/2014/02/18/facebook_user_37402681_avatar.jpg	2014-02-18 22:08:18.113823+00	11	
61	PT3VpDMn3jhCZ8VBXeFFj7RX061	baobao32_avatar	uploads/img/share/photo/2014/02/22/facebook_user_1345749750_avatar.jpg	2014-02-22 17:46:03.814574+00	12	
\.


--
-- Name: main_photo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_photo_id_seq', 61, true);


--
-- Data for Name: main_report; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_report (id, unique_id, report_type, user_report_id, date, report_content) FROM stdin;
\.


--
-- Name: main_report_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_report_id_seq', 1, false);


--
-- Data for Name: main_siteglobaldata; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_siteglobaldata (id, users_with_fb_id, site) FROM stdin;
\.


--
-- Name: main_siteglobaldata_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_siteglobaldata_id_seq', 1, false);


--
-- Data for Name: main_tag; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_tag (id, user_tag_id, content) FROM stdin;
\.


--
-- Name: main_tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_tag_id_seq', 1, false);


--
-- Data for Name: main_ticket; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_ticket (id, price, fee, ticket_status, ticket_type) FROM stdin;
\.


--
-- Name: main_ticket_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_ticket_id_seq', 1, false);


--
-- Data for Name: main_tickettransaction; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_tickettransaction (id, user_id, ticket_id, quantity, transaction_date) FROM stdin;
\.


--
-- Name: main_tickettransaction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_tickettransaction_id_seq', 1, false);


--
-- Data for Name: main_userprofile; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_userprofile (id, user_id, basic_info, gender, avatar_id, city, state, country, privacy_status, date_of_birth, facebook_id) FROM stdin;
1	1		m	\N				1	\N	\N
2	2		1	50				1	\N	702871491
3	3		1	52				1	\N	1062759651
4	4		m	53				1	\N	\N
5	5		1	54				1	\N	100000074240587
6	6		1	55				1	\N	1077534707
7	7		1	56				1	\N	1436051371
8	8		1	57				1	\N	1676239881
9	9		1	58				1	\N	100001505924367
10	10		0	59				1	\N	1528519675
11	11		1	60				1	\N	37402681
12	12		1	61				1	\N	1345749750
\.


--
-- Data for Name: main_userprofile_followers; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_userprofile_followers (id, userprofile_id, user_id) FROM stdin;
\.


--
-- Name: main_userprofile_followers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_userprofile_followers_id_seq', 1, false);


--
-- Data for Name: main_userprofile_following; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_userprofile_following (id, userprofile_id, user_id) FROM stdin;
\.


--
-- Name: main_userprofile_following_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_userprofile_following_id_seq', 1, false);


--
-- Name: main_userprofile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_userprofile_id_seq', 12, true);


--
-- Data for Name: main_userprofile_invitations; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_userprofile_invitations (id, userprofile_id, invitation_id) FROM stdin;
\.


--
-- Name: main_userprofile_invitations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_userprofile_invitations_id_seq', 1, false);


--
-- Data for Name: main_userprofile_watchlist; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_userprofile_watchlist (id, userprofile_id, activity_id) FROM stdin;
2	5	1
4	9	1
\.


--
-- Name: main_userprofile_watchlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_userprofile_watchlist_id_seq', 5, true);


--
-- Data for Name: main_video; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY main_video (id, unique_id, filename, video, key_data, upload_date, user_post_id, video_type) FROM stdin;
\.


--
-- Name: main_video_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('main_video_id_seq', 1, false);


--
-- Data for Name: socialaccount_socialaccount; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY socialaccount_socialaccount (id, user_id, last_login, date_joined, provider, uid, extra_data) FROM stdin;
2	3	2014-02-24 11:33:24.757644+00	2014-02-13 23:17:40.444567+00	facebook	1062759651	{"verified": true, "birthday": "12/17/1990", "locale": "en_US", "last_name": "Nguyen", "timezone": -5, "link": "https://www.facebook.com/Danny.Nguyen1712", "name": "Dang Nguyen", "updated_time": "2014-02-13T17:51:35+0000", "id": "1062759651", "education": [{"school": {"name": "Nguyen Du Secondary School", "id": "195758667108400"}, "type": "High School"}], "username": "Danny.Nguyen1712", "location": {"name": "Philadelphia, Pennsylvania", "id": "101881036520836"}, "email": "dangnguyen_1712@yahoo.com", "gender": "male", "first_name": "Dang"}
4	6	2014-02-15 14:21:33.955157+00	2014-02-15 14:21:33.955191+00	facebook	1077534707	{"verified": true, "id": "1077534707", "username": "knightus", "link": "https://www.facebook.com/knightus", "quotes": "Listen = Silent.", "birthday": "09/02/1991", "bio": "\\u00bf\\u0279\\u01dd\\u029el\\u0250\\u0287s 's\\u0131\\u0265\\u0287 \\u0183u\\u0131p\\u0250\\u01dd\\u0279 \\u029e\\u0254\\u01ddu \\u0279no\\u028e \\u029e\\u0250\\u01dd\\u0279q no\\u028e p\\u0131p", "last_name": "L\\u00ea Kh\\u00e1nh", "gender": "male", "timezone": 7, "first_name": "D\\u01b0\\u01a1ng", "updated_time": "2014-01-14T11:20:41+0000", "locale": "en_US", "email": "coldkiller131@gmail.com", "name": "L\\u00ea Kh\\u00e1nh D\\u01b0\\u01a1ng", "location": {"id": "106388046062960", "name": "Hanoi, Vietnam"}}
6	8	2014-02-15 17:18:23.382046+00	2014-02-15 17:18:23.382112+00	facebook	1676239881	{"verified": true, "id": "1676239881", "username": "tdz0110", "link": "https://www.facebook.com/tdz0110", "quotes": "\\"It's more than great, it's immense. Why? Because it dares.\\" - Victor Hugo, Les Mis\\u00e9rables\\n\\n\\"If you're not being born, you are busy dying\\" - Bob Dylan\\n\\n\\"Today Apple is gonna reinvent the phone\\" - Steve Jobs, 2007\\n\\n\\"You are already naked. There are no reason not to follow your heart\\" - Steve Jobs, 2005\\n\\n\\"But the painter of the future will be a colourist the like of which has never yet been seen\\" - Vincent van Gogh, May 1888", "birthday": "10/01/1992", "bio": "Eh! What? About me? I'd love you find out yourself!!! :D", "last_name": "V\\u0169", "gender": "male", "timezone": 7, "first_name": "Thanh T\\u00f9ng", "updated_time": "2014-01-13T09:59:59+0000", "hometown": {"id": "106388046062960", "name": "Hanoi, Vietnam"}, "locale": "en_US", "email": "tdz0110@gmail.com", "name": "V\\u0169 Thanh T\\u00f9ng", "location": {"id": "106388046062960", "name": "Hanoi, Vietnam"}, "education": [{"school": {"id": "103789649659423", "name": "Hanoi \\u2013 Amsterdam High School"}, "type": "High School", "year": {"id": "142963519060927", "name": "2010"}}, {"school": {"id": "111936798819016", "name": "Foreign Trade University"}, "type": "College", "year": {"id": "143641425651920", "name": "2014"}}]}
5	7	2014-02-23 09:50:40.019478+00	2014-02-15 17:18:09.763344+00	facebook	1436051371	{"name": "Tu Nguyen", "gender": "male", "updated_time": "2014-01-30T16:09:42+0000", "verified": true, "link": "https://www.facebook.com/de606b", "last_name": "Nguyen", "locale": "en_US", "email": "tunguyen243@gmail.com", "username": "de606b", "location": {"name": "Hanoi, Vietnam", "id": "106388046062960"}, "id": "1436051371", "timezone": 7, "birthday": "03/24/1992", "first_name": "Tu"}
7	9	2014-02-17 21:52:22.985251+00	2014-02-17 21:52:22.985322+00	facebook	100001505924367	{"id": "100001505924367", "verified": true, "timezone": 1, "email": "hachicha.taya@yahoo.fr", "location": {"id": "110774245616525", "name": "Paris, France"}, "updated_time": "2013-12-24T22:46:44+0000", "first_name": "Mohamed Taya", "birthday": "02/15/1990", "name": "Mohamed Taya Hachicha", "username": "adabou", "gender": "male", "last_name": "Hachicha", "link": "https://www.facebook.com/adabou", "locale": "fr_FR", "hometown": {"id": "106540302714545", "name": "Msaken"}, "education": [{"type": "High School", "school": {"id": "114645571886200", "name": "Lyc\\u00e9e Pilote de Sousse"}}, {"type": "College", "concentration": [{"id": "104076956295773", "name": "Computer Science"}], "school": {"id": "186291828074120", "name": "Drexel University"}}, {"type": "Graduate School", "year": {"id": "115222815248992", "name": "2012"}, "school": {"id": "106024132771717", "name": "\\u00c9cole nationale sup\\u00e9rieure d'informatique et de math\\u00e9matiques appliqu\\u00e9es de Grenoble"}}]}
8	10	2014-02-18 01:25:52.475402+00	2014-02-18 01:25:52.475464+00	facebook	1528519675	{"id": "1528519675", "verified": true, "quotes": "\\"We've all got both light and dark inside us. What matters is the part we choose to act on. That's who we really are.\\"--Sirius Black, Harry Potter and the Order of the Phoenix, J.K. Rowling\\n\\n\\"That's how knowledge works. It builds up, like compound interest.\\" --Warren Buffett", "email": "vuongthanhhue@gmail.com", "location": {"id": "101881036520836", "name": "Philadelphia, Pennsylvania"}, "updated_time": "2014-01-08T21:26:32+0000", "first_name": "Hue", "birthday": "12/16/1992", "name": "Hue Vuong", "languages": [{"id": "104059856296458", "name": "Vietnamese"}, {"id": "106059522759137", "name": "English"}, {"id": "108106272550772", "name": "French"}], "gender": "female", "last_name": "Vuong", "link": "https://www.facebook.com/huetvuong", "username": "huetvuong", "bio": "I love pigging out omnomnom", "locale": "en_US", "hometown": {"id": "106388046062960", "name": "Hanoi, Vietnam"}, "timezone": -5}
9	11	2014-02-18 22:08:18.161581+00	2014-02-18 22:08:18.161626+00	facebook	37402681	{"birthday": "12/19/1986", "bio": "something about myself", "last_name": "Tuleu", "gender": "male", "username": "etuleu", "id": "37402681", "languages": [{"name": "Romanian", "id": "103144026392979"}, {"name": "American English", "id": "103999719637010"}, {"name": "Crimean Tatar", "id": "108368109188554"}], "timezone": -8, "verified": true, "email": "etuleu@gmail.com", "quotes": "\\"I like rusty spoons\\"", "location": {"name": "Palo Alto, California", "id": "104022926303756"}, "locale": "en_US", "updated_time": "2013-09-14T06:12:53+0000", "name": "Erdal Tuleu", "link": "https://www.facebook.com/etuleu", "first_name": "Erdal"}
3	5	2014-02-22 05:05:59.689768+00	2014-02-14 04:19:53.059389+00	facebook	100000074240587	{"education": [{"school": {"name": "Hanoi \\u2013 Amsterdam High School", "id": "103789649659423"}, "type": "High School", "classes": [{"with": [{"name": "Minh Do Quang", "id": "100000079523564"}], "name": "H1 09-12", "id": "189100357832678"}], "year": {"name": "2012", "id": "115222815248992"}}, {"school": {"name": "Ti\\u1ec3u h\\u1ecdc Th\\u00e0nh C\\u00f4ng B", "id": "207475262765073"}, "type": "High School", "classes": [{"name": "A (00-05)", "id": "347479521991414"}], "year": {"name": "2005", "id": "138383069535219"}}, {"school": {"name": "Dickinson College", "id": "6628797891"}, "type": "College", "year": {"name": "2016", "id": "127342053975510"}}], "favorite_athletes": [{"name": "B\\u00e9b\\u00e9", "id": "102246133168756"}], "id": "100000074240587", "gender": "male", "timezone": -5, "birthday": "01/29/1994", "hometown": {"name": "Hanoi, Vietnam", "id": "106388046062960"}, "first_name": "Le Kinh", "location": {"name": "Carlisle, Pennsylvania", "id": "103757346329850"}, "verified": true, "name": "Le Kinh Hieu", "bio": "honestly...\\r\\n\\r\\nb\\u00ecnh  th\\u01b0\\u1eddng \\u0111\\u1ebfn m\\u1ee9c kh\\u00f4ng b\\u00ecnh th\\u01b0\\u1eddng =))", "link": "https://www.facebook.com/lekinh.hieu", "updated_time": "2014-02-20T04:07:07+0000", "username": "lekinh.hieu", "locale": "en_US", "last_name": "Hieu", "email": "batman_begin_a10@yahoo.com"}
10	12	2014-02-22 17:46:03.86703+00	2014-02-22 17:46:03.867071+00	facebook	1345749750	{"id": "1345749750", "gender": "male", "timezone": -5, "birthday": "02/22/1989", "first_name": "Quoc", "verified": true, "name": "Quoc Le", "updated_time": "2014-02-19T01:37:30+0000", "link": "https://www.facebook.com/baobao32", "username": "baobao32", "locale": "en_US", "last_name": "Le", "email": "kinhquoc89@gmail.com"}
1	2	2014-02-24 19:17:36.971255+00	2014-02-13 22:51:29.726086+00	facebook	702871491	{"verified": true, "birthday": "03/11/1992", "favorite_athletes": [{"name": "Nguyen Tien Minh", "id": "115078845171286"}, {"name": "Roger Federer", "id": "64760994940"}, {"name": "Leo Messi", "id": "176063032413299"}], "last_name": "Vu", "timezone": -5, "link": "https://www.facebook.com/vuthanhduc", "name": "Duc Vu", "email": "vuthanhduc92@yahoo.com", "updated_time": "2014-01-27T19:50:26+0000", "id": "702871491", "languages": [{"name": "Vietnamese", "id": "104059856296458"}, {"name": "English", "id": "106059522759137"}], "quotes": "broaden our world view, keeping the country at the heart", "education": [{"with": [{"name": "Chu Tu\\u1ea5n Nam", "id": "1597434381"}], "school": {"name": "THCS nguy\\u1ec5n tr\\u01b0\\u1eddng t\\u1ed9", "id": "137465306291448"}, "type": "High School", "year": {"name": "2007", "id": "140617569303679"}}, {"with": [{"name": "T\\u1ef1 Tin Qu\\u00e1 \\u0110\\u00e1ng", "id": "100000843850124"}], "school": {"name": "Hanoi \\u2013 Amsterdam High School", "id": "103789649659423"}, "type": "High School", "classes": [{"with": [{"name": "Tr\\u00ed L\\u00ea", "id": "100000066245417"}], "name": "IT 07-10", "from": {"name": "Tr\\u00ed L\\u00ea", "id": "100000066245417"}, "id": "183250528383247"}], "year": {"name": "2010", "id": "142963519060927"}}], "locale": "en_US", "hometown": {"name": "Hanoi, Vietnam", "id": "106388046062960"}, "bio": "~~~ I have a dream for vietnam :)\\n\\nI used to want to have the whole world, but now I know the world is just you\\n\\ndecoder of autumn\\n\\ncrazy fan of summer's night\\n\\n\\u95ee\\u5fc3\\u65e0\\u6127 \\u968f\\u5fc3\\u6240\\u6b32 -- No regrets, follow your heart\\n\\n\\"The secret to creativity is knowing how to hide your sources.\\"\\n\\nmaster of lim will touch master of love in one bridge", "username": "vuthanhduc", "gender": "male", "favorite_teams": [{"name": "FC Barcelona", "id": "197394889304"}, {"name": "Barcelona", "id": "48389720371"}], "first_name": "Duc", "location": {"name": "Philadelphia, Pennsylvania", "id": "101881036520836"}}
\.


--
-- Name: socialaccount_socialaccount_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('socialaccount_socialaccount_id_seq', 10, true);


--
-- Data for Name: socialaccount_socialapp; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY socialaccount_socialapp (id, provider, name, key, secret, client_id) FROM stdin;
1	facebook	Frittie Facebook App		6affb16d935e406397b5c68a413141f5	265377323561995
\.


--
-- Name: socialaccount_socialapp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('socialaccount_socialapp_id_seq', 1, true);


--
-- Data for Name: socialaccount_socialapp_sites; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY socialaccount_socialapp_sites (id, socialapp_id, site_id) FROM stdin;
1	1	1
\.


--
-- Name: socialaccount_socialapp_sites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('socialaccount_socialapp_sites_id_seq', 1, true);


--
-- Data for Name: socialaccount_socialtoken; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY socialaccount_socialtoken (id, app_id, account_id, token, token_secret, expires_at) FROM stdin;
4	1	4	CAADxWZCkaNAsBAM9BudZBNwGx5kTg94rWNl0WA8qqOj4pANZB0PfEFiOnZCg1S1kif7qztGzPZASLSJDIGZByPhTyZAj30vpvulwYogWCEUk2Q0xREyOcqeTmZBxEr81yRL27NMQ9zfZBYPJf85K8puDBR0t2feHQ5vbFDlIj8gGuNMFWla6jA0bknDdtnX3WGvsRutrTDaPtZBQZDZD		\N
6	1	6	CAADxWZCkaNAsBALY2pNtEa4Sskhsk1chE25PJ7SPHFadW2spiCRWoYmyVwMsyZAk0fbrPJ2AZBdXjtsoRSSQDTTQpQZCUPlxBhmXgq9u6ZBbeUqYoqrS6gu0H91IrZAGWPBWanC3GxlTG4fNWetUkyq8mr36cZBRB2ZB7ZCjJ5V1oIzQyaKLEZCZBEZAdJMSnlfHdOgZD		\N
7	1	7	CAADxWZCkaNAsBAOQdZBDOZAKaGtudUKaMs53qKX4tKN6C8Lf9NH2CX6HZA4Mc9lolmCcCzKNf15Jzarihuq5FztrorEN6hnszZBpyIVPu3GuX5hslfjFNZAa2XPhAtMbKQtUXUogH9TxYRlZBPVl6ToffMDv2ocZCzZArIywfHU2v0Hts6d8gUfZCZClKZBypwbglgD9IxijLL9xTwZDZD		\N
8	1	8	CAADxWZCkaNAsBAJw7C0qrZAVn1ULXJ76CSJLZANs5jTzjsDG7qneM2ZCaFzojKZBYzaVqMO0LyglALFSRmOiZBwwoU7M04wSZBv7s873wuh86SaJvKWayjEd3b6FA4yc9FaMrfErVS9mLi7sh04UqZAAobxg1dqylpxOw4a0skzvR80bnzB9F8mFbf7QcSrJnJUZD		\N
9	1	9	CAADxWZCkaNAsBAJv7rqCNzMt3r4OpVtJ0zTFtbiougd7tHqtmMYQei0vZB1U1BSLAyGZCuHmdBQXBxhO5ZBfRUufZBBaRm7Cic0znKHJ1sI8x1cYV2tPfJ6jVaZB47MRib4UFvGr2B0E9K0BOjWJBjCezlmSUZBbJpEAZBZB5d2NDCeV3k9ZB5KNfanUHdZCOD1VpsZD		\N
3	1	3	CAADxWZCkaNAsBAFCjWaaYhqaZBaXUdOHGgr1MZBAdKXcIkTEJIHe0LYEY7YSWHVviZCnBQcvjMqkFu9vtF1IbJmfklVEA3MpUvG78GcPAULGjAumm35DabyL1KPxonIva5gZBirUNbZAa7XUy8vnwdwD2QieQTTHnSiD9qUZCCAMcTNdSbDUP9lwdsJzm1cV8NydDuLqL7WaQZDZD		\N
10	1	10	CAADxWZCkaNAsBAHaJzZB7MeloXdlZBblA6dlXbsl4owYv0oTvLseDawUeWYr0CBYf2nnUPIZBDsbZAjv2iOHaidHAxJVzVXCpD9pwHifXzbZCZBD6ZBFLWFEZCKzRAlAgp1e9GgEvlNALTVQ3QbeZCZBhxYcU2ogbEX6ZBfDzCubsEZBBomQi8xchJMnsYWRK88gCUsCsbhUaVRSlqgZDZD		\N
5	1	5	CAADxWZCkaNAsBAPSFqEZAw2QAPYKVs3SPgRLcqBHUbrDSvgpZAmndaWnKxylVozCeZC99ZBDvqPZBpnbrelRPJgsxcSWHGzO1FGZBHm3ZCKZBMF9xDDz1CMv3jGc1InpZAbRuKfZCdtyTvulO8UPJW8NzOjMUgBRiv8uaAqmwSvvk7IVwsDH739BZBwsJqmnfZBfFjjMZD		\N
2	1	2	CAADxWZCkaNAsBAG8rfel3lr4YTcuWJgAjpJMCeLuoJiuQIIBqXjZBhDiDMfghsUlpsGsBgGEDbb8IwhF76Il3FZCItZAR9Dmi0zhHcD8zB3qtfsbzQpgZAK09jZAFgDNB6wZAVqAIWrl5k8sPDRXcB7McmVMZCiKiWOALZCZBHLezVbIaR3kyrwfHw1Nyqeo9UtbjW9qHJ8BZBRPQZDZD		\N
1	1	1	CAADxWZCkaNAsBAHwxHn8fxlifVerQAEmEbXXxdgvhFU7wve8xYroAbpHMzCHrZCOo5i3bLvROzfwdV9qZBRAWeNsTjdJZCrn4XQSJTMp6z1DTy9A5GeRysJRIZAZBeZA81NZCiwbZAG91x7i3ZBPNs0f3Yky2FKuwYV9GVHftXapTP5oc3rlFLARJ3pIiFMVsTgPgZD		\N
\.


--
-- Name: socialaccount_socialtoken_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('socialaccount_socialtoken_id_seq', 10, true);


--
-- Data for Name: south_migrationhistory; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY south_migrationhistory (id, app_name, migration, applied) FROM stdin;
1	main	0001_initial	2014-02-13 22:45:37.184921+00
2	socialaccount	0001_initial	2014-02-13 22:45:38.985079+00
3	socialaccount	0002_genericmodels	2014-02-13 22:45:39.263802+00
4	facebook	0001_initial	2014-02-13 22:45:39.562835+00
5	facebook	0002_auto__add_facebookaccesstoken__add_unique_facebookaccesstoken_app_acco	2014-02-13 22:45:39.804055+00
6	facebook	0003_tosocialaccount	2014-02-13 22:45:39.87392+00
7	socialaccount	0003_auto__add_unique_socialaccount_uid_provider	2014-02-13 22:45:39.916685+00
8	socialaccount	0004_add_sites	2014-02-13 22:45:39.969408+00
9	socialaccount	0005_set_sites	2014-02-13 22:45:40.0083+00
10	socialaccount	0006_auto__del_field_socialapp_site	2014-02-13 22:45:40.042229+00
11	socialaccount	0007_auto__add_field_socialapp_client_id	2014-02-13 22:45:40.092292+00
12	socialaccount	0008_client_id	2014-02-13 22:45:40.152829+00
13	socialaccount	0009_auto__add_field_socialtoken_expires_at	2014-02-13 22:45:40.180401+00
14	socialaccount	0010_auto__chg_field_socialtoken_token	2014-02-13 22:45:40.250866+00
15	socialaccount	0011_auto__chg_field_socialtoken_token	2014-02-13 22:45:40.385998+00
16	facebook	0004_auto__del_facebookapp__del_facebookaccesstoken__del_unique_facebookacc	2014-02-13 22:45:41.969737+00
17	tastypie	0001_initial	2014-02-13 22:45:43.100597+00
18	tastypie	0002_add_apikey_index	2014-02-13 22:45:43.187257+00
19	djcelery	0001_initial	2014-02-13 22:45:44.478951+00
20	djcelery	0002_v25_changes	2014-02-13 22:45:44.637323+00
21	djcelery	0003_v26_changes	2014-02-13 22:45:44.978646+00
22	djcelery	0004_v30_changes	2014-02-13 22:45:45.066869+00
29	main	0002_auto	2014-02-24 22:34:03.171359+00
\.


--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('south_migrationhistory_id_seq', 29, true);


--
-- Data for Name: tastypie_apiaccess; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY tastypie_apiaccess (id, identifier, url, request_method, accessed) FROM stdin;
\.


--
-- Name: tastypie_apiaccess_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('tastypie_apiaccess_id_seq', 1, false);


--
-- Data for Name: tastypie_apikey; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY tastypie_apikey (id, user_id, key, created) FROM stdin;
1	2	8006f57e74011dc8a1f784b321e169552b60f06d	2014-02-13 22:51:28.920996+00
2	3	307bb93d8bbacdc865dfdd57a3fbcb73ab14e0cc	2014-02-13 23:17:39.598277+00
3	4	4874c93ce1e82048543731f87bbf24069f8e9341	2014-02-13 23:49:29.144277+00
4	5	400a86b497a00a58abb23d354b426ab9b451e992	2014-02-14 04:19:51.602784+00
5	6	cd60fc62cfe74fbd21c9dda46d9023e837f180d2	2014-02-15 14:21:33.15796+00
6	7	9690031180c3dccc9e30dc25d23d83f7a7f2c4d6	2014-02-15 17:18:08.943772+00
7	8	ed8e27cc2769c79a15f7674564e4f00050a63a3b	2014-02-15 17:18:22.563894+00
8	9	abfe312dc5d0035d48e3a292b08f80c8336ae4bd	2014-02-17 21:52:21.971147+00
9	10	9bcb324b546eb7c183fa4298abee1f228cb3b41f	2014-02-18 01:25:51.407043+00
10	11	2f4a5e1274ef95599c321bb3f3b66582c3df6fe6	2014-02-18 22:08:17.266258+00
11	12	6198e68725a5538836754e016d217d044130f7bb	2014-02-22 17:46:02.890651+00
\.


--
-- Name: tastypie_apikey_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('tastypie_apikey_id_seq', 11, true);


--
-- Data for Name: user_streams_single_table_backend_streamitem; Type: TABLE DATA; Schema: public; Owner: bobzovgeshjqfj
--

COPY user_streams_single_table_backend_streamitem (id, user_id, content, created_at) FROM stdin;
\.


--
-- Name: user_streams_single_table_backend_streamitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bobzovgeshjqfj
--

SELECT pg_catalog.setval('user_streams_single_table_backend_streamitem_id_seq', 1, false);


--
-- Name: account_emailaddress_email_key; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY account_emailaddress
    ADD CONSTRAINT account_emailaddress_email_key UNIQUE (email);


--
-- Name: account_emailaddress_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY account_emailaddress
    ADD CONSTRAINT account_emailaddress_pkey PRIMARY KEY (id);


--
-- Name: account_emailconfirmation_key_key; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY account_emailconfirmation
    ADD CONSTRAINT account_emailconfirmation_key_key UNIQUE (key);


--
-- Name: account_emailconfirmation_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY account_emailconfirmation
    ADD CONSTRAINT account_emailconfirmation_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_key UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_key; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_key UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_group_id_key; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_key UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_key UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: celery_taskmeta_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY celery_taskmeta
    ADD CONSTRAINT celery_taskmeta_pkey PRIMARY KEY (id);


--
-- Name: celery_taskmeta_task_id_key; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY celery_taskmeta
    ADD CONSTRAINT celery_taskmeta_task_id_key UNIQUE (task_id);


--
-- Name: celery_tasksetmeta_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY celery_tasksetmeta
    ADD CONSTRAINT celery_tasksetmeta_pkey PRIMARY KEY (id);


--
-- Name: celery_tasksetmeta_taskset_id_key; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY celery_tasksetmeta
    ADD CONSTRAINT celery_tasksetmeta_taskset_id_key UNIQUE (taskset_id);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_model_key; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_key UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: djcelery_crontabschedule_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY djcelery_crontabschedule
    ADD CONSTRAINT djcelery_crontabschedule_pkey PRIMARY KEY (id);


--
-- Name: djcelery_intervalschedule_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY djcelery_intervalschedule
    ADD CONSTRAINT djcelery_intervalschedule_pkey PRIMARY KEY (id);


--
-- Name: djcelery_periodictask_name_key; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY djcelery_periodictask
    ADD CONSTRAINT djcelery_periodictask_name_key UNIQUE (name);


--
-- Name: djcelery_periodictask_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY djcelery_periodictask
    ADD CONSTRAINT djcelery_periodictask_pkey PRIMARY KEY (id);


--
-- Name: djcelery_periodictasks_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY djcelery_periodictasks
    ADD CONSTRAINT djcelery_periodictasks_pkey PRIMARY KEY (ident);


--
-- Name: djcelery_taskstate_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY djcelery_taskstate
    ADD CONSTRAINT djcelery_taskstate_pkey PRIMARY KEY (id);


--
-- Name: djcelery_taskstate_task_id_key; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY djcelery_taskstate
    ADD CONSTRAINT djcelery_taskstate_task_id_key UNIQUE (task_id);


--
-- Name: djcelery_workerstate_hostname_key; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY djcelery_workerstate
    ADD CONSTRAINT djcelery_workerstate_hostname_key UNIQUE (hostname);


--
-- Name: djcelery_workerstate_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY djcelery_workerstate
    ADD CONSTRAINT djcelery_workerstate_pkey PRIMARY KEY (id);


--
-- Name: main_activity_activity_request_activity_id_7de50ca315b8485_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_activity_activity_request
    ADD CONSTRAINT main_activity_activity_request_activity_id_7de50ca315b8485_uniq UNIQUE (activity_id, activityrequest_id);


--
-- Name: main_activity_activity_request_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_activity_activity_request
    ADD CONSTRAINT main_activity_activity_request_pkey PRIMARY KEY (id);


--
-- Name: main_activity_comments_activity_id_c3c19ee75be2143_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_activity_comments
    ADD CONSTRAINT main_activity_comments_activity_id_c3c19ee75be2143_uniq UNIQUE (activity_id, comment_id);


--
-- Name: main_activity_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_activity_comments
    ADD CONSTRAINT main_activity_comments_pkey PRIMARY KEY (id);


--
-- Name: main_activity_list_photos_activity_id_47425dde741b3f39_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_activity_list_photos
    ADD CONSTRAINT main_activity_list_photos_activity_id_47425dde741b3f39_uniq UNIQUE (activity_id, photo_id);


--
-- Name: main_activity_list_photos_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_activity_list_photos
    ADD CONSTRAINT main_activity_list_photos_pkey PRIMARY KEY (id);


--
-- Name: main_activity_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_activity
    ADD CONSTRAINT main_activity_pkey PRIMARY KEY (id);


--
-- Name: main_activity_posts_activity_id_23ed66c3614a6478_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_activity_posts
    ADD CONSTRAINT main_activity_posts_activity_id_23ed66c3614a6478_uniq UNIQUE (activity_id, notification_id);


--
-- Name: main_activity_posts_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_activity_posts
    ADD CONSTRAINT main_activity_posts_pkey PRIMARY KEY (id);


--
-- Name: main_activity_reports_activity_id_66bc3187562e937b_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_activity_reports
    ADD CONSTRAINT main_activity_reports_activity_id_66bc3187562e937b_uniq UNIQUE (activity_id, report_id);


--
-- Name: main_activity_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_activity_reports
    ADD CONSTRAINT main_activity_reports_pkey PRIMARY KEY (id);


--
-- Name: main_activity_tags_activity_id_6a901b0336e95f5d_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_activity_tags
    ADD CONSTRAINT main_activity_tags_activity_id_6a901b0336e95f5d_uniq UNIQUE (activity_id, tag_id);


--
-- Name: main_activity_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_activity_tags
    ADD CONSTRAINT main_activity_tags_pkey PRIMARY KEY (id);


--
-- Name: main_activity_ticket_activity_id_749030123b942651_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_activity_ticket
    ADD CONSTRAINT main_activity_ticket_activity_id_749030123b942651_uniq UNIQUE (activity_id, ticket_id);


--
-- Name: main_activity_ticket_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_activity_ticket
    ADD CONSTRAINT main_activity_ticket_pkey PRIMARY KEY (id);


--
-- Name: main_activityrequest_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_activityrequest
    ADD CONSTRAINT main_activityrequest_pkey PRIMARY KEY (id);


--
-- Name: main_activityuserrelation_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_activityuserrelation
    ADD CONSTRAINT main_activityuserrelation_pkey PRIMARY KEY (id);


--
-- Name: main_comment_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_comment
    ADD CONSTRAINT main_comment_pkey PRIMARY KEY (id);


--
-- Name: main_comment_reports_comment_id_16bbad368db6cf1_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_comment_reports
    ADD CONSTRAINT main_comment_reports_comment_id_16bbad368db6cf1_uniq UNIQUE (comment_id, report_id);


--
-- Name: main_comment_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_comment_reports
    ADD CONSTRAINT main_comment_reports_pkey PRIMARY KEY (id);


--
-- Name: main_conversation_messages_conversation_id_fdd38ae9671999b_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_conversation_messages
    ADD CONSTRAINT main_conversation_messages_conversation_id_fdd38ae9671999b_uniq UNIQUE (conversation_id, message_id);


--
-- Name: main_conversation_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_conversation_messages
    ADD CONSTRAINT main_conversation_messages_pkey PRIMARY KEY (id);


--
-- Name: main_conversation_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_conversation
    ADD CONSTRAINT main_conversation_pkey PRIMARY KEY (id);


--
-- Name: main_feed_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_feed
    ADD CONSTRAINT main_feed_pkey PRIMARY KEY (id);


--
-- Name: main_feed_user_receive_feed_id_7c2574bb2c6da315_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_feed_user_receive
    ADD CONSTRAINT main_feed_user_receive_feed_id_7c2574bb2c6da315_uniq UNIQUE (feed_id, user_id);


--
-- Name: main_feed_user_receive_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_feed_user_receive
    ADD CONSTRAINT main_feed_user_receive_pkey PRIMARY KEY (id);


--
-- Name: main_frit_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_frit
    ADD CONSTRAINT main_frit_pkey PRIMARY KEY (id);


--
-- Name: main_invitation_list_photos_invitation_id_2c81e0f52e58c961_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_invitation_list_photos
    ADD CONSTRAINT main_invitation_list_photos_invitation_id_2c81e0f52e58c961_uniq UNIQUE (invitation_id, photo_id);


--
-- Name: main_invitation_list_photos_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_invitation_list_photos
    ADD CONSTRAINT main_invitation_list_photos_pkey PRIMARY KEY (id);


--
-- Name: main_invitation_list_videos_invitation_id_39db3497bfee0963_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_invitation_list_videos
    ADD CONSTRAINT main_invitation_list_videos_invitation_id_39db3497bfee0963_uniq UNIQUE (invitation_id, video_id);


--
-- Name: main_invitation_list_videos_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_invitation_list_videos
    ADD CONSTRAINT main_invitation_list_videos_pkey PRIMARY KEY (id);


--
-- Name: main_invitation_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_invitation
    ADD CONSTRAINT main_invitation_pkey PRIMARY KEY (id);


--
-- Name: main_invitation_send_to_invitation_id_581e69bccafaa1e3_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_invitation_send_to
    ADD CONSTRAINT main_invitation_send_to_invitation_id_581e69bccafaa1e3_uniq UNIQUE (invitation_id, user_id);


--
-- Name: main_invitation_send_to_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_invitation_send_to
    ADD CONSTRAINT main_invitation_send_to_pkey PRIMARY KEY (id);


--
-- Name: main_location_comments_location_id_5b57807b3bba1a7f_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_location_comments
    ADD CONSTRAINT main_location_comments_location_id_5b57807b3bba1a7f_uniq UNIQUE (location_id, comment_id);


--
-- Name: main_location_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_location_comments
    ADD CONSTRAINT main_location_comments_pkey PRIMARY KEY (id);


--
-- Name: main_location_follow_by_location_id_7d575f63c59d38d0_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_location_follow_by
    ADD CONSTRAINT main_location_follow_by_location_id_7d575f63c59d38d0_uniq UNIQUE (location_id, user_id);


--
-- Name: main_location_follow_by_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_location_follow_by
    ADD CONSTRAINT main_location_follow_by_pkey PRIMARY KEY (id);


--
-- Name: main_location_list_photos_location_id_79d80723bd6aa6c3_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_location_list_photos
    ADD CONSTRAINT main_location_list_photos_location_id_79d80723bd6aa6c3_uniq UNIQUE (location_id, photo_id);


--
-- Name: main_location_list_photos_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_location_list_photos
    ADD CONSTRAINT main_location_list_photos_pkey PRIMARY KEY (id);


--
-- Name: main_location_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_location
    ADD CONSTRAINT main_location_pkey PRIMARY KEY (id);


--
-- Name: main_location_posts_location_id_7c712ca1c6480790_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_location_posts
    ADD CONSTRAINT main_location_posts_location_id_7c712ca1c6480790_uniq UNIQUE (location_id, notification_id);


--
-- Name: main_location_posts_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_location_posts
    ADD CONSTRAINT main_location_posts_pkey PRIMARY KEY (id);


--
-- Name: main_location_reports_location_id_34ed52f5f2a7c1a3_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_location_reports
    ADD CONSTRAINT main_location_reports_location_id_34ed52f5f2a7c1a3_uniq UNIQUE (location_id, report_id);


--
-- Name: main_location_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_location_reports
    ADD CONSTRAINT main_location_reports_pkey PRIMARY KEY (id);


--
-- Name: main_message_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_message
    ADD CONSTRAINT main_message_pkey PRIMARY KEY (id);


--
-- Name: main_notification_notify__notification_id_70dc8f87b02f9ff8_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_notification_notify_to
    ADD CONSTRAINT main_notification_notify__notification_id_70dc8f87b02f9ff8_uniq UNIQUE (notification_id, user_id);


--
-- Name: main_notification_notify_to_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_notification_notify_to
    ADD CONSTRAINT main_notification_notify_to_pkey PRIMARY KEY (id);


--
-- Name: main_notification_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_notification
    ADD CONSTRAINT main_notification_pkey PRIMARY KEY (id);


--
-- Name: main_photo_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_photo
    ADD CONSTRAINT main_photo_pkey PRIMARY KEY (id);


--
-- Name: main_report_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_report
    ADD CONSTRAINT main_report_pkey PRIMARY KEY (id);


--
-- Name: main_siteglobaldata_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_siteglobaldata
    ADD CONSTRAINT main_siteglobaldata_pkey PRIMARY KEY (id);


--
-- Name: main_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_tag
    ADD CONSTRAINT main_tag_pkey PRIMARY KEY (id);


--
-- Name: main_ticket_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_ticket
    ADD CONSTRAINT main_ticket_pkey PRIMARY KEY (id);


--
-- Name: main_tickettransaction_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_tickettransaction
    ADD CONSTRAINT main_tickettransaction_pkey PRIMARY KEY (id);


--
-- Name: main_userprofile_followers_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_userprofile_followers
    ADD CONSTRAINT main_userprofile_followers_pkey PRIMARY KEY (id);


--
-- Name: main_userprofile_followers_userprofile_id_664144a348631526_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_userprofile_followers
    ADD CONSTRAINT main_userprofile_followers_userprofile_id_664144a348631526_uniq UNIQUE (userprofile_id, user_id);


--
-- Name: main_userprofile_following_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_userprofile_following
    ADD CONSTRAINT main_userprofile_following_pkey PRIMARY KEY (id);


--
-- Name: main_userprofile_following_userprofile_id_6f492908290f376e_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_userprofile_following
    ADD CONSTRAINT main_userprofile_following_userprofile_id_6f492908290f376e_uniq UNIQUE (userprofile_id, user_id);


--
-- Name: main_userprofile_invitatio_userprofile_id_54ed973731bba193_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_userprofile_invitations
    ADD CONSTRAINT main_userprofile_invitatio_userprofile_id_54ed973731bba193_uniq UNIQUE (userprofile_id, invitation_id);


--
-- Name: main_userprofile_invitations_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_userprofile_invitations
    ADD CONSTRAINT main_userprofile_invitations_pkey PRIMARY KEY (id);


--
-- Name: main_userprofile_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_userprofile
    ADD CONSTRAINT main_userprofile_pkey PRIMARY KEY (id);


--
-- Name: main_userprofile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_userprofile
    ADD CONSTRAINT main_userprofile_user_id_key UNIQUE (user_id);


--
-- Name: main_userprofile_watchlist_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_userprofile_watchlist
    ADD CONSTRAINT main_userprofile_watchlist_pkey PRIMARY KEY (id);


--
-- Name: main_userprofile_watchlist_userprofile_id_7e2b99621ff62284_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_userprofile_watchlist
    ADD CONSTRAINT main_userprofile_watchlist_userprofile_id_7e2b99621ff62284_uniq UNIQUE (userprofile_id, activity_id);


--
-- Name: main_video_key_data_key; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_video
    ADD CONSTRAINT main_video_key_data_key UNIQUE (key_data);


--
-- Name: main_video_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY main_video
    ADD CONSTRAINT main_video_pkey PRIMARY KEY (id);


--
-- Name: socialaccount_socialaccount_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY socialaccount_socialaccount
    ADD CONSTRAINT socialaccount_socialaccount_pkey PRIMARY KEY (id);


--
-- Name: socialaccount_socialaccount_uid_725a93fcbb7e2d60_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY socialaccount_socialaccount
    ADD CONSTRAINT socialaccount_socialaccount_uid_725a93fcbb7e2d60_uniq UNIQUE (uid, provider);


--
-- Name: socialaccount_socialapp_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY socialaccount_socialapp
    ADD CONSTRAINT socialaccount_socialapp_pkey PRIMARY KEY (id);


--
-- Name: socialaccount_socialapp_site_socialapp_id_59392f1211cf5449_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY socialaccount_socialapp_sites
    ADD CONSTRAINT socialaccount_socialapp_site_socialapp_id_59392f1211cf5449_uniq UNIQUE (socialapp_id, site_id);


--
-- Name: socialaccount_socialapp_sites_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY socialaccount_socialapp_sites
    ADD CONSTRAINT socialaccount_socialapp_sites_pkey PRIMARY KEY (id);


--
-- Name: socialaccount_socialtoken_app_id_4233f2c819ee7f30_uniq; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_socialtoken_app_id_4233f2c819ee7f30_uniq UNIQUE (app_id, account_id);


--
-- Name: socialaccount_socialtoken_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_socialtoken_pkey PRIMARY KEY (id);


--
-- Name: south_migrationhistory_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY south_migrationhistory
    ADD CONSTRAINT south_migrationhistory_pkey PRIMARY KEY (id);


--
-- Name: tastypie_apiaccess_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY tastypie_apiaccess
    ADD CONSTRAINT tastypie_apiaccess_pkey PRIMARY KEY (id);


--
-- Name: tastypie_apikey_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY tastypie_apikey
    ADD CONSTRAINT tastypie_apikey_pkey PRIMARY KEY (id);


--
-- Name: tastypie_apikey_user_id_key; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY tastypie_apikey
    ADD CONSTRAINT tastypie_apikey_user_id_key UNIQUE (user_id);


--
-- Name: user_streams_single_table_backend_streamitem_pkey; Type: CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

ALTER TABLE ONLY user_streams_single_table_backend_streamitem
    ADD CONSTRAINT user_streams_single_table_backend_streamitem_pkey PRIMARY KEY (id);


--
-- Name: celery_taskmeta_hidden; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX celery_taskmeta_hidden ON celery_taskmeta USING btree (hidden);


--
-- Name: celery_taskmeta_task_id_like; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX celery_taskmeta_task_id_like ON celery_taskmeta USING btree (task_id varchar_pattern_ops);


--
-- Name: celery_tasksetmeta_hidden; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX celery_tasksetmeta_hidden ON celery_tasksetmeta USING btree (hidden);


--
-- Name: celery_tasksetmeta_taskset_id_like; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX celery_tasksetmeta_taskset_id_like ON celery_tasksetmeta USING btree (taskset_id varchar_pattern_ops);


--
-- Name: djcelery_periodictask_crontab_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX djcelery_periodictask_crontab_id ON djcelery_periodictask USING btree (crontab_id);


--
-- Name: djcelery_periodictask_interval_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX djcelery_periodictask_interval_id ON djcelery_periodictask USING btree (interval_id);


--
-- Name: djcelery_periodictask_name_like; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX djcelery_periodictask_name_like ON djcelery_periodictask USING btree (name varchar_pattern_ops);


--
-- Name: djcelery_taskstate_hidden; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX djcelery_taskstate_hidden ON djcelery_taskstate USING btree (hidden);


--
-- Name: djcelery_taskstate_name; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX djcelery_taskstate_name ON djcelery_taskstate USING btree (name);


--
-- Name: djcelery_taskstate_name_like; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX djcelery_taskstate_name_like ON djcelery_taskstate USING btree (name varchar_pattern_ops);


--
-- Name: djcelery_taskstate_state; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX djcelery_taskstate_state ON djcelery_taskstate USING btree (state);


--
-- Name: djcelery_taskstate_state_like; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX djcelery_taskstate_state_like ON djcelery_taskstate USING btree (state varchar_pattern_ops);


--
-- Name: djcelery_taskstate_task_id_like; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX djcelery_taskstate_task_id_like ON djcelery_taskstate USING btree (task_id varchar_pattern_ops);


--
-- Name: djcelery_taskstate_tstamp; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX djcelery_taskstate_tstamp ON djcelery_taskstate USING btree (tstamp);


--
-- Name: djcelery_taskstate_worker_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX djcelery_taskstate_worker_id ON djcelery_taskstate USING btree (worker_id);


--
-- Name: djcelery_workerstate_hostname_like; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX djcelery_workerstate_hostname_like ON djcelery_workerstate USING btree (hostname varchar_pattern_ops);


--
-- Name: djcelery_workerstate_last_heartbeat; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX djcelery_workerstate_last_heartbeat ON djcelery_workerstate USING btree (last_heartbeat);


--
-- Name: main_activity_activity_request_activity_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activity_activity_request_activity_id ON main_activity_activity_request USING btree (activity_id);


--
-- Name: main_activity_activity_request_activityrequest_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activity_activity_request_activityrequest_id ON main_activity_activity_request USING btree (activityrequest_id);


--
-- Name: main_activity_comments_activity_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activity_comments_activity_id ON main_activity_comments USING btree (activity_id);


--
-- Name: main_activity_comments_comment_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activity_comments_comment_id ON main_activity_comments USING btree (comment_id);


--
-- Name: main_activity_list_photos_activity_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activity_list_photos_activity_id ON main_activity_list_photos USING btree (activity_id);


--
-- Name: main_activity_list_photos_photo_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activity_list_photos_photo_id ON main_activity_list_photos USING btree (photo_id);


--
-- Name: main_activity_location_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activity_location_id ON main_activity USING btree (location_id);


--
-- Name: main_activity_logo_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activity_logo_id ON main_activity USING btree (logo_id);


--
-- Name: main_activity_posts_activity_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activity_posts_activity_id ON main_activity_posts USING btree (activity_id);


--
-- Name: main_activity_posts_notification_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activity_posts_notification_id ON main_activity_posts USING btree (notification_id);


--
-- Name: main_activity_reports_activity_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activity_reports_activity_id ON main_activity_reports USING btree (activity_id);


--
-- Name: main_activity_reports_report_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activity_reports_report_id ON main_activity_reports USING btree (report_id);


--
-- Name: main_activity_tags_activity_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activity_tags_activity_id ON main_activity_tags USING btree (activity_id);


--
-- Name: main_activity_tags_tag_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activity_tags_tag_id ON main_activity_tags USING btree (tag_id);


--
-- Name: main_activity_ticket_activity_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activity_ticket_activity_id ON main_activity_ticket USING btree (activity_id);


--
-- Name: main_activity_ticket_ticket_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activity_ticket_ticket_id ON main_activity_ticket USING btree (ticket_id);


--
-- Name: main_activity_user_create_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activity_user_create_id ON main_activity USING btree (user_create_id);


--
-- Name: main_activityrequest_activity_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activityrequest_activity_id ON main_activityrequest USING btree (activity_id);


--
-- Name: main_activityrequest_user_request_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activityrequest_user_request_id ON main_activityrequest USING btree (user_request_id);


--
-- Name: main_activityuserrelation_activity_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activityuserrelation_activity_id ON main_activityuserrelation USING btree (activity_id);


--
-- Name: main_activityuserrelation_user_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_activityuserrelation_user_id ON main_activityuserrelation USING btree (user_id);


--
-- Name: main_comment_reports_comment_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_comment_reports_comment_id ON main_comment_reports USING btree (comment_id);


--
-- Name: main_comment_reports_report_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_comment_reports_report_id ON main_comment_reports USING btree (report_id);


--
-- Name: main_comment_user_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_comment_user_id ON main_comment USING btree (user_id);


--
-- Name: main_conversation_latest_message_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_conversation_latest_message_id ON main_conversation USING btree (latest_message_id);


--
-- Name: main_conversation_messages_conversation_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_conversation_messages_conversation_id ON main_conversation_messages USING btree (conversation_id);


--
-- Name: main_conversation_messages_message_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_conversation_messages_message_id ON main_conversation_messages USING btree (message_id);


--
-- Name: main_conversation_user1_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_conversation_user1_id ON main_conversation USING btree (user1_id);


--
-- Name: main_conversation_user2_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_conversation_user2_id ON main_conversation USING btree (user2_id);


--
-- Name: main_feed_create_by_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_feed_create_by_id ON main_feed USING btree (create_by_id);


--
-- Name: main_feed_user_receive_feed_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_feed_user_receive_feed_id ON main_feed_user_receive USING btree (feed_id);


--
-- Name: main_feed_user_receive_user_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_feed_user_receive_user_id ON main_feed_user_receive USING btree (user_id);


--
-- Name: main_frit_user_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_frit_user_id ON main_frit USING btree (user_id);


--
-- Name: main_invitation_activity_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_invitation_activity_id ON main_invitation USING btree (activity_id);


--
-- Name: main_invitation_list_photos_invitation_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_invitation_list_photos_invitation_id ON main_invitation_list_photos USING btree (invitation_id);


--
-- Name: main_invitation_list_photos_photo_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_invitation_list_photos_photo_id ON main_invitation_list_photos USING btree (photo_id);


--
-- Name: main_invitation_list_videos_invitation_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_invitation_list_videos_invitation_id ON main_invitation_list_videos USING btree (invitation_id);


--
-- Name: main_invitation_list_videos_video_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_invitation_list_videos_video_id ON main_invitation_list_videos USING btree (video_id);


--
-- Name: main_invitation_send_from_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_invitation_send_from_id ON main_invitation USING btree (send_from_id);


--
-- Name: main_invitation_send_to_invitation_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_invitation_send_to_invitation_id ON main_invitation_send_to USING btree (invitation_id);


--
-- Name: main_invitation_send_to_user_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_invitation_send_to_user_id ON main_invitation_send_to USING btree (user_id);


--
-- Name: main_location_comments_comment_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_location_comments_comment_id ON main_location_comments USING btree (comment_id);


--
-- Name: main_location_comments_location_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_location_comments_location_id ON main_location_comments USING btree (location_id);


--
-- Name: main_location_create_by_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_location_create_by_id ON main_location USING btree (create_by_id);


--
-- Name: main_location_follow_by_location_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_location_follow_by_location_id ON main_location_follow_by USING btree (location_id);


--
-- Name: main_location_follow_by_user_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_location_follow_by_user_id ON main_location_follow_by USING btree (user_id);


--
-- Name: main_location_list_photos_location_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_location_list_photos_location_id ON main_location_list_photos USING btree (location_id);


--
-- Name: main_location_list_photos_photo_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_location_list_photos_photo_id ON main_location_list_photos USING btree (photo_id);


--
-- Name: main_location_main_picture_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_location_main_picture_id ON main_location USING btree (main_picture_id);


--
-- Name: main_location_posts_location_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_location_posts_location_id ON main_location_posts USING btree (location_id);


--
-- Name: main_location_posts_notification_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_location_posts_notification_id ON main_location_posts USING btree (notification_id);


--
-- Name: main_location_reports_location_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_location_reports_location_id ON main_location_reports USING btree (location_id);


--
-- Name: main_location_reports_report_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_location_reports_report_id ON main_location_reports USING btree (report_id);


--
-- Name: main_message_user_receive_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_message_user_receive_id ON main_message USING btree (user_receive_id);


--
-- Name: main_message_user_send_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_message_user_send_id ON main_message USING btree (user_send_id);


--
-- Name: main_notification_notify_from_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_notification_notify_from_id ON main_notification USING btree (notify_from_id);


--
-- Name: main_notification_notify_to_notification_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_notification_notify_to_notification_id ON main_notification_notify_to USING btree (notification_id);


--
-- Name: main_notification_notify_to_user_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_notification_notify_to_user_id ON main_notification_notify_to USING btree (user_id);


--
-- Name: main_photo_user_post_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_photo_user_post_id ON main_photo USING btree (user_post_id);


--
-- Name: main_report_user_report_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_report_user_report_id ON main_report USING btree (user_report_id);


--
-- Name: main_tag_user_tag_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_tag_user_tag_id ON main_tag USING btree (user_tag_id);


--
-- Name: main_tickettransaction_ticket_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_tickettransaction_ticket_id ON main_tickettransaction USING btree (ticket_id);


--
-- Name: main_tickettransaction_user_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_tickettransaction_user_id ON main_tickettransaction USING btree (user_id);


--
-- Name: main_userprofile_avatar_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_userprofile_avatar_id ON main_userprofile USING btree (avatar_id);


--
-- Name: main_userprofile_followers_user_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_userprofile_followers_user_id ON main_userprofile_followers USING btree (user_id);


--
-- Name: main_userprofile_followers_userprofile_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_userprofile_followers_userprofile_id ON main_userprofile_followers USING btree (userprofile_id);


--
-- Name: main_userprofile_following_user_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_userprofile_following_user_id ON main_userprofile_following USING btree (user_id);


--
-- Name: main_userprofile_following_userprofile_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_userprofile_following_userprofile_id ON main_userprofile_following USING btree (userprofile_id);


--
-- Name: main_userprofile_invitations_invitation_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_userprofile_invitations_invitation_id ON main_userprofile_invitations USING btree (invitation_id);


--
-- Name: main_userprofile_invitations_userprofile_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_userprofile_invitations_userprofile_id ON main_userprofile_invitations USING btree (userprofile_id);


--
-- Name: main_userprofile_watchlist_activity_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_userprofile_watchlist_activity_id ON main_userprofile_watchlist USING btree (activity_id);


--
-- Name: main_userprofile_watchlist_userprofile_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_userprofile_watchlist_userprofile_id ON main_userprofile_watchlist USING btree (userprofile_id);


--
-- Name: main_video_key_data_like; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_video_key_data_like ON main_video USING btree (key_data varchar_pattern_ops);


--
-- Name: main_video_user_post_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX main_video_user_post_id ON main_video USING btree (user_post_id);


--
-- Name: socialaccount_socialaccount_user_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX socialaccount_socialaccount_user_id ON socialaccount_socialaccount USING btree (user_id);


--
-- Name: socialaccount_socialapp_sites_site_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX socialaccount_socialapp_sites_site_id ON socialaccount_socialapp_sites USING btree (site_id);


--
-- Name: socialaccount_socialapp_sites_socialapp_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX socialaccount_socialapp_sites_socialapp_id ON socialaccount_socialapp_sites USING btree (socialapp_id);


--
-- Name: socialaccount_socialtoken_account_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX socialaccount_socialtoken_account_id ON socialaccount_socialtoken USING btree (account_id);


--
-- Name: socialaccount_socialtoken_app_id; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX socialaccount_socialtoken_app_id ON socialaccount_socialtoken USING btree (app_id);


--
-- Name: tastypie_apikey_key; Type: INDEX; Schema: public; Owner: bobzovgeshjqfj; Tablespace: 
--

CREATE INDEX tastypie_apikey_key ON tastypie_apikey USING btree (key);


--
-- Name: account_emailaddress_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY account_emailaddress
    ADD CONSTRAINT account_emailaddress_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_emailconfirmation_email_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY account_emailconfirmation
    ADD CONSTRAINT account_emailconfirmation_email_address_id_fkey FOREIGN KEY (email_address_id) REFERENCES account_emailaddress(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_id_refs_id_1337a128; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY socialaccount_socialtoken
    ADD CONSTRAINT account_id_refs_id_1337a128 FOREIGN KEY (account_id) REFERENCES socialaccount_socialaccount(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: activity_id_refs_id_0b94df90; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_posts
    ADD CONSTRAINT activity_id_refs_id_0b94df90 FOREIGN KEY (activity_id) REFERENCES main_activity(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: activity_id_refs_id_12e8014c; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_list_photos
    ADD CONSTRAINT activity_id_refs_id_12e8014c FOREIGN KEY (activity_id) REFERENCES main_activity(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: activity_id_refs_id_18e2fe44; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activityuserrelation
    ADD CONSTRAINT activity_id_refs_id_18e2fe44 FOREIGN KEY (activity_id) REFERENCES main_activity(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: activity_id_refs_id_1e142ae5; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_userprofile_watchlist
    ADD CONSTRAINT activity_id_refs_id_1e142ae5 FOREIGN KEY (activity_id) REFERENCES main_activity(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: activity_id_refs_id_28c43321; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_reports
    ADD CONSTRAINT activity_id_refs_id_28c43321 FOREIGN KEY (activity_id) REFERENCES main_activity(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: activity_id_refs_id_35020d6c; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activityrequest
    ADD CONSTRAINT activity_id_refs_id_35020d6c FOREIGN KEY (activity_id) REFERENCES main_activity(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: activity_id_refs_id_4b895fe0; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_ticket
    ADD CONSTRAINT activity_id_refs_id_4b895fe0 FOREIGN KEY (activity_id) REFERENCES main_activity(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: activity_id_refs_id_4c8364c1; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_tags
    ADD CONSTRAINT activity_id_refs_id_4c8364c1 FOREIGN KEY (activity_id) REFERENCES main_activity(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: activity_id_refs_id_ba711b4d; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_invitation
    ADD CONSTRAINT activity_id_refs_id_ba711b4d FOREIGN KEY (activity_id) REFERENCES main_activity(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: activity_id_refs_id_e0670648; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_activity_request
    ADD CONSTRAINT activity_id_refs_id_e0670648 FOREIGN KEY (activity_id) REFERENCES main_activity(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: activity_id_refs_id_fc380d31; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_comments
    ADD CONSTRAINT activity_id_refs_id_fc380d31 FOREIGN KEY (activity_id) REFERENCES main_activity(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: activityrequest_id_refs_id_cbf5465f; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_activity_request
    ADD CONSTRAINT activityrequest_id_refs_id_cbf5465f FOREIGN KEY (activityrequest_id) REFERENCES main_activityrequest(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_id_refs_id_edac8a54; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY socialaccount_socialtoken
    ADD CONSTRAINT app_id_refs_id_edac8a54 FOREIGN KEY (app_id) REFERENCES socialaccount_socialapp(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_permission_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_permission_id_fkey FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_fkey FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_permission_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_permission_id_fkey FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: avatar_id_refs_id_8ef93b52; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_userprofile
    ADD CONSTRAINT avatar_id_refs_id_8ef93b52 FOREIGN KEY (avatar_id) REFERENCES main_photo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: comment_id_refs_id_a3de80c2; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_location_comments
    ADD CONSTRAINT comment_id_refs_id_a3de80c2 FOREIGN KEY (comment_id) REFERENCES main_comment(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: comment_id_refs_id_b7938b98; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_comment_reports
    ADD CONSTRAINT comment_id_refs_id_b7938b98 FOREIGN KEY (comment_id) REFERENCES main_comment(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: comment_id_refs_id_f9cbe5df; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_comments
    ADD CONSTRAINT comment_id_refs_id_f9cbe5df FOREIGN KEY (comment_id) REFERENCES main_comment(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: content_type_id_refs_id_d043b34a; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT content_type_id_refs_id_d043b34a FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: conversation_id_refs_id_a38ce98a; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_conversation_messages
    ADD CONSTRAINT conversation_id_refs_id_a38ce98a FOREIGN KEY (conversation_id) REFERENCES main_conversation(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: create_by_id_refs_id_c24bf037; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_feed
    ADD CONSTRAINT create_by_id_refs_id_c24bf037 FOREIGN KEY (create_by_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: create_by_id_refs_id_da16d567; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_location
    ADD CONSTRAINT create_by_id_refs_id_da16d567 FOREIGN KEY (create_by_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: crontab_id_refs_id_286da0d1; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY djcelery_periodictask
    ADD CONSTRAINT crontab_id_refs_id_286da0d1 FOREIGN KEY (crontab_id) REFERENCES djcelery_crontabschedule(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_content_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_fkey FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: feed_id_refs_id_454bf479; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_feed_user_receive
    ADD CONSTRAINT feed_id_refs_id_454bf479 FOREIGN KEY (feed_id) REFERENCES main_feed(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: group_id_refs_id_f4b32aac; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT group_id_refs_id_f4b32aac FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: interval_id_refs_id_1829f358; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY djcelery_periodictask
    ADD CONSTRAINT interval_id_refs_id_1829f358 FOREIGN KEY (interval_id) REFERENCES djcelery_intervalschedule(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: invitation_id_refs_id_20f2891f; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_invitation_list_photos
    ADD CONSTRAINT invitation_id_refs_id_20f2891f FOREIGN KEY (invitation_id) REFERENCES main_invitation(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: invitation_id_refs_id_514de004; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_userprofile_invitations
    ADD CONSTRAINT invitation_id_refs_id_514de004 FOREIGN KEY (invitation_id) REFERENCES main_invitation(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: invitation_id_refs_id_85c32769; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_invitation_send_to
    ADD CONSTRAINT invitation_id_refs_id_85c32769 FOREIGN KEY (invitation_id) REFERENCES main_invitation(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: invitation_id_refs_id_fdbe5e84; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_invitation_list_videos
    ADD CONSTRAINT invitation_id_refs_id_fdbe5e84 FOREIGN KEY (invitation_id) REFERENCES main_invitation(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: latest_message_id_refs_id_91e1a74c; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_conversation
    ADD CONSTRAINT latest_message_id_refs_id_91e1a74c FOREIGN KEY (latest_message_id) REFERENCES main_message(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: location_id_refs_id_10129335; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_location_posts
    ADD CONSTRAINT location_id_refs_id_10129335 FOREIGN KEY (location_id) REFERENCES main_location(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: location_id_refs_id_133783c8; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity
    ADD CONSTRAINT location_id_refs_id_133783c8 FOREIGN KEY (location_id) REFERENCES main_location(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: location_id_refs_id_2816af68; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_location_follow_by
    ADD CONSTRAINT location_id_refs_id_2816af68 FOREIGN KEY (location_id) REFERENCES main_location(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: location_id_refs_id_3f5577c7; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_location_list_photos
    ADD CONSTRAINT location_id_refs_id_3f5577c7 FOREIGN KEY (location_id) REFERENCES main_location(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: location_id_refs_id_4b20ec4b; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_location_comments
    ADD CONSTRAINT location_id_refs_id_4b20ec4b FOREIGN KEY (location_id) REFERENCES main_location(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: location_id_refs_id_d8796ad7; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_location_reports
    ADD CONSTRAINT location_id_refs_id_d8796ad7 FOREIGN KEY (location_id) REFERENCES main_location(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: logo_id_refs_id_8ccf44a7; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity
    ADD CONSTRAINT logo_id_refs_id_8ccf44a7 FOREIGN KEY (logo_id) REFERENCES main_photo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_picture_id_refs_id_d32ae225; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_location
    ADD CONSTRAINT main_picture_id_refs_id_d32ae225 FOREIGN KEY (main_picture_id) REFERENCES main_photo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: message_id_refs_id_cfad027c; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_conversation_messages
    ADD CONSTRAINT message_id_refs_id_cfad027c FOREIGN KEY (message_id) REFERENCES main_message(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: notification_id_refs_id_15ffd11a; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_posts
    ADD CONSTRAINT notification_id_refs_id_15ffd11a FOREIGN KEY (notification_id) REFERENCES main_notification(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: notification_id_refs_id_4def9eb3; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_notification_notify_to
    ADD CONSTRAINT notification_id_refs_id_4def9eb3 FOREIGN KEY (notification_id) REFERENCES main_notification(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: notification_id_refs_id_efa52228; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_location_posts
    ADD CONSTRAINT notification_id_refs_id_efa52228 FOREIGN KEY (notification_id) REFERENCES main_notification(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: notify_from_id_refs_id_00c1b50d; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_notification
    ADD CONSTRAINT notify_from_id_refs_id_00c1b50d FOREIGN KEY (notify_from_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: photo_id_refs_id_2ff7101b; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_invitation_list_photos
    ADD CONSTRAINT photo_id_refs_id_2ff7101b FOREIGN KEY (photo_id) REFERENCES main_photo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: photo_id_refs_id_a5daae34; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_list_photos
    ADD CONSTRAINT photo_id_refs_id_a5daae34 FOREIGN KEY (photo_id) REFERENCES main_photo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: photo_id_refs_id_efe7ad64; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_location_list_photos
    ADD CONSTRAINT photo_id_refs_id_efe7ad64 FOREIGN KEY (photo_id) REFERENCES main_photo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: report_id_refs_id_028d1d91; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_location_reports
    ADD CONSTRAINT report_id_refs_id_028d1d91 FOREIGN KEY (report_id) REFERENCES main_report(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: report_id_refs_id_0d4d091c; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_comment_reports
    ADD CONSTRAINT report_id_refs_id_0d4d091c FOREIGN KEY (report_id) REFERENCES main_report(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: report_id_refs_id_b101869f; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_reports
    ADD CONSTRAINT report_id_refs_id_b101869f FOREIGN KEY (report_id) REFERENCES main_report(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: send_from_id_refs_id_50b5c648; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_invitation
    ADD CONSTRAINT send_from_id_refs_id_50b5c648 FOREIGN KEY (send_from_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: site_id_refs_id_05d6147e; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY socialaccount_socialapp_sites
    ADD CONSTRAINT site_id_refs_id_05d6147e FOREIGN KEY (site_id) REFERENCES django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: socialapp_id_refs_id_e7a43014; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY socialaccount_socialapp_sites
    ADD CONSTRAINT socialapp_id_refs_id_e7a43014 FOREIGN KEY (socialapp_id) REFERENCES socialaccount_socialapp(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tag_id_refs_id_8f89d50f; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_tags
    ADD CONSTRAINT tag_id_refs_id_8f89d50f FOREIGN KEY (tag_id) REFERENCES main_tag(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ticket_id_refs_id_6f22ac83; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_tickettransaction
    ADD CONSTRAINT ticket_id_refs_id_6f22ac83 FOREIGN KEY (ticket_id) REFERENCES main_ticket(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ticket_id_refs_id_7c54a412; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity_ticket
    ADD CONSTRAINT ticket_id_refs_id_7c54a412 FOREIGN KEY (ticket_id) REFERENCES main_ticket(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user1_id_refs_id_f58682cd; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_conversation
    ADD CONSTRAINT user1_id_refs_id_f58682cd FOREIGN KEY (user1_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user2_id_refs_id_f58682cd; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_conversation
    ADD CONSTRAINT user2_id_refs_id_f58682cd FOREIGN KEY (user2_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_create_id_refs_id_d7935764; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activity
    ADD CONSTRAINT user_create_id_refs_id_d7935764 FOREIGN KEY (user_create_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_03765c38; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_tickettransaction
    ADD CONSTRAINT user_id_refs_id_03765c38 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_0b3729d9; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_notification_notify_to
    ADD CONSTRAINT user_id_refs_id_0b3729d9 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_1027138c; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_location_follow_by
    ADD CONSTRAINT user_id_refs_id_1027138c FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_1212f0cb; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_userprofile_followers
    ADD CONSTRAINT user_id_refs_id_1212f0cb FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_199ccdd2; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_frit
    ADD CONSTRAINT user_id_refs_id_199ccdd2 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_220001ef; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activityuserrelation
    ADD CONSTRAINT user_id_refs_id_220001ef FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_40c41112; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT user_id_refs_id_40c41112 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_4dc23c39; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT user_id_refs_id_4dc23c39 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_8eb3993f; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_comment
    ADD CONSTRAINT user_id_refs_id_8eb3993f FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_990aee10; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY tastypie_apikey
    ADD CONSTRAINT user_id_refs_id_990aee10 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_b4f0248b; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY socialaccount_socialaccount
    ADD CONSTRAINT user_id_refs_id_b4f0248b FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_c6c6bebc; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_feed_user_receive
    ADD CONSTRAINT user_id_refs_id_c6c6bebc FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_cb435143; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_invitation_send_to
    ADD CONSTRAINT user_id_refs_id_cb435143 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_d030f25f; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_userprofile_following
    ADD CONSTRAINT user_id_refs_id_d030f25f FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_f9ddd10f; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_userprofile
    ADD CONSTRAINT user_id_refs_id_f9ddd10f FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_post_id_refs_id_01ba3acd; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_video
    ADD CONSTRAINT user_post_id_refs_id_01ba3acd FOREIGN KEY (user_post_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_post_id_refs_id_a86deb4a; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_photo
    ADD CONSTRAINT user_post_id_refs_id_a86deb4a FOREIGN KEY (user_post_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_receive_id_refs_id_9ac4bf0f; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_message
    ADD CONSTRAINT user_receive_id_refs_id_9ac4bf0f FOREIGN KEY (user_receive_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_report_id_refs_id_b08d8c93; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_report
    ADD CONSTRAINT user_report_id_refs_id_b08d8c93 FOREIGN KEY (user_report_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_request_id_refs_id_c578428e; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_activityrequest
    ADD CONSTRAINT user_request_id_refs_id_c578428e FOREIGN KEY (user_request_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_send_id_refs_id_9ac4bf0f; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_message
    ADD CONSTRAINT user_send_id_refs_id_9ac4bf0f FOREIGN KEY (user_send_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_streams_single_table_backend_streamitem_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY user_streams_single_table_backend_streamitem
    ADD CONSTRAINT user_streams_single_table_backend_streamitem_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_tag_id_refs_id_33c6a779; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_tag
    ADD CONSTRAINT user_tag_id_refs_id_33c6a779 FOREIGN KEY (user_tag_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: userprofile_id_refs_id_19600bab; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_userprofile_following
    ADD CONSTRAINT userprofile_id_refs_id_19600bab FOREIGN KEY (userprofile_id) REFERENCES main_userprofile(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: userprofile_id_refs_id_34f13717; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_userprofile_invitations
    ADD CONSTRAINT userprofile_id_refs_id_34f13717 FOREIGN KEY (userprofile_id) REFERENCES main_userprofile(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: userprofile_id_refs_id_762dc6b8; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_userprofile_watchlist
    ADD CONSTRAINT userprofile_id_refs_id_762dc6b8 FOREIGN KEY (userprofile_id) REFERENCES main_userprofile(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: userprofile_id_refs_id_c75b56c0; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_userprofile_followers
    ADD CONSTRAINT userprofile_id_refs_id_c75b56c0 FOREIGN KEY (userprofile_id) REFERENCES main_userprofile(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: video_id_refs_id_02ef6d1b; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY main_invitation_list_videos
    ADD CONSTRAINT video_id_refs_id_02ef6d1b FOREIGN KEY (video_id) REFERENCES main_video(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: worker_id_refs_id_6fd8ce95; Type: FK CONSTRAINT; Schema: public; Owner: bobzovgeshjqfj
--

ALTER TABLE ONLY djcelery_taskstate
    ADD CONSTRAINT worker_id_refs_id_6fd8ce95 FOREIGN KEY (worker_id) REFERENCES djcelery_workerstate(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

